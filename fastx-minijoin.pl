#!/usr/bin/env perl
use warnings;
use strict;


use Pod::Usage; ## uses pod documentation in usage code
use Getopt::Long qw(:config auto_help pass_through);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);
use File::Temp; ## for temporary files and directories
use IPC::Open2; ## for redirecting STDOUT from called commands
use IPC::Open3; ## for redirecting STDOUT and STDERR from called commands

our $VERSION = "0.1";

=head1 NAME

fastx-minijoin.pl -- Join sequences using minimap2

=head1 SYNOPSIS

./fastx-minijoin.pl <seqs.fa> [options]

=head2 OPTIONS

=over 2

=item B<-matchlength> I<len>

Set the minimum overlap length to I<len>. If two sequences overlap
less than this, then they will not be considered for joining.

=back

=head1 DESCRIPTION

MiniJoin - Join sequences using minimap2

Sequences containing near-matching ends are joined into a single
merged sequence, using information from minimap2 alignments.

=back

=cut

sub signal_handler {
  my ($sigName) = @_;
  printf(STDERR "Interrupt received (SIG%s); deleting temporary files... ", $sigName);
  File::Temp::cleanup();
  print(STDERR "done.\n");
  exit(1);
};

sub rc {
  my ($seq) = @_;
  $seq =~ tr/ACGTUYRSWMKDVHBXN-/TGCAARYSWKMHBDVX*-/;
  # work on masked sequences as well
  $seq =~ tr/acgtuyrswmkdvhbx*/tgcaaryswkmhbdvx*/;
  return(scalar(reverse($seq)));
}

sub rev {
  my ($seq) = @_;
  return(scalar(reverse($seq)));
}

$| = 1; ## don't buffer output

my $maxDelta = 50;
my $maxEndGap = -1;
my $matchLength = 20000;
my $rcBasesThreshold = 50;
my $numThreads = 12;

GetOptions("matchlength|m=i" => \$matchLength,
           "endgap|e=i" => \$maxEndGap,
           "maxdelta|d=i" => \$maxDelta,
           "threads|t=i" => \$numThreads) or
    pod2usage("Error in command line arguments");

# Complain about non-file command line argument
my @files = ();
while(@ARGV){
  my $arg = shift(@ARGV);
  if(-e $arg){
    push(@files, $arg);
  } else {
    pod2usage("Unknown argument: $arg");
  }
}
@ARGV = @files;

# use stdin if no files supplied
if(!@ARGV){
  @ARGV = '-' unless (-t STDIN);
}

if($maxEndGap == -1){
  $maxEndGap = $maxDelta;
}

## Load sequences into memory
my %seqs = ();
my %quals = ();
my $inQual = 0; # false
my $seqID = "";
my $qualID = "";
my $seq = "";
my $qual = "";
my $buffer = "";
foreach my $file (@ARGV) {
  # This little gunzip dance makes sure the script can handle both
  # gzip-compressed and uncompressed input, regardless of whether
  # or not it is piped
  my $z = new IO::Uncompress::Gunzip($file, MultiStream => 1)
    or pod2usage("gunzip failed: $GunzipError\n");
  while(<$z>){
    chomp;
    chomp;
    if (!$inQual) {
      if (/^(>|@)((.+?)( .*?\s*)?)$/) {
        my $newSeqID = $2;
        my $newShortID = $3;
        if($seqID){
          printf("Adding %s: %s..%s\n",
                 $seqID, substr($seq, 0, 20), substr($seq, -20));
          $seqs{$seqID} = $seq;
          $quals{$qualID} = $qual if $qual;
        }
        $seq = "";
        $qual = "";
        $buffer = "";
        $seqID = $newSeqID;
      } elsif (/^\+(.*)$/) {
        $inQual = 1;            # true
        $qualID = $1;
      } else {
        $seq .= $_;
      }
    } else {
      $qual .= $_;
      if (length($qual) >= length($seq)) {
        $inQual = 0;            # false
      }
    }
  }
}
if($seqID){
  printf("Adding %s: %s..%s\n",
         $seqID, substr($seq, 0, 20), substr($seq, -20));
  $seqs{$seqID} = $seq;
  $quals{$qualID} = $qual if $qual;
}

my $mm2Version = qx(minimap2 -V);
if(!$mm2Version || ($mm2Version !~ /^[0-9]+\.[0-9]+/)){
  pod2usage("Error: minimap2 is not installed; cannot continue\n");
} else {
  print(STDERR "minimap2 installed: ${mm2Version}");
}

my $tmpOutDir = File::Temp -> newdir("minijoinXXXXXX");

print("Running minimap2...\n");

## write merged sequence to combined file
my $searchFileName = ${tmpOutDir}."/search_seqs.fa";
open(my $searchFile, '>', $searchFileName) or die "Could not write to $searchFileName: $!";
while (my($seqID, $seq) = each %seqs){
  printf($searchFile ">%s\n%s\n", $seqID, $seq);
}
close($searchFile);

my $errorFileName = ${tmpOutDir}."/minijoin.err";
my ($mjInput, $mjOutput, $mjErr);
open($mjErr, '>', $errorFileName) or die "Could not write to $errorFileName: $!";
my $pid = open3($mjInput, $mjOutput, $mjErr,
                "minimap2", "-m", $matchLength, "-D", "--dual=no",
                "-t", $numThreads, $searchFileName, $searchFileName);
close($mjInput);
while(<$mjOutput>){
  my @F = split(/\t/);
  my $seqName1 = $F[0];
  my $seqLen1 = $F[1]; 
  my $start1 = $F[2];
  my $end1 = $F[3];
  my $matchDir = $F[4];
  my $seqName2 = $F[5];
  my $seqLen2 = $F[6];
  my $start2 = $F[7];
  my $end2 = $F[8];
  my $matchLen1 = $F[8];
  my $matchLen2 = $F[9];
  my $rc1 = 0; # false
  my $rc2 = ($matchDir eq "-");
  if(($seqName1 ne $seqName2) &&
     ($matchLen1 >= $matchLength) && ($matchLen2 >= $matchLength)){
    if(($matchDir eq "+") || ($matchDir eq "-")){
      if(($start2 <= $maxEndGap) || (($seqLen1 - $end1) <= $maxEndGap) ||
         (($seqLen2 - $end2) <= $maxEndGap) || ($start1 <= $maxEndGap)){
        ## Try to keep the greatest amount of leftover sequence
        my $trimmedCurrent = ($matchDir eq "+") ?
            ($start2 + ($seqLen1 - $end1)) :
            (($seqLen2 - $end2) + ($seqLen1 - $end1));
        my $trimmedAlternate = ($matchDir eq "+") ?
            ($start1 + ($seqLen2 - $end2)) :
            ($start1 + $start2);
        if($trimmedCurrent > $trimmedAlternate){
          print("Swapping to better orientation...\n");
          if($matchDir eq "-"){
            $rc1 = !$rc1;
            $rc2 = !$rc2;
          } else {
            ($seqName1, $start1, $end1, $seqLen1, $matchLen1,
             $seqName2, $start2, $end2, $seqLen2, $matchLen2) =
                ($seqName2, $start2, $end2, $seqLen2, $matchLen2,
                 $seqName1, $start1, $end1, $seqLen1, $matchLen1);
          }
        }
        my $startChunkSize = ($rc1) ? ($seqLen1 - $end1) : ($start1);
        my $endChunkSize = ($rc2) ? ($start2) : ($seqLen2 - $end2);
        if($startChunkSize >= 43){
          printf("%-20s %9d|%s...%s|%9d\n",
                 substr($seqName1,0,20),
                 ($rc1) ? $seqLen1 : 1,
                 ($rc1) ?
                 rc(substr($seqs{$seqName1}, $seqLen1 - 20, 20)) :
                 substr($seqs{$seqName1}, 0, 20),
                 ($rc1) ?
                 rc(substr($seqs{$seqName1}, $end1, 20)):
                 substr($seqs{$seqName1}, $start1 - 20, 20),
                 ($rc1) ? ($end1+1) : ($start1));
        } else {
          printf("%-20s %9d|%43s|%9d\n",
                 substr($seqName1,0,20),
                 ($rc1) ? $seqLen1 : 1,
                 ($rc1) ?
                 rc(substr($seqs{$seqName1}, -$startChunkSize)) :
                 substr($seqs{$seqName1}, 0, $start1),
                 ($rc1) ? ($end1+1) : ($start1));
        }
        printf("%-20s %9d|%s...%s|%9d\n",
               substr($seqName1,0,20),
               ($rc1) ? ($end1) : ($start1+1),
               ($rc1) ?
               rc(substr($seqs{$seqName1}, $end1 - 20, 20)) :
               substr($seqs{$seqName1}, $start1, 20),
               ($rc1) ?
               rc(substr($seqs{$seqName1}, $start1, 20)) :
               substr($seqs{$seqName1}, $end1 - 20, 20),
               ($rc1) ? ($start1) : ($end1+1));
        printf("%-20s %9s %s...%s %9s\n",
               "",
               "",
               "|" x 20,
               "|" x 20,
               "");
        printf("%-20s %9d|%s...%s|%9d\n",
               substr($seqName2,0,20),
               
               ($rc2) ? ($end2 + 1) : ($start2+1),
               ($rc2) ?
               rc(substr($seqs{$seqName2}, $end2 - 20, 20)) :
               substr($seqs{$seqName2}, $start2, 20),
               ($rc2) ?
               rc(substr($seqs{$seqName2}, $start2, 20)):
               substr($seqs{$seqName2}, $end2 - 20, 20),
               ($rc2) ? ($start2 + 1) : ($end2+1));
        if($endChunkSize >= 43){
          printf("%-20s %9d|%s...%s|%9d\n",
                 substr($seqName2, 0, 20),
                 ($rc2) ? ($start2) : ($end2+1),
                 ($rc2) ?
                 rc(substr($seqs{$seqName2}, $start2, 20)) :
                 substr($seqs{$seqName2}, $end2, 20),
                 ($rc2) ?
                 rc(substr($seqs{$seqName2}, 0, 20)) :
                 substr($seqs{$seqName2}, $seqLen2 - 20, 20),
                 ($rc2) ? 1 : $seqLen2);
        } elsif($endChunkSize > 0) {
          printf("%-20s %9d|%-43s|%9d\n",
                 substr($seqName2, 0, 20),
                 ($rc2) ? ($start2) : ($end2+1),
                 ($rc2) ?
                 rc(substr($seqs{$seqName2}, 0, $start2)) :
                 substr($seqs{$seqName2}, $end2),
                 ($rc2) ? 1 : $seqLen2);
        }
        print("mm2: $_");
      }
    } elsif($matchDir eq "-"){
      if(($start2 <= $maxEndGap) || (($seqLen1 - $end1) <= $maxEndGap) ||
         (($seqLen2 - $end2) <= $maxEndGap) || ($start1 <= $maxEndGap)){
        print("mm2: $_");
      }
    }
  }
}
waitpid($pid, 0);

my $child_exit_status = $? >> 8;
close($mjOutput);
close($mjErr);
