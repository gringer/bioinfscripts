#!/usr/bin/Rscript

blast.headings <- c("qseqid","sseqid","pident","length","mismatch","gapopen",
                    "qstart","qend","sstart","send","evalue","bitscore");

data.df <- read.delim("APR_nbref_vs_protein.tsv", header=FALSE,
                      col.names=blast.headings);

qMins <- tapply(c(data.df$qstart,data.df$qend),rep(data.df$qseqid,2),min);
qMaxes <- tapply(c(data.df$qstart,data.df$qend),rep(data.df$qseqid,2),max);
qLens <- qMaxes - qMins;
qPadding <- sum(qLens)/10;
qcumStart <- cumsum(c(0,qLens)+qPadding);
names(qcumStart) <- c(names(qLens),"--");
data.df$adjqstart <- data.df$qstart - qMins[data.df$qseqid] +
    qcumStart[data.df$qseqid];
data.df$adjqend <- data.df$qend - qMins[data.df$qseqid] +
    qcumStart[data.df$qseqid];

sMins <- tapply(c(data.df$sstart,data.df$send),rep(data.df$sseqid,2),min);
sMaxes <- tapply(c(data.df$sstart,data.df$send),rep(data.df$sseqid,2),max);
sLens <- sMaxes - sMins;
sPadding <- sum(sLens) * 0;
scumStart <- cumsum(c(0,sLens)+sPadding);
names(scumStart) <- c(names(sLens),"--");
data.df$adjsstart <- data.df$sstart - sMins[data.df$sseqid] +
    scumStart[data.df$sseqid];
data.df$adjsend <- data.df$send - sMins[data.df$sseqid] +
    scumStart[data.df$sseqid];

pdf("APR1_APR2_vs_nbref.pdf", width=12, height=4);
plot(NA, ylim = range(c(data.df$adjsstart,data.df$adjsend)),
     xlim = range(c(data.df$adjqstart,data.df$adjqend)),
     axes=FALSE, frame.plot=TRUE, xlab="", ylab="");
abline(h=scumStart-sPadding/2, lty="dotted", col="#808080");
abline(v=qcumStart-qPadding/2, lty="dotted", col="#808080");
segments(y0=data.df$adjsstart, y1=data.df$adjsend,
         x0=data.df$adjqstart, x1=data.df$adjqend);
axis(1,at=qcumStart-qPadding/2,labels=FALSE);
axis(1,at=diff(qcumStart)/2 + head(qcumStart,-1) - qPadding/2,
     labels=names(qLens),
     las=1, tick=FALSE);
axis(2,at=scumStart,labels=FALSE);
axis(2,at=diff(scumStart)/2 + head(scumStart,-1),labels=names(sLens),
     las=1, tick=FALSE);
dummy <- dev.off();
