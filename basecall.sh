#!/bin/sh

model_location=$(readlink -e ~/install/dorado);
R10_4_1_model="${model_location}/dna_r10.4.1_e8.2_400bps_sup@v4.2.0";
R9_4_1_model="${model_location}/dna_r9.4.1_e8_sup@v3.6";

echo "Model location: ${model_location}";

old_dir=$(pwd);
read_dir=$1;
thread_count=10;

cd read_dir;
find . -name '*.pod5' > pod5_files_all.txt;
split -l 100 pod5_files_all.txt pod5_files_split_;
rename 's/$/.txt/' *_split_*;
mkdir -p toCall;
for x in pod5_files_split_*.txt
  do rm toCall/*;
  cat ${x} | while read fname
    do ln -s $(readlink -e ${fname}) toCall
  done
  dorado basecaller -b 128 -r --emit-fastq ${R10_4_1_model} toCall | bgzip -@ 10 > ${x}_called.fastq.gz;
done

cd ${old_dir};
