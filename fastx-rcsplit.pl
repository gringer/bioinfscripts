#!/usr/bin/env perl
use warnings;
use strict;

## fastx-rcsplit.pl -- Split a sequence containing a
## reverse-complement copy of itself into two component sequences,
## split at the median point where reverse-complement repeats are seen

use Getopt::Long qw(:config auto_help pass_through);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);

sub rc {
  my ($seq) = @_;
  $seq =~ tr/ACGTUYRSWMKDVHBXN-/TGCAARYSWKMHBDVX*-/;
  # work on masked sequences as well
  $seq =~ tr/acgtuyrswmkdvhbx*/tgcaaryswkmhbdvx*/;
  return(scalar(reverse($seq)));
}

sub rev {
  my ($seq) = @_;
  return(scalar(reverse($seq)));
}

sub printStats {
  my ($seq, $seqID, $qual, $qualID, $kmerLength, $rcPropThresh, $rcBaseThresh) = @_;
  my $len = length($seq);
  my %rcCentres = ();
  my $sseq = "";
  if($seqID && (length($seq) > $kmerLength)){
    my %rptCount = ();
    my $maxKmer = "";
    my %rptPos = ();
    my %rcSeen = ();
    my %rcDups = ();
    my $rcCount = 0;
    for(my $p = 0; ($p + $kmerLength) <= $len; $p++){
      $sseq = substr($seq, $p, $kmerLength);
      if($sseq =~ /N/){
        next;
      }
      my $rcs  = rc($sseq);
      if(exists($rptPos{$rcs})){
        my $oldP = $rptPos{$rcs};
	my $mid = ($p + ($oldP + $kmerLength - 1)) >> 1; # divide by 2, remove remainder
        if(!exists($rcCentres{$oldP})){
          $rcCentres{$p} = $mid;
          $rcCentres{$oldP} = $mid;
          $rcCount += 2;
        } else {
          $rcDups{$p} = 1;
          $rcDups{$oldP} = 1;
          $rcCount += 1;
        }
      }
      $rptPos{$sseq} = $p;
    }
    my @midPoints = ();
    foreach my $pos (keys(%rcCentres)){
      if(!$rcDups{$pos}){
        push(@midPoints, $rcCentres{$pos});
      }
    }
    my $medMidPoint = -1;
    if(@midPoints){
      my @sortedMids = sort {$a <=> $b} @midPoints;
      $medMidPoint = $sortedMids[($#midPoints >> 1)];
    }
    my $rcRatio = $rcCount/($len - $kmerLength + 1);
    my $numDups = scalar(keys(%rcDups));
    printf(STDERR "%8d %0.3f %8d %8d %8d %s\n",
	   $len, $rcRatio, $rcCount,
	   $numDups, $medMidPoint,
	   $seqID);
    if(($rcRatio > $rcPropThresh) && ($rcCount > $rcBaseThresh) && ($medMidPoint > -1)){
      my $seqIDbase = "";
      my $seqIDrest = "";
      if($seqID =~ /(^[^ ]+)(.*)$/){
        $seqIDbase = $1;
        $seqIDrest = $2;
      }
      ## When outputting split reads, the forward sequence is
      ## reverse-complemented, so that both sequences start from the
      ## split point (for nanopore reads, the split point is slightly
      ## more likely to have good bases compared to the sequence ends)
      if($qual){
        printf("@%s_split1%s\n%s\n+\n%s\n", $seqIDbase, $seqIDrest, rc(substr($seq, 0, $medMidPoint)), scalar(reverse(substr($qual, 0, $medMidPoint))));
        printf("@%s_split2%s\n%s\n+\n%s\n", $seqIDbase, $seqIDrest, substr($seq, $medMidPoint),    substr($qual, $medMidPoint));
      } else {
        my $seq1 = rc(substr($seq, 0, $medMidPoint));
        my $seq2 = substr($seq, $medMidPoint);
        $seq1 =~ s/(.{100})/$1\n/g;
        $seq1 =~ s/\n$//;
        $seq2 =~ s/(.{100})/$1\n/g;
        $seq2 =~ s/\n$//;
        printf(">%s_split1%s\n%s\n", $seqIDbase, $seqIDrest, $seq);
        printf(">%s_split2%s\n%s\n", $seqIDbase, $seqIDrest, $seq);
      }
    } else {
      if($qual){
        printf("@%s\n%s\n+\n%s\n", $seqID, $seq, $qual);
      } else {
        $seq = substr($seq, $medMidPoint);
        $seq =~ s/(.{100})/$1\n/g;
        $seq =~ s/\n$//;
        printf(">%s\n%s\n", $seqID, $seq);
      }
    }
  }
}

$| = 1; ## don't buffer output

my $kmerLength = 17; ## number of bases in hash keys
my $rcPropThreshold = 0.6;
my $rcBasesThreshold = 50;

GetOptions("kmerlength|k=i" => \$kmerLength) or
    die("Error in command line arguments");

# Complain about non-file command line argument
my @files = ();
while(@ARGV){
  my $arg = shift(@ARGV);
  if(-e $arg){
    push(@files, $arg);
  } else {
    die("Unknown argument: $arg");
  }
}
@ARGV = @files;

# use stdin if no files supplied
if(!@ARGV){
  @ARGV = '-' unless (-t STDIN);
}

if($kmerLength != 17){
  print(STDERR "Setting kmer length to $kmerLength\n");
}

my $inQual = 0; # false
my $seqID = "";
my $qualID = "";
my $seq = "";
my $qual = "";
my $buffer = "";
printf(STDERR "%8s %5s %8s %8s %8s %s\n",
       "length", "rcRat", "cntRC", "cntDups", "medMid",
       "SeqID");
foreach my $file (@ARGV) {
  # This little gunzip dance makes sure the script can handle both
  # gzip-compressed and uncompressed input, regardless of whether
  # or not it is piped
  my $z = new IO::Uncompress::Gunzip($file, MultiStream => 1)
    or die "gunzip failed: $GunzipError\n";
  while(<$z>){
    chomp;
    chomp;
    if (!$inQual) {
      if (/^(>|@)((.+?)( .*?\s*)?)$/) {
        my $newSeqID = $2;
        my $newShortID = $3;
        printStats($seq, $seqID, $qual, $qualID, $kmerLength, $rcPropThreshold, $rcBasesThreshold);
        $seq = "";
        $qual = "";
        $buffer = "";
        $seqID = $newSeqID;
      } elsif (/^\+(.*)$/) {
        $inQual = 1;            # true
        $qualID = $1;
      } else {
        $seq .= $_;
      }
    } else {
      $qual .= $_;
      if (length($qual) >= length($seq)) {
        $inQual = 0;            # false
      }
    }
  }
}

printStats($seq, $seqID, $qual, $qualID, $kmerLength, $rcPropThreshold, $rcBasesThreshold);
