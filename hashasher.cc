// HashAsher -- A fast hash-based repeat finder library

// Copyright (c) 2020-2024, David Eccles (gringer) <bioinformatics@gringene.org>

// See 'hashasher.r' for an example implementation
// [usage: ./hashasher.r [options] <file.fasta>]

// This code can use greg7mdp's parallel hashmap library
// [https://github.com/greg7mdp/parallel-hashmap]
// See Apache-2.0 license information in 'parallel_hashmap/phmap.h'

// Using phmap.h will speed up repeat calculations, but is otherwise
// unnecessary. To use, place the 'parallel_hashmap' directory in the
// same directory as this file (it is included in the bioinfscripts
// repository, so should be automatically used if the repository is
// cloned).

#if __has_include("parallel_hashmap/phmap.h")
#include <parallel_hashmap/phmap.h>
#define USE_PHMAP
#endif

#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <bitset>
#include <cstdio>
#include <ctime>
#include <climits>
#include <chrono>
#include "Rcpp.h"

#if defined(USE_PHMAP)
using phmap::flat_hash_map;
using phmap::flat_hash_set;
#endif // USE_PHMAP

#define UPPER_MASK ((1L << 62L) - 1)
#define HALF_MASK ((1L << 32L) - 1)
// The missing hash should be unlikely to occur in normal circumstances
// currently equal to
//    1011011101111011111011111101111111011111111011111111101111111111b
// or  TGCGCGTGGTGGGCGGGCGGGTGGGGTGGGGG
#define MISSING_HASH (0xB77BEFDFDFEFFBFFL)
// complement is used for flipping the upper bits, and is
// equal to 10101010 ... 10101010b
#define COMPLEMENT   (0xAAAAAAAAAAAAAAAAL)
// UPPER_2BIT is used for checking reverse bit sequences
#define UPPER_2BIT (0xC000000000000000L)

// upper nibble of location defines type
#define KT_N    (0xF000000000000000L)
#define KT_F    (0x1000000000000000L)
#define KT_C    (0x3000000000000000L)
#define KT_R    (0x5000000000000000L)
#define KT_RC   (0x7000000000000000L)
#define KT_REVD (0x4000000000000000L)
#define KT_COMP (0x2000000000000000L)
#define COMP_MASK   (0x2000000000000000L)
#define REV_MASK    (0x4000000000000000L)
#define POS_MASK    (0x0FFFFFFFFFFFFFFFL)
#define POS_MASK    (0x0FFFFFFFFFFFFFFFL)
#define POS_INVALID (0xFFFFFFFFFFFFFFF0L)

// Kmer_C is the same as Kmer, but base representations are the
// complement of the original base. 
// Kmer_R is traversed in reverse order.
// Kmer_RC is both reversed and complemented.

// templated sign function; returns -1/0/1 based on sign of the number
// see https://stackoverflow.com/questions/1903954
template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

// left circular shift (bit rotation)
// see https://stackoverflow.com/a/776523/3389895
inline uint64_t rotl64 ( uint64_t x, unsigned int r ) {
  const unsigned int mask = (CHAR_BIT*sizeof(x) - 1);
  r = (r % 64) & mask;
  return (x << r) | (x >> ((-r) & mask));
}

// right circular shift (bit rotation)
inline uint64_t rotr64 ( uint64_t x, unsigned int r ) {
  const unsigned int mask = (CHAR_BIT*sizeof(x) - 1);
  r = (r % 64) & mask;
  return (x >> r) | (x << ((-r) & mask));
}

// Magic numbers for hashing, designed so that each 2-bit chunk
// contains a shuffling of 00, 01, 10, 11, which should make the hash
// more representative of (and unpredictable for) DNA sequences.
// > apply(t(replicate(16, sprintf("%x", colSums(t(replicate(2, sample(0:3))) *
//          c(1,4))))), 2, paste, collapse="")
const uint64_t baseHash[4] =  { 0x8052315a0dea934bL, 0x5f3984bd9341c9e4L,
                                0xe58f5f24f8b736bdL, 0x3ae4eac3661c6c12L };
// [simple single base translation for debugging]
// const uint64_t baseHash[4] =  { 0x0000000000000000L, 0x0000000000000001L,
//                                 0x0000000000000002L, 0x0000000000000003L};

using namespace Rcpp;
using namespace std;

enum FastXFileType { none, fasta, fastq, fastm, gfa };

// temporary variables
vector<string> fastxBuf;
vector<string> readBuf;
string buf;

// independent state variables
fstream fastaFileFS, readFileFS;
FastXFileType seqFormat = FastXFileType::none;
size_t kmerSize;
size_t kmerSizeM1;
size_t kmerBitSize;
size_t kmerBitSizeM1;
size_t kmerBitSizeM2;
uint64_t kmerMask;
string seqID, readID;
string seqQual;
uint64_t seqLen, readLen;
uint64_t urpt, uorpt;
uint64_t hashCount;
uint64_t compCount;
uint64_t binDeltaPos = POS_INVALID;
uint64_t binDeltaNeg = POS_INVALID;
vector<uint8_t> bitSeq;
vector<bool> baseMissing;
vector<bool> baseModified;
vector<bool> nearMissing;
map<uint64_t, vector<uint64_t>> repetitivePoss;
vector<bool> isInteresting;
vector<bool> isRepetitive;
vector<bool> isReverse;
vector<bool> isRevcomp;
vector<bool> isComplement;
bool useFastHash = true;
uint64_t hashDiv16Hits = 0;

// memoise cache for pwLogLin function, because it involves a lot of
// float computation
vector<int64_t> llCache;
bool cachePrepared = false;

// hash caches to speed up hash (re)calculations
uint64_t nextHash = MISSING_HASH;
uint64_t nextPos = POS_INVALID;
uint64_t nextType = KT_N;

// state flags
bool fileLoaded = false;
bool seqsRemaining = false;
bool readsRemaining = false;
bool kmersCounted = false;
bool repeatsHashed = false;

class Kmer {
protected:
  const uint64_t startPos;
public:
  Kmer(uint64_t basePos, uint64_t kmerType);
  static uint64_t makeHash(uint64_t basePos, uint64_t kmerType);
  static uint64_t makeHash(Kmer const& k);
  static uint64_t makeFastHash(uint64_t basePos, uint64_t kmerType,
                               uint64_t lastHash,
                               vector<uint8_t>::iterator kS,
                               vector<uint8_t>::iterator kE);
  uint64_t getPos() const;
  uint64_t getType() const;
  virtual string getSequence() const;
  virtual bool operator< (const Kmer& b) const;
  virtual bool operator== (const Kmer& b) const;
};

// dependent state variables
map<Kmer, uint64_t> kmerCountsOrdered;
#if defined(USE_PHMAP)
flat_hash_map<Kmer, uint64_t> kmerCounts;
flat_hash_set<Kmer> kmerStore;
#else
unordered_map<Kmer, uint64_t> kmerCounts;
unordered_set<Kmer> kmerStore;
#endif // USE_PHMAP

char baseLookup[4] = {'A', 'C', 'T', 'G'};

namespace std {
  template<> struct hash<Kmer> {
    uint64_t operator()(Kmer const& k) const {
      return(Kmer::makeHash(k));
    }
  };
}

inline uint64_t Kmer::makeHash(uint64_t basePos, uint64_t kmerType) {
  // For kmers of all lengths, this uses a rolling hash algorithm
  // similar to ntHash (Mohamadi et al, 2016), following the
  // process as translated by Heng Li here:
  // [https://bioinformatics.stackexchange.com/questions/19#comment563_293]
  //
  // The algorithm has been modified to slide two bits each time,
  // to be in line with the 2bit-base shuffling used in generating
  // the magic numbers (and decoding bases). This means that the
  // rotations have a cycle of 32 slots rather than 64 slots, but
  // that shouldn't be an issue for hash distribution because the
  // bases are encoded as 2 bits and are effectively random.
  //
  // Optimisation note: the creation of a Kmer object for every hash
  // is probably slowing this down a lot.
  //
  hashCount++;
  if((nextPos == basePos) && (nextType == kmerType)){
    return(nextHash);
  }
  if(nearMissing[basePos]){ // short-circuit kmers containing N
    return(MISSING_HASH);
  }
  uint64_t hr = 0L;
  uint64_t compBits = ((kmerType & KT_COMP) != 0) ? 2L : 0;
  if((kmerType & KT_REVD) == 0){ // forward direction (may be complemented)
    for(size_t hp = basePos, rot = kmerBitSizeM2;
        hp < (basePos + kmerSize); hp++, rot-= 2){
      hr ^= (uint64_t)rotl64(baseHash[((uint64_t)bitSeq[hp]) ^ compBits], rot);
    }
  } else { // reverse direction (may be complemented)
    for(size_t hp = basePos + kmerSize, rot = kmerBitSizeM2;
        hp > basePos; rot-= 2){
      hr ^= (uint64_t)rotl64(baseHash[((uint64_t)bitSeq[--hp]) ^ compBits],
                             rot);
    }
  }
  return(hr);
}

// When a hash needs to be calculated for the next base after
// the last hash calculated, the rolling hash aspect is
// exploited to quickly calculate that next hash.
//
// This has been separated from the makeHash code to allow for alternative
// optimisations
inline uint64_t Kmer::makeFastHash(uint64_t basePos, uint64_t kmerType,
                                   uint64_t lastHash,
                                   vector<uint8_t>::iterator kS,
                                   vector<uint8_t>::iterator kE) {
  if(!useFastHash){
    return(makeHash(basePos, kmerType));
  }
  uint64_t compBits = ((kmerType & KT_COMP) != 0) ? 2L : 0;
  if((kmerType & KT_REVD) == 0){ // forward direction (may be complemented)
    // XOR the following:
    // * a single left rotation of the previous hash, f(s[i-1,i+k-2])
    // * a k-times left rotation of the base hash of s[i-1]
    // * the base hash of s[i+k-1]
    return(rotl64(lastHash, 2L) ^
           rotl64(baseHash[*kS ^ compBits], kmerBitSize) ^
           baseHash[*kE ^ compBits]);
  } else { // reverse direction (may be complemented)
    // XOR the following:
    // * a single right rotation of the previous R hash, r(s[i-1, i+k-2])
    // * a single right rotation of the base hash of the R of s[i-1]
    // * a k−1-times left rotation of the base hash of the R of s[i+k-1]
    return(rotr64(lastHash, 2L) ^
           rotr64(baseHash[*kS ^ compBits], 2L) ^
           rotl64(baseHash[*kE ^ compBits], kmerBitSizeM2));
  }
}

inline uint64_t Kmer::makeHash(Kmer const& k) {
  return Kmer::makeHash(k.startPos & POS_MASK, k.startPos & KT_N);
}

Kmer::Kmer(uint64_t basePos, uint64_t kmerType) :
  startPos(basePos | kmerType){
  if((kmerType & KT_N) == 0){
    cerr << "Error: Kmer without type at " << (basePos) << endl;
  }
}

// Retrieve a string representation of the kmer
string Kmer::getSequence() const{
  uint64_t basePos = getPos();
  uint64_t kmerType = getType();
  if(basePos + kmerSize > seqLen){
    cerr << "Error: (getSequence) asked to retrieve kmer beyond sequence end ["
	 << basePos << "]\n";
    return("");
  }
  uint8_t bsp;
  string res = "";
  uint64_t compBits = ((kmerType & KT_COMP) != 0) ? 2L : 0;
  if((kmerType & KT_REVD) != 0){ // reverse direction (may be complemented)
    for(uint64_t hp = basePos; hp < (basePos + kmerSize); hp++){
      bsp = (uint64_t)bitSeq[hp] ^ compBits;
      res = (baseMissing[hp] ? 'N' :
             (((kmerType != KT_RC) && baseModified[hp]) ? 'm' :
              baseLookup[bsp])) + res;
    }
  } else { // forward direction
    for(uint64_t hp = basePos; hp < (basePos + kmerSize); hp++){
      bsp = (uint64_t)bitSeq[hp] ^ compBits;
      res = res + (baseMissing[hp] ? 'N' :
                   (((kmerType != KT_C) && baseModified[hp]) ? 'm' :
                    baseLookup[bsp]));
    }
  }
  return(res);
}

uint64_t Kmer::getPos() const{
  return(startPos & POS_MASK);
}

uint64_t Kmer::getType() const{
  return(startPos & KT_N);
}

// Check order of two kmers (note: complement / reverse not checked)
bool Kmer::operator< (const Kmer& b) const{
  uint64_t basePos1 = getPos();
  uint64_t basePos2 = b.getPos();
  if((basePos1 + kmerSize > seqLen) || (basePos2 + kmerSize > seqLen)){
    cerr << "Error: (<) asked to retrieve kmer beyond sequence end ["
	 << basePos1 << ", " << basePos2 << "]\n";
    return false;
  }
  if(getType() != b.getType()){
    cerr << "Error: (<) asked to order kmers of different types ["
	 << getType() << ", " << b.getType() << "]\n";
    return false;
  }
  // kmers containing missing bases are considered to have a value
  // less than other kmers
  if(nearMissing[basePos1] || nearMissing[basePos2]){
    return(nearMissing[basePos1] && !nearMissing[basePos2]);
  }
  uint8_t bs1, bs2; // should probably use `*` instead of variable assignment
  for(uint64_t i1 = basePos1, i2=basePos2; i1 < (basePos1 + kmerSize); i1++, i2++){
    bs1 = bitSeq[i1]; bs2 = bitSeq[i2];
    if(bs1 != bs2){
      return(bs1 < bs2);
    }
  }
  // equal
  return(false);
}

// Check equality of two kmers
// todo: implement base modification
bool Kmer::operator== (const Kmer& b) const{
  compCount++;
  uint64_t basePos1 = getPos();
  uint64_t basePos2 = b.getPos();
  // cout << "Checking for equality: " << basePos1 << " vs " << basePos2 << "...";
  if(basePos1 == basePos2){ // trivial, hopefully not needed
    return true;
  }
  bool doComplement = ((getType() ^ b.getType()) & COMP_MASK) > 0;
  bool doReverse = ((getType() ^ b.getType()) & REV_MASK) > 0;
  if((basePos1 + kmerSize > seqLen) || (basePos2 + kmerSize > seqLen)){
    cerr << "Error: (==) asked to retrieve kmer beyond sequence end ["
	 << basePos1 << ", " << basePos2 << "]\n";
    return false;
  }
  // all kmers containing 'N' are considered equal,
  // and not equal to kmers not containing 'N'
  if(nearMissing[basePos1] || nearMissing[basePos2]){
    return(nearMissing[basePos1] == nearMissing[basePos2]);
  }
  uint8_t compBits = doComplement ? 2 : 0;
  uint8_t bs1, bs2;
  if(!doReverse){
    for(uint64_t i1 = basePos1, i2=basePos2; i1 < (basePos1 + kmerSize); i1++, i2++){
      bs1 = bitSeq[i1]; bs2 = bitSeq[i2] ^ compBits;
      if(bs1 != bs2){
	// cout << " not equal (F) at " << basePos1 << "," << basePos2 << endl;
	return(false);
      }
    }
  } else {
    // reversed, so work from end of sequence 2 back to start
    for(uint64_t i1 = basePos1, i2=(basePos2 + kmerSize - 1);
	i1 < (basePos1 + kmerSize); i1++, i2--){
      bs1 = bitSeq[i1]; bs2 = bitSeq[i2] ^ compBits;
      if(bs1 != bs2){
	return(false);
      }
    }
  }
  // equal
  return(true);
}

// [[Rcpp::export]]
string getID(){
  return(seqID);
}

// [[Rcpp::export]]
string getShortID(){
  return(seqID.substr(0, seqID.find(' ')));
}

char getBase(uint64_t basePos){
  if(basePos > seqLen){
    return('-');
  }
  if(baseMissing[basePos]){
    return('N');
  }
  return(baseLookup[bitSeq[basePos]]);
}

// Stores a DNA sequence as a bit-packed sequence; A/C/G/T can be
// described/distinguished by bits 1 & 2 (ignoring bit 0):
//
// A = 0x41 [00.]; C=0x43 [01.]; G = 0x47 [11.]; T = 0x54 [10.]
// a = 0x61 [00.]; c=0x63 [01.]; g = 0x67 [11.]; t = 0x74 [10.]
// Complement bases: the upper bit is 'notted'
// A =      [10.]; C=     [11.]; G =      [01.]; T =      [00.]
// a =      [10.]; c=     [11.]; g =      [01.]; t =      [00.]
bool storeBits(string seq, bool silent=false){
  uint64_t seqPos = 0;
  uint64_t arrayPos = 0;
  uint64_t bitPos = 0;
  uint64_t h = 0;
  // set sequence length
  seqLen = seq.length();
  if(seqLen < kmerSize){
    if(!silent){
      cerr << "Warning: sequence '" << getShortID() << "' is shorter than kmer size; not storing\n";
    }
    seqLen = 0;
    return(false);
  }
  hashCount = 0;
  compCount = 0;
  // clear missing bits
  // needed so that missing comparisons work properly
  baseMissing.assign(seqLen, false);
  baseModified.assign(seqLen, false);
  nearMissing.assign(seqLen, false);
  llCache.assign(seqLen, -1);
  cachePrepared = true;
  // reserve enough space to store the sequence, fill with zero
  // [one position per base]
  bitSeq.assign(seqLen, 0);
  if(!silent){
    cerr << " storing sequence \"" << seqID
         << "\" [" << seq.substr(0, (kmerSize < 15 ? kmerSize : 15)) << ".."
         << seq.substr(seqLen - (kmerSize < 15 ? kmerSize : 15)) << "] ("
         << seqLen << " bp) in bit array...";
  }
  for(char ch : seq){
    baseMissing[seqPos] = ((ch == 'N') || (ch == 'n'));
    baseModified[seqPos] = (ch == 'm');
    // set ch to 'C' if it was previously 'm'
    ch = (ch == 'm') ? 'C' : ch;
    // cerr << "  Storing " << bitset<3>(ch & 6) << " -> "
    // 	   << bitset<3>((ch & 6) >> 1) << " to " << seqPos;
    bitSeq[seqPos++] = (ch & 6) >> 1;
    // cerr << " [" << +bitSeq[seqPos - 1] << "]" << endl;
  }
  // fill in nearMissing array
  uint64_t nextMissing = POS_INVALID;
  for(uint64_t i = 0; (i <= (seqLen - kmerSize)); i++){
    bool nearMissingCheck = (baseMissing[i + kmerSize - 1]);
    // if the current position is missing, update the nextMissing position
    nextMissing = (nearMissingCheck) ? (i + kmerSize - 1) : nextMissing;
    // set 'nearMissing' if the next missing position is close enough
    nearMissing[i] = (nextMissing == POS_INVALID) ? 0 : (nextMissing >= i);
  }
  return(true);
}

bool appendBits(string read, bool silent=false){
  uint64_t readPos = seqLen;
  uint64_t arrayPos = 0;
  uint64_t bitPos = 0;
  uint64_t h = 0;
  // set sequence length
  readLen = read.length();
  if(readLen < kmerSize){
    if(!silent){
      cerr << "Warning: read '" << readID
           << "' is shorter than kmer size; not mapping\n";
    }
    return(false);
  }
  hashCount = 0;
  compCount = 0;
  // reserve enough space to store the sequence, fill with zero
  // [one position per base]
  bitSeq.resize(seqLen + 1 + readLen);
  baseMissing.resize(seqLen + 1 + readLen);
  baseModified.resize(seqLen + 1 + readLen);
  if(seqLen > 0){
    baseMissing[readPos++] = true; // insert one N to avoid kmer crossover from ref to read
  }
  for(char ch : read){
    baseMissing[readPos] = ((ch == 'N') || (ch == 'n'));
    baseModified[readPos] = (ch == 'm');
    // set ch to 'C' if it was previously 'm'
    ch = ('C' & -(ch == 'm')) | (ch & -(ch != 'm'));
    bitSeq[readPos++] = (ch & 6) >> 1;
  }
  // fill in nearMissing array
  int64_t nextMissing = -1;
  for(uint64_t i = seqLen - kmerSize + 1; (i <= (seqLen + readLen + 1 - kmerSize)); i++){
    bool nearMissingCheck = (baseMissing[i + kmerSize - 1]);
    // if the current position is missing, update the nextMissing position
    nextMissing = (nearMissingCheck) ? (i + kmerSize - 1) : nextMissing;
    // set 'nearMissing' if the next missing position is close enough
    nearMissing[i] = (nextMissing >= i);
  }
  return(true);
}


// return value: true if a sequence has been loaded / processed
bool storeSequence(FastXFileType ft, bool silent=false){
  seqFormat = ft;
  seqQual.clear();
  switch (ft) {
  case gfa:
    if(fastxBuf[0][0] == 'S'){
      size_t seqIDPos = fastxBuf[0].find('\t', 0) + 1;
      size_t seqPos = fastxBuf[0].find('\t', seqIDPos) + 1;
      // Allow for additional optional fields after the sequence
      size_t seqEndPos = (fastxBuf[0].find('\t', seqPos) != string::npos) ?
        fastxBuf[0].find('\t', seqPos) :
        fastxBuf[0].find('\n', seqPos);
      string seq = fastxBuf[0].substr(seqPos, seqEndPos - seqPos);
      seqID = fastxBuf[0].substr(seqIDPos, seqPos - seqIDPos - 1);
      bool retVal = storeBits(seq, silent=silent);
      fastxBuf.clear();
      return(retVal);
    } // non-sequences will not be processed
    cerr << "Warning: attempted to load something that wasn't a sequence\n";
    break;
  case fasta:
    if(fastxBuf.size() == 2){
      seqID = fastxBuf[0].substr(1);
      bool retVal = storeBits(fastxBuf[1], silent=silent);
      fastxBuf.clear();
      return(retVal);
    } // partially-complete sequences will not be processed
    cerr << "Warning: attempted to load incomplete sequence\n";
    break;
  case fastq:
    if(fastxBuf.size() == 4){
      seqID = fastxBuf[0].substr(1);
      seqQual = fastxBuf[3];
      bool retVal = storeBits(fastxBuf[1], silent=silent);
      fastxBuf.clear();
      return(retVal);
    } // partially-complete sequences will not be processed
    cerr << "Warning: attempted to load incomplete sequence\n";
    break;
  case fastm:
    bool retVal = false;
    for(size_t i = 0; i < fastxBuf.size(); i += 2){
      if(fastxBuf[i].find("#seq:") == 0){
        seqID = fastxBuf[i].substr(5);
        retVal = storeBits(fastxBuf[i+1].substr(1), silent=silent);
      }
      if(fastxBuf[i].find("#qual:") == 0){
        seqID = fastxBuf[i].substr(5);
        seqQual = fastxBuf[i+1].substr(1);
      }
    }
    fastxBuf.clear();
    return(retVal);
    break;
  }
  return(false);
}

// return value: true if a sequence has been loaded / processed
bool appendSequence(FastXFileType ft, bool silent=false){
  seqFormat = ft;
  seqQual.clear();
  switch (ft) {
  case fasta:
    if(fastxBuf.size() == 2){
      seqID = fastxBuf[0].substr(1);
      bool retVal = appendBits(fastxBuf[1], silent=silent);
      fastxBuf.clear();
      return(retVal);
    } // partially-complete sequences will not be processed
    cerr << "Warning: attempted to load incomplete sequence\n";
    break;
  case fastq:
    if(fastxBuf.size() == 4){
      seqID = fastxBuf[0].substr(1);
      seqQual = fastxBuf[3];
      bool retVal = appendBits(fastxBuf[1], silent=silent);
      fastxBuf.clear();
      return(retVal);
    } // partially-complete sequences will not be processed
    cerr << "Warning: attempted to load incomplete sequence\n";
    break;
  case fastm:
    bool retVal = false;
    for(size_t i = 0; i < fastxBuf.size(); i += 2){
      if(fastxBuf[i].find("#seq:") == 0){
        seqID = fastxBuf[i].substr(5);
        retVal = appendBits(fastxBuf[i+1].substr(1), silent=silent);
      }
      if(fastxBuf[i].find("#qual:") == 0){
        seqID = fastxBuf[i].substr(5);
        seqQual = fastxBuf[i+1].substr(1);
      }
    }
    fastxBuf.clear();
    return(retVal);
    break;
  }
  return(false);
}

// return value: true if a sequence has been loaded / processed
bool processFastx(bool dumpRemainder, bool silent=false){
  if(fastxBuf.size() == 0){
    return(false);
  }
  FastXFileType fileType = seqFormat;
  bool retVal;
  if(seqFormat == FastXFileType::none){
    switch (fastxBuf[0][0]) {
    case 'S':
      fileType = gfa;
      break;
    case '#':
      fileType = fastm;
      break;
    case '@':
      fileType = fastq;
      break;
    default:
      fileType = fasta;
    }
  }
  // GFA format expects sequence ID and sequence on the same line
  if((fastxBuf.size() == 1) && (seqFormat != FastXFileType::gfa)){
    return(false);
  }
  if(dumpRemainder){
    retVal = storeSequence(fileType, silent=silent);
    return(retVal);
  } else {
    string lastLine = fastxBuf.back(); fastxBuf.pop_back();
    // FASTQ / FASTM loading code from Booksembler
    // [gitlab.com/gringer/booksembler/-/blob/master/booksembler.cc]
    switch (fileType) {
    case gfa:
      if(lastLine[0] == 'S'){
        fastxBuf.push_back(lastLine); // restore sequence line
        retVal = storeSequence(fileType, silent=silent);
        return(retVal);
      } else {
        return(false);
      }
      break;
    case fasta: // ** fasta logic **
      if(fastxBuf.size() == 1){ // [first] sequence line
        fastxBuf.push_back(lastLine); // restore line
        return(false);
      } else if(lastLine[0] == '>'){
        retVal = storeSequence(fileType, silent=silent);
        fastxBuf.push_back(lastLine); // restore first line of next sequence
        return(retVal);
      } else { // a continuation of the sequence
        fastxBuf.back() += lastLine; // add to sequence line
        return(false);
      }
      break;
    case fastq: // ** fastq logic **
      switch (fastxBuf.size()) {
      case 0: // header line
      case 1: // [first] sequence line
      case 3: // [first] quality line
	fastxBuf.push_back(lastLine);
        return(false);
	break;
      case 2:
	if(lastLine[0] == '+'){ // reached end of sequence
	  fastxBuf.push_back(lastLine);
          return(false);
	} else { // allow multi-line fastq
	  fastxBuf.back() += lastLine; // add to sequence line
          return(false);
	}
	break;
      case 4:
	if(fastxBuf[1].size() <= fastxBuf[3].size()){ // finished sequence
	  retVal = storeSequence(fileType, silent=silent);
	  fastxBuf.push_back(lastLine); // restore first line of next sequence
          return(retVal);
	} else {
	  fastxBuf.back() += lastLine;  // add to quality line
          return(false);
	}
	break;
      default:
	cerr << "Warning: unexpected fastq condition; clearing buffer\n";
	fastxBuf.clear();
        return(false);
      }
      break;
    case fastm: // ** fastm logic **
      if(fastxBuf.size() == 1){ // [first] record line
        fastxBuf.push_back(lastLine); // restore next line
        return(false);
      } else if(lastLine[0] == ' '){ // multi-line; continuation of previous
        fastxBuf.back() += lastLine.substr(1);
        return(false);
      } else if(lastLine[0] == '#'){ // record is complete
        retVal = storeSequence(fileType, silent=silent);
        fastxBuf.push_back(lastLine); // restore first line of next record
        return(retVal);
      } else {
	cerr << "Warning: unexpected fastq condition; clearing buffer\n";
	fastxBuf.clear();
        return(false);
      }
      break;
    }
    cerr << "Warning: unexpected stream condition; clearing buffer\n";
    fastxBuf.clear();
    return(false);
  }
}

// return value: true if a read has been loaded / processed
bool processRead(bool dumpRemainder, bool silent=false){
  if(readBuf.size() == 1){
    return(false);
  }
  FastXFileType fileType;
  bool retVal;
  switch (readBuf[0][0]) {
  case '#':
    fileType = fastm;
    break;
  case '@':
    fileType = fastq;
    break;
  default:
    fileType = fasta;
  }
  if(dumpRemainder){
    retVal = appendSequence(fileType, silent=silent);
    return(retVal);
  } else {
    string lastLine = readBuf.back(); readBuf.pop_back();
    // FASTQ / FASTM loading code from Booksembler
    // [gitlab.com/gringer/booksembler/-/blob/master/booksembler.cc]
    switch (fileType) {
    case fasta: // ** fasta logic **
      if(readBuf.size() == 1){ // [first] sequence line
        readBuf.push_back(lastLine); // restore line
        return(false);
      } else if(lastLine[0] == '>'){
        retVal = appendSequence(fileType, silent=silent);
        readBuf.push_back(lastLine); // restore first line of next sequence
        return(retVal);
      } else { // a continuation of the sequence
        readBuf.back() += lastLine; // add to sequence line
        return(false);
      }
      break;
    case fastq: // ** fastq logic **
      switch (readBuf.size()) {
      case 0: // header line
      case 1: // [first] sequence line
      case 3: // [first] quality line
	readBuf.push_back(lastLine);
        return(false);
	break;
      case 2:
	if(lastLine[0] == '+'){ // reached end of sequence
	  readBuf.push_back(lastLine);
          return(false);
	} else { // allow multi-line fastq
	  readBuf.back() += lastLine; // add to sequence line
          return(false);
	}
	break;
      case 4:
	if(readBuf[1].size() <= readBuf[3].size()){ // finished sequence
	  retVal = appendSequence(fileType, silent=silent);
	  readBuf.push_back(lastLine); // restore first line of next sequence
          return(retVal);
	} else {
	  readBuf.back() += lastLine;  // add to quality line
          return(false);
	}
	break;
      default:
	cerr << "Warning: unexpected fastq condition; clearing buffer\n";
	readBuf.clear();
        return(false);
      }
      break;
    case fastm: // ** fastm logic **
      if(readBuf.size() == 1){ // [first] record line
        readBuf.push_back(lastLine); // restore next line
        return(false);
      } else if(lastLine[0] == ' '){ // multi-line; continuation of previous
        readBuf.back() += lastLine.substr(1);
        return(false);
      } else if(lastLine[0] == '#'){ // record is complete
        retVal = appendSequence(fileType, silent=silent);
        readBuf.push_back(lastLine); // restore first line of next record
        return(retVal);
      } else {
	cerr << "Warning: unexpected fastq condition; clearing buffer\n";
	readBuf.clear();
        return(false);
      }
      break;
    }
    cerr << "Warning: unexpected stream condition; clearing buffer\n";
    readBuf.clear();
    return(false);
  }
}


// [[Rcpp::export]]
bool hasMoreSeqs(){
  return(seqsRemaining);
}


// [[Rcpp::export]]
bool prepareFastxFile(StringVector fileName, uint64_t tKmerSize){
  if(fastaFileFS.is_open()){
    fastaFileFS.close();
    fastaFileFS.clear();
  }
  fastaFileFS.open(fileName[0]);
  fileLoaded = true;
  kmersCounted = false;
  repeatsHashed = false;
  // Reset the sequence variables
  seqID = "";
  bitSeq.clear();
  seqLen = 0;
  kmerSize = tKmerSize;
  kmerSizeM1 = kmerSize - 1;
  kmerBitSize = kmerSize << 1;
  kmerBitSizeM1 = kmerBitSize - 1;
  kmerBitSizeM2 = kmerBitSize - 2;
  kmerMask = (kmerBitSize == 64) ? numeric_limits<uint64_t>::max() :
    ((kmerBitSize < 64) ? ((1L << kmerBitSize) - 1) : HALF_MASK);
  // Clear the sequence buffer
  fastxBuf.clear();
  getline(fastaFileFS, buf);
  //cout << "first line: " << buf << endl;
  seqsRemaining = fastaFileFS.good();
  if(seqsRemaining){
    fastxBuf.push_back(buf);
  }
  return(seqsRemaining);
}

// [[Rcpp::export]]
bool loadNextSequence(bool silent=false){
  if(!fileLoaded){
    cerr << "Error: no file loaded\n";
    return false;
  }
  if(!seqsRemaining){
    cerr << "== no more sequences ==\n";
    return false;
  }
  // Reset the sequence variables
  seqID = "";
  bitSeq.clear();
  seqLen = 0;
  bool shownFirstMessage = false;
  // Keep loading lines from the file until a full sequence record is seen
  while(getline(fastaFileFS, buf)){
    if(buf != ""){
      if(!shownFirstMessage && !silent){
        cerr << "Loading next sequence...";
      }
      fastxBuf.push_back(buf);
      shownFirstMessage = true;
    }
    if(buf == ""){
      cerr << " error: empty line seen\n";
      return(false);
    } else if(processFastx(false, silent=silent)){
      cerr << " done!\n";
      return(true);
    }
  }
  bool retVal = processFastx(true, silent=silent);
  cerr << " done!\n";
  seqsRemaining = false;
  return(retVal);
}

bool loadNextRead(bool silent=false){
  if(!fileLoaded){
    cerr << "Error: no reference file loaded\n";
    return false;
  }
  if(!readsRemaining){
    cerr << "Error: no more reads to process\n";
    return false;
  }
  // Reset the read variables
  readID = "";
  // Keep loading lines from the file until a full sequence record is seen
  while(readFileFS.good()){
    getline(readFileFS, buf);
    if(readFileFS.good()){
      readBuf.push_back(buf);
    }
    if(processRead(!readFileFS.good(), silent=silent)){
      return(true);
    }
  }
  // Reached the end of the file (or other file error)
  return(false);
}

// [[Rcpp::export]]
bool mapReads(StringVector fileName){
  if(seqLen < kmerSize){
    return(false);
  }
  if(readFileFS.is_open()){
    readFileFS.close();
    readFileFS.clear();
  }
  readFileFS.open(fileName[0]);
  // Reset the sequence variables
  readID = "";
  // Clear the map buffer
  readBuf.clear();
  getline(readFileFS, buf);
  //cout << "first line: " << buf << endl;
  readsRemaining = readFileFS.good();
  if(readsRemaining){
    readBuf.push_back(buf);
  }
  bool header=false;
  while(loadNextRead()){
    size_t repeatCount = 0;
    for(uint64_t i = seqLen + 1; (i <= (seqLen + readLen + 1- kmerSize)); i++){
      Kmer tk(i, KT_F);
      auto tkf = kmerStore.find(tk);
      repeatCount += (tkf != kmerStore.end());
    }
    cerr << "Reference: " << getShortID() << "; Read: " << readID << "; Repeat count: " << repeatCount << endl;
  }
  return(true);
}

// Piecewise distance mapping function (split log/linear)
// [[Rcpp::export]]
int64_t pwLogLin(int64_t x, uint64_t pxHeight=1000,
		 bool doLog=true, bool circular=false){
  int sx = sgn(x); x = abs(x);
  if(!doLog || (seqLen <= 100)){
    return((x * pxHeight * 2) / seqLen * sx);
  }
  if(x >= seqLen){
    return((pxHeight * 2) + 1);
  }
  if(x == 0){
    return(0);
  }
  if(!cachePrepared){
    cerr << "Error: cache not properly prepared\n";
    return(-1);
  }
  int64_t res = llCache[x];
  if(res >= 0){
    return(res * sx);
  }
  // optimisation: this branch could be removed
  if(circular && (x > (seqLen / 2))){
   x = (seqLen - x);
   sx = -sx;
  }
  int64_t a;
  double endPos;
  a = seqLen / (25 + (25 * circular));
  endPos = ((seqLen - ((seqLen/2) * circular)) - a) / (a * log(a)) + 1;
  // optimisation: this branch could be removed
  double xtr = (x < a) ?
    // logarithmic up to the split point
    log(x) / log(a) :
    // linear after the split point
    (x-a) / (a * log(a)) + 1;
  xtr = (pxHeight * 2 * xtr) / endPos + 0.5;
  res = floor(xtr);
  llCache[x] = res;
  return(res * sx);
}

// [[Rcpp::export]]
string getKmerF(uint64_t tPos){
  if(tPos > (seqLen - kmerSize)){
    cerr << "Error: request for kmer beyond sequence end\n";
    return("");
  }
  Kmer tk(tPos, KT_F);
  return(tk.getSequence());
}

// [[Rcpp::export]]
string getKmerC(uint64_t tPos){
  if(tPos > (seqLen - kmerSize)){
    cerr << "Error: request for kmer beyond sequence end\n";
    return("");
  }
  Kmer tk(tPos, KT_C);
  return(tk.getSequence());
}

// [[Rcpp::export]]
string getKmerR(uint64_t tPos){
  if(tPos > (seqLen - kmerSize)){
    cerr << "Error: request for kmer beyond sequence end\n";
    return("");
  }
  Kmer tk(tPos, KT_R);
  return(tk.getSequence());
}

// [[Rcpp::export]]
string getKmerRC(uint64_t tPos){
  if(tPos > (seqLen - kmerSize)){
    cerr << "Error: request for kmer beyond sequence end\n";
    return("");
  }
  Kmer tk(tPos, KT_RC);
  return(tk.getSequence());
}


// [[Rcpp::export]]
uint64_t getSeqLen(){
  return(seqLen);
}

// [[Rcpp::export]]
bool hashRepeats(bool silent=false){
  repeatsHashed = false;
  if(seqLen < kmerSize){
    if(!silent){
      cerr << "Error: sequence missing, or sequence is shorter than the kmer length\n";
    }
    return(false);
  }
  urpt=0;
  uorpt=0;
  isRepetitive.clear(); // needed so that missing comparisons work properly
  isRepetitive.resize(seqLen, false);
  isInteresting.clear();
  isInteresting.resize(seqLen, false);
  isComplement.clear();
  isComplement.resize(seqLen, false);
  isReverse.clear();
  isReverse.resize(seqLen, false);
  isRevcomp.clear();
  isRevcomp.resize(seqLen, false);
  repetitivePoss.clear();
  kmerStore.clear();
  kmerStore.reserve(seqLen);
  if(!silent){
    cerr << "Finding forward repeats: |" << setfill('-') << setw(49) << "|"
	 << setfill(' ') << endl
	 << "                         ";
  }
  size_t starsAdded = 0;
  uint64_t basesMod = 0;
  uint64_t propSlice = (seqLen / 50);
  uint64_t repeatCount = 0;
  uint64_t firstPos = 0;
  nextType = KT_F;
  auto t_start = chrono::high_resolution_clock::now();
  uint64_t hashVal;
  auto kS = bitSeq.begin();
  auto kE = bitSeq.begin();
  advance(kE, kmerSize - 1);
  bool lastMissing = true;
  for(uint64_t i = 0; (i <= (seqLen - kmerSize)); i++){
    hashVal = (lastMissing) ? Kmer::makeHash(i, KT_F) :
      Kmer::makeFastHash(i, KT_F, hashVal, kS, kE);
    nextHash = hashVal;
    lastMissing = nearMissing[i];
    nextPos = i;
    if((hashVal & KT_N) == 0){
      hashDiv16Hits++;
    }
    Kmer tkf(i, KT_F);
    auto tki =
     kmerStore.insert(tkf);
    if(!tki.second){
      // kmer already exists, so it's a repeat
      firstPos = tki.first->getPos();
      if(!nearMissing[i]){
	isInteresting[i] = true;
	isRepetitive[i] = true;
	repeatCount++;
	if(!isRepetitive[firstPos]){
	  isInteresting[firstPos] = true;
	  isRepetitive[firstPos] = true;
	  repeatCount++;
	  // add first position to repeat location array
          // optimisation: this makes branch removal difficult
	  repetitivePoss[firstPos].push_back(firstPos);
	}
	// add current position to repeat location array
	repetitivePoss[firstPos].push_back(i);
      }
    }
    if((++basesMod > propSlice) && (starsAdded < 50)){
      R_CheckUserInterrupt();
      basesMod = 0;
      if(!silent){
	cerr << "*";
      }
      starsAdded++;
    }
    if(i > 0){
      kS++;
    }
    kE++;
  }
  while(starsAdded < 50){
    if(!silent){
      cerr << "*";
    }
    starsAdded++;
  }
  if(!silent){
    cerr << endl;
  }
  urpt = repetitivePoss.size();
  if(!silent){
    std::ios oldState(nullptr);
    oldState.copyfmt(std::cerr);
    auto t_end = chrono::high_resolution_clock::now();
    cerr << fixed << setprecision(3)
         << "Done in "
         << chrono::duration<double>(t_end - t_start).count();
    cerr.copyfmt(oldState);
    cerr << " seconds. " << repeatCount << " repetitive " << kmerSize << "-mer";
    if(repeatCount != 1){
      cerr << "s";
    }
    cerr << " found in " << seqLen << " bases"
	 << " [" << urpt << " unique]\n";
    // float hashDivProp = (1600.0 * hashDiv16Hits) / (seqLen - kmerSize);
    // cerr << fixed << setprecision(4)
    //      << "Hash hits for 1/16th of the hash space: "
    //      << hashDiv16Hits << " (" << (hashDivProp/100.0) << " * 1/16) \n";
    // cerr.copyfmt(oldState);
  }
  repeatsHashed = true;
  if(!silent){
    cerr << "Finding other repeats:   |" << setfill('-') << setw(49) << "|"
	 << setfill(' ') << endl
	 << "                         ";
  }
  starsAdded = 0;
  basesMod = 0;
  propSlice = (seqLen / 50);
  repeatCount = 0;
  t_start = chrono::high_resolution_clock::now();
  kS = bitSeq.begin();
  kE = bitSeq.begin();
  advance(kE, kmerSize - 1);
  uint64_t hashValC;
  uint64_t hashValR;
  uint64_t hashValRC;
  lastMissing = true;
  for(uint64_t i = 0; (i <= (seqLen - kmerSize)); i++){
    if(nearMissing[i]){
      lastMissing = true;
      nextPos = POS_INVALID;
      if(i > 0){
        kS++;
      }
      kE++;
      continue;
    }
    if(lastMissing){ // Make hashes using exact/slow method (first position, or after gaps)
      hashValC = Kmer::makeHash(i, KT_C);
      hashValR = Kmer::makeHash(i, KT_R);
      hashValRC = Kmer::makeHash(i, KT_RC);
      lastMissing = false;
    } else { // Make hashes for the subsequent positions using the quick method
      hashValC = Kmer::makeFastHash(i, KT_C, hashValC, kS, kE);
      hashValR = Kmer::makeFastHash(i, KT_R, hashValR, kS, kE);
      hashValRC = Kmer::makeFastHash(i, KT_RC, hashValRC, kS, kE);
    }
    nextPos = i;
    nextHash = hashValC; nextType = KT_C;
    Kmer tkc(i, KT_C); auto tkcf = kmerStore.find(tkc);
    if(tkcf != kmerStore.end()){
      isComplement[i] = true;
      uint64_t firstPos = tkcf->getPos();
      if(!isInteresting[firstPos]){
	// kmer is not yet in repeat array
	isInteresting[firstPos] = true;
	repetitivePoss[firstPos].push_back(firstPos);
      }
      repeatCount++;
    }
    nextHash = hashValR; nextType = KT_R;
    Kmer tkr(i, KT_R); auto tkrf = kmerStore.find(tkr);
    if(tkrf != kmerStore.end()){
      isReverse[i] = true;
      uint64_t firstPos = tkrf->getPos();
      if(!isInteresting[firstPos]){
	// kmer is not yet in repeat array
	isInteresting[firstPos] = true;
	repetitivePoss[firstPos].push_back(firstPos);
      }
      repeatCount++;
    }
    nextHash = hashValRC; nextType = KT_RC;
    Kmer tkrc(i, KT_RC); auto tkrcf = kmerStore.find(tkrc);
    if(tkrcf != kmerStore.end()){
      isRevcomp[i] = true;
      uint64_t firstPos = tkrcf->getPos();
      if(!isInteresting[firstPos]){
	// kmer is not yet in repeat array
	isInteresting[firstPos] = true;
	repetitivePoss[firstPos].push_back(firstPos);
      }
      repeatCount++;
    }
    if((++basesMod > propSlice) && (starsAdded < 50)){
      R_CheckUserInterrupt();
      basesMod = 0;
      if(!silent){
	cerr << "*";
      }
      starsAdded++;
    }
    if(i > 0){
      kS++;
    }
    kE++;
  }
  while(starsAdded < 50){
    if(!silent){
      cerr << "*";
    }
    starsAdded++;
  }
  if(!silent){
    std::ios oldState(nullptr);
    oldState.copyfmt(std::cerr);
    auto t_end = chrono::high_resolution_clock::now();
    cerr << fixed << setprecision(3)
         << "\nDone in "
         << chrono::duration<double>(t_end - t_start).count();
    cerr.copyfmt(oldState);
    cerr << " seconds. " << repeatCount
	 << " repetitive Comp / Rev / RevComp " << kmerSize << "-mer";
    if(repeatCount != 1){
      cerr << "s";
    }
    cerr << " found in " << seqLen << " bases";
    uorpt = repetitivePoss.size();
    cerr << " [" << (uorpt-urpt) << " additionally unique]\n";
  }
  return(true);
}

// [[Rcpp::export]]
bool writeCountOutput(string outFileName, bool append=false){
  if(!repeatsHashed){
    cerr << "Error: repeats have not yet been hashed\n";
    return(false);
  }
  uint64_t countN=0, countAll=0, countF=0, countRC=0, countR=0, countC=0;
  for(uint64_t i = 0; i < seqLen; i++){
    if(nearMissing[i]){
      countN++;
      continue;
    }
    countAll++;
    if(isRepetitive[i]){
      countF++;
    }
    if(isRevcomp[i]){
      countRC++;
    }
    if(isReverse[i]){
      countR++;
    }
    if(isComplement[i]){
      countC++;
    }
  }
  if(!repeatsHashed){
    cerr << "Error: repeats have not yet been hashed\n";
    return(false);
  }
  // output to File
  // Note: cstdio used because it's faster
  // https://stackoverflow.com/a/2085777/3389895
  FILE* of = fopen(outFileName.c_str(), (append) ? "a" : "w");
  if(of == NULL){
    cerr << "Error: File could not be opened for writing\n";
    return(false);
  }
  if(!append){
    fprintf(of, "Name\tseqLen\tuniqueF\tuniqueOther\tkmersWithN\tkmersNotN\tcountF\tcountRC\tcountR\tcountC\n");
  }
  double countAllD = (double) countAll;
  //           ID  SL   UF   UO   N    A    F    RC   R    C
  fprintf(of, "%s\t%ld\t%ld\t%ld\t%ld\t%ld\t%ld\t%ld\t%ld\t%ld\t\n",
	  getShortID().c_str(), seqLen, urpt, uorpt,
	  countN, countAll, countF, countRC, countR, countC);
  fclose(of);
  return(true);
}

// [[Rcpp::export]]
bool writeSequence(){
  bool retVal = false;
  // Note: outputs to standard output, rather than a file
  string seqStr("");
  for(uint64_t i = 0; i < seqLen; i++){
    if((i > 0) && (seqFormat != fastq) && (i % 60 == 0)){
      seqStr += "\n";
    }
    seqStr += getBase(i);
  }
  switch (seqFormat) {
  case fasta:
    printf(">%s\n", seqID.c_str());
    printf("%s\n", seqStr.c_str());
    break;
  case fastq:
    printf("@%s\n", seqID.c_str());
    printf("%s\n", seqStr.c_str());
    printf("+\n");
    printf("%s\n", seqQual.c_str());
    break;
  case fastm:
    printf("#seq:%s\n", seqID.c_str());
    printf(" %s\n", seqStr.c_str());
    printf("#qual:\n");
    printf(" %s\n", seqQual.c_str());
    break;
  }
  return(true);
}

// [[Rcpp::export]]
String writeMaskedSequence(string search = "FCRV"){
  // Write out a sequence, replacing every internally-repetitive base with 'N'
  String outStr = "";
  // Note: outputs to standard output, rather than a file
  string seqStr("");
  size_t continuingRepetitive = 0;
  bool searchFwd, searchRev, searchComp, searchRevC = false;
  for(char ch: search){
    if(ch == 'F'){
      searchFwd = true;
    } else if(ch == 'C'){
      searchComp = true;
    } else if(ch == 'R'){
      searchRevC = true;
    } else if(ch == 'V'){
      searchRev = true;
    }
  }
  for(uint64_t i = 0; i < seqLen; i++){
    if((i > 0) && (seqFormat != fastq) && (i % 60 == 0)){
      seqStr += "\n";
    }
    if((searchFwd && isRepetitive[i]) ||
       (searchRevC && isRevcomp[i]) ||
       (searchRev && isReverse[i]) ||
       (searchComp && isComplement[i])){
      seqStr += "N";
      continuingRepetitive = kmerSize;
    } else if(continuingRepetitive > 0) {
      continuingRepetitive--;
      seqStr += "N";
    } else {
      seqStr += getBase(i);
    }
  }
  switch (seqFormat) {
  case fasta:
    outStr += ">" + seqID + "\n";
    outStr += seqStr + "\n";
    break;
  case fastq:
    outStr += "@" + seqID + "\n";
    outStr += seqStr + "\n";
    outStr += "+\n";
    outStr += seqQual + "\n";
    break;
  case fastm:
    outStr += "#seq:" + seqID + "\n";
    outStr += " " + seqStr + "\n";
    outStr += "#qual:\n";
    outStr += " " + seqQual + "\n";
    printf(" %s\n", seqQual.c_str());
    break;
  }
  return(outStr);
}

// [[Rcpp::export]]
bool filterReads(bool invert=false,
                 double minF=0, double minR=0, double minC=0, double minRC=0,
                 double maxF=1.1, double maxR=1.1, double maxC=1.1, double maxRC=1.1){
  if(!repeatsHashed){
    cerr << "Error: repeats have not yet been hashed\n";
    return(false);
  }
  uint64_t countN=0, countAll=0, countF=0, countRC=0, countR=0, countC=0;
  for(uint64_t i = 0; i < seqLen; i++){
    if(nearMissing[i]){
      countN++;
      continue;
    }
    countAll++;
    if(isRepetitive[i]){
      countF++;
    }
    if(isRevcomp[i]){
      countRC++;
    }
    if(isReverse[i]){
      countR++;
    }
    if(isComplement[i]){
      countC++;
    }
  }
  if(!repeatsHashed){
    cerr << "Error: repeats have not yet been hashed\n";
    return(false);
  }
  bool keep = true;
  bool retVal = false;
  // filter on maximum values
  if( (((maxF > 1.5) && (countF > maxF )) || ((maxF < 1.5) && (countF > (maxF  * countAll)))) ||
      (((maxR > 1.5) && (countR > maxR )) || ((maxR < 1.5) && (countR > (maxR  * countAll)))) ||
      (((maxC > 1.5) && (countC > maxC )) || ((maxC < 1.5) && (countC > (maxC  * countAll)))) ||
      (((maxRC> 1.5) && (countRC> maxRC)) || ((maxRC< 1.5) && (countRC> (maxRC * countAll)))) ){
    keep = false;
  }
  // filter on minimum values
  if( (((minF > 1.5) && (countF < minF )) || ((minF < 1.5) && (countF < (minF  * countAll)))) ||
      (((minR > 1.5) && (countR < minR )) || ((minR < 1.5) && (countR < (minR  * countAll)))) ||
      (((minC > 1.5) && (countC < minC )) || ((minC < 1.5) && (countC < (minC  * countAll)))) ||
      (((minRC> 1.5) && (countRC< minRC)) || ((minRC< 1.5) && (countRC< (minRC * countAll)))) ){
    keep = false;
  }
  if(keep != invert){
    retVal = writeSequence();
  }
  return(retVal);
}

// [[Rcpp::export]]
bool makeRepeatBed(string outFileName, bool writeKmers=true, bool append=false,
		   bool merge=false, bool split=false){
  if(!repeatsHashed){
    cerr << "Error: repeats have not yet been hashed\n";
    return(false);
  }
  // Note: cstdio used because it's faster
  // https://stackoverflow.com/a/2085777/3389895
  FILE* of = fopen(outFileName.c_str(), (append) ? "a" : "w");
  if(of == NULL){
    cerr << "Error: File could not be opened for writing\n";
    return(false);
  }
  if(split){
    for(uint64_t i = 0; (i <= (seqLen - kmerSize)); i++){
      if(nearMissing[i]){
	// ignore Kmers containing missing values
	continue;
      }
      if(isRepetitive[i]){
	if(writeKmers){
	  fprintf(of, "%s\t%lu\t%lu\t%s_FWD\n", seqID.c_str(),
		  i, i + kmerSize, getKmerF(i).c_str());
	} else {
	  fprintf(of, "%s\t%lu\t%lu\tFWD_RPT\n", seqID.c_str(),
		  i, i + kmerSize);
	}
      }
      if(isComplement[i]){
	if(writeKmers){
	  fprintf(of, "%s\t%lu\t%lu\t%s_CMP\n", seqID.c_str(),
		  i, i + kmerSize, getKmerF(i).c_str());
	} else {
	  fprintf(of, "%s\t%lu\t%lu\tCMP_RPT\n", seqID.c_str(),
		  i, i + kmerSize);
	}
      }
      if(isRevcomp[i]){
	if(writeKmers){
	  fprintf(of, "%s\t%lu\t%lu\t%s_RVC\n", seqID.c_str(),
		  i, i + kmerSize, getKmerF(i).c_str());
	} else {
	  fprintf(of, "%s\t%lu\t%lu\tRVC_RPT\n", seqID.c_str(),
		  i, i + kmerSize);
	}
      }
      if(isReverse[i]){
	if(writeKmers){
	  fprintf(of, "%s\t%lu\t%lu\t%s_REV\n", seqID.c_str(),
		  i, i + kmerSize, getKmerF(i).c_str());
	} else {
	  fprintf(of, "%s\t%lu\t%lu\tREV_RPT\n", seqID.c_str(),
		  i, i + kmerSize);
	}
      }
    }
    fclose(of);
    return(true);
  }
  uint64_t repeatStart = 0;
  uint64_t lastRegionEnd = 0;
  for(uint64_t i = 0; (i <= (seqLen - kmerSize)); i++){
    if((isRepetitive[i] || isComplement[i] || isRevcomp[i] || isReverse[i]) || (merge && (lastRegionEnd > i))){
      if(repeatStart == 0){
        repeatStart = i+1;
      }
      if(isRepetitive[i] || isComplement[i] || isRevcomp[i] || isReverse[i]){
	lastRegionEnd = i + kmerSize - 1;
      }
    } else {
      if(repeatStart > 0){
	if(writeKmers){
	  fprintf(of, "%s\t%lu\t%lu\t%s\n", seqID.c_str(),
		  repeatStart-1, i + kmerSize, getKmerF(repeatStart-1).c_str());
	} else {
	  fprintf(of, "%s\t%lu\t%lu\n", seqID.c_str(),
		  repeatStart-1, i + kmerSize);
	}
        repeatStart = 0;
      }
    }
  }
  if(repeatStart > 0){
    if(writeKmers){
      fprintf(of, "%s\t%lu\t%lu\t%s\n", seqID.c_str(),
	      repeatStart-1, seqLen, getKmerF(seqLen - kmerSize).c_str());
    } else {
      fprintf(of, "%s\t%lu\t%lu\n", seqID.c_str(),
	      repeatStart-1, seqLen);
    }
  }
  fclose(of);
  return(true);
}

// [[Rcpp::export]]
// Note: the BedGraph format doesn't allow overlaps, so regions are automatically merged
bool makeRepeatBedGraph(string outFileName, bool append=false){
  if(!repeatsHashed){
    cerr << "Error: repeats have not yet been hashed\n";
    return(false);
  }
  // Note: cstdio used because it's faster
  // https://stackoverflow.com/a/2085777/3389895
  FILE* of = fopen(outFileName.c_str(), (append) ? "a" : "w");
  if(of == NULL){
    cerr << "Error: File could not be opened for writing\n";
    return(false);
  }
  if(!append){
    fprintf(of, "track type=bedGraph name=Repetitive_regions\n");
  }
  vector<bool> dropCoverage(seqLen + 1, false);
  uint64_t regionStart = 0;
  uint64_t currentCoverage = 0;
  int coverageDelta = 0;
  for(uint64_t i = 0; i < seqLen; i++){
    if((i <= (seqLen - kmerSize)) && isRepetitive[i]){
      coverageDelta++;
      dropCoverage[i + kmerSize] = true;
    }
    if(dropCoverage[i]){
      coverageDelta--;
    }
    if((coverageDelta != 0) || (i == (seqLen-1))){
      fprintf(of, "%s\t%lu\t%lu\t%lu\n", seqID.c_str(),
	      regionStart, i, currentCoverage);
      currentCoverage += coverageDelta;
      coverageDelta = 0;
      regionStart = i;
    }
  }
  fclose(of);
  return(true);
}

// [[Rcpp::export]]
uint64_t writeHiCRepeatBins(string outFileBase, uint64_t basesPerChunk=1,
                            uint64_t yBins=1000, bool doLog=true,
                            bool circular=false, bool append=false,
                            uint64_t startBinID=1){
  if(!repeatsHashed){
    cerr << "Error: repeats have not yet been hashed\n";
    return(0);
  }
  // Note: cstdio used because it's faster
  // https://stackoverflow.com/a/2085777/3389895
  FILE* of = fopen((outFileBase + ".bin").c_str(), (append) ? "a" : "w");
  if(of == NULL){
    cerr << "Error: File could not be opened for writing\n";
    return(0);
  }
  uint64_t currentChunk = POS_INVALID;
  uint64_t startPos = 0;
  if(!append){
    // using format from [https://hicmaptools.readthedocs.io/en/latest/format.html]
    fprintf(of, "cbin\tchr\tfrom.coord\tto.coord\n");
    binDeltaPos = startBinID;
    for(uint64_t i = 1; i < (seqLen+1); i++){
      int64_t logChunk = pwLogLin(i, yBins=yBins, doLog=doLog, circular=circular);
      if(logChunk != currentChunk){
        if(currentChunk != POS_INVALID){
          fprintf(of, "%ld\tpwLogLinPos\t%ld\t%ld\n",
                  currentChunk + startBinID, startPos, i);
        }
        startPos = i;
        currentChunk = logChunk;
      }
    }
    startPos = 0;
    startBinID = startBinID + currentChunk;
    binDeltaNeg = startBinID;
    currentChunk = POS_INVALID;
    for(uint64_t i = 1; i < (seqLen+1); i++){
      int64_t logChunk = pwLogLin(i, yBins=yBins, doLog=doLog, circular=circular);
      if(logChunk != currentChunk){
        if(currentChunk != POS_INVALID){
          fprintf(of, "%ld\tpwLogLinNeg\t%ld\t%ld\n",
                  currentChunk + startBinID, startPos, i);
        }
        startPos = i;
        currentChunk = logChunk;
      }
    }
  }
  startBinID = startBinID + (append ? 0 : currentChunk);
  uint64_t nextBinID = startBinID;
  map<char, set<int64_t>> repeatDeltas;
  currentChunk = 0;
  uint64_t startChunk = currentChunk;
  startPos = 0;
  for(uint64_t i = 0; i < seqLen; i++){
    if(i / basesPerChunk > currentChunk){
      fprintf(of, "%ld\t%s\t%ld\t%ld\n",
              nextBinID++,
              seqID.c_str(),
              startPos,
              i);
      startPos = i;
      currentChunk = i / basesPerChunk;
    }
  }
  fprintf(of, "%ld\t%s\t%ld\t%ld\n", nextBinID++, seqID.c_str(), startPos, seqLen);
  fclose(of);
  return(nextBinID);
}

// [[Rcpp::export]]
bool writeHiCRepeatMaps(string outFileBase, uint64_t basesPerChunk=1,
                        uint64_t yBins=1000, bool doLog=true, bool circular=false,
                        bool append=false, uint64_t startBinID=1){
  if(!repeatsHashed){
    cerr << "Error: repeats have not yet been hashed\n";
    return(false);
  }
  // Note: cstdio used because it's faster
  // https://stackoverflow.com/a/2085777/3389895
  FILE* off = fopen((outFileBase + ".fwd.map").c_str(), (append) ? "a" : "w");
  FILE* ofr = fopen((outFileBase + ".rev.map").c_str(), (append) ? "a" : "w");
  FILE* ofc = fopen((outFileBase + ".comp.map").c_str(), (append) ? "a" : "w");
  FILE* ofx = fopen((outFileBase + ".revc.map").c_str(), (append) ? "a" : "w");
  if((off == NULL) || (ofr == NULL) || (ofc == NULL) || (ofx == NULL)){
    cerr << "Error: File(s) could not be opened for writing\n";
    return(false);
  }
  if(!append){
    // using format from [https://hicmaptools.readthedocs.io/en/latest/format.html]
    fprintf(off, "cbin1\tcbin2\texpected_count\tobserved_count\n");
    fprintf(ofr, "cbin1\tcbin2\texpected_count\tobserved_count\n");
    fprintf(ofc, "cbin1\tcbin2\texpected_count\tobserved_count\n");
    fprintf(ofx, "cbin1\tcbin2\texpected_count\tobserved_count\n");
  }
  map<char, multiset<pair<int64_t,int64_t>>> repeatDeltas;
  uint64_t currentChunk = 0;
  for(uint64_t i = 0; i < seqLen; i++){
    currentChunk = i / basesPerChunk;
    if(isRepetitive[i]){
      Kmer tkf(i, KT_F);
      auto tkff = kmerStore.find(tkf); // fetch first repeat loc from kmer store
      for(uint64_t it : repetitivePoss[tkff->getPos()]){
	if(i != it){
          int64_t deltaBin = pwLogLin(it - i, yBins, doLog=doLog, circular=circular);
          deltaBin = abs(deltaBin) + ((deltaBin > 0) ? binDeltaPos : binDeltaNeg);
          repeatDeltas['F'].insert(make_pair(currentChunk, deltaBin));
	}
      }
    }
    if(isComplement[i]){
      Kmer tkc(i, KT_C);
      auto tkf = kmerStore.find(tkc);
      for(uint64_t it : repetitivePoss[tkf->getPos()]){
        int64_t deltaBin = pwLogLin(it - i, yBins, doLog=doLog, circular=circular);
        deltaBin = abs(deltaBin) + ((deltaBin > 0) ? binDeltaPos : binDeltaNeg);
        repeatDeltas['C'].insert(make_pair(currentChunk, deltaBin));
      }
    }
    if(isRevcomp[i]){
      Kmer tkrc(i, KT_RC);
      auto tkf = kmerStore.find(tkrc);
      for(uint64_t it : repetitivePoss[tkf->getPos()]){
        int64_t deltaBin = pwLogLin(it - i, yBins, doLog=doLog, circular=circular);
        deltaBin = abs(deltaBin) + ((deltaBin > 0) ? binDeltaPos : binDeltaNeg);
        repeatDeltas['X'].insert(make_pair(currentChunk, deltaBin));
      }
    }
    if(isReverse[i]){
      Kmer tkr(i, KT_R);
      auto tkf = kmerStore.find(tkr);
      for(uint64_t it : repetitivePoss[tkf->getPos()]){
        int64_t deltaBin = pwLogLin(it - i, yBins, doLog=doLog, circular=circular);
        deltaBin = abs(deltaBin) + ((deltaBin > 0) ? binDeltaPos : binDeltaNeg);
        repeatDeltas['R'].insert(make_pair(currentChunk, deltaBin));
      }
    }
  }
  // output to File
  bool first;
  for(auto it : repeatDeltas){
    switch (it.first) {
    case 'F':
      for(auto itd = it.second.begin(); itd != it.second.end(); ){
        size_t count = it.second.count(*itd);
        fprintf(off, "%ld\t%ld\t1\t%ld\n", itd->first, itd->second, count);
        advance(itd, count);
      }
      fprintf(off, "\n");
      break;
    case 'R':
      for(auto itd = it.second.begin(); itd != it.second.end(); ){
        size_t count = it.second.count(*itd);
        fprintf(ofr, "%ld\t%ld\t1\t%ld\n", itd->first, itd->second, count);
        advance(itd, count);
      }
      fprintf(ofr, "\n");
      break;
    case 'C':
      for(auto itd = it.second.begin(); itd != it.second.end(); ){
        size_t count = it.second.count(*itd);
        fprintf(ofc, "%ld\t%ld\t1\t%ld\n", itd->first, itd->second, count);
        advance(itd, count);
      }
      fprintf(ofc, "\n");
      break;
    case 'X':
      for(auto itd = it.second.begin(); itd != it.second.end(); ){
        size_t count = it.second.count(*itd);
        fprintf(ofx, "%ld\t%ld\t1\t%ld\n", itd->first, itd->second, count);
        advance(itd, count);
      }
      break;
    }
  }
  fclose(off);
  fclose(ofr);
  fclose(ofc);
  fclose(ofx);
  return(true);
}

// [[Rcpp::export]]
bool makeBinnedBed(string outFileName, uint64_t basesPerChunk=1,
                   uint64_t yBins=1000, bool doLog=true, bool circular=false,
                   bool append=false, bool silent=false){
  if(!repeatsHashed){
    cerr << "Error: repeats have not yet been hashed\n";
    return(false);
  }
  string seqShortID = getShortID();
  // Note: cstdio used because it's faster
  // https://stackoverflow.com/a/2085777/3389895
  FILE* of = fopen(outFileName.c_str(), (append) ? "a" : "w");
  if(of == NULL){
    cerr << "Error: File(s) could not be opened for writing\n";
    return(false);
  }
  if(!append){
    // using format from [https://bedtools.readthedocs.io/en/latest/content/general-usage.html]
    fprintf(of, "#hashasher v2.0 [%ld base%s per chunk; kmerSize=%ld bp; ybins=%ld; log=%s; circular=%s]\n",
            basesPerChunk,
            (basesPerChunk == 1) ? "" : "s",
            kmerSize,
            yBins,
            (doLog ? "TRUE" : "FALSE"),
            (circular ? "TRUE" : "FALSE"));
    fprintf(of, "#chr\tstart\tend\trepeatClass\tscore\tdirection\n");
  }
  if(!silent){
    cerr << endl
         << "Reprocessing repeat data into bins: |" << setfill('-') << setw(49) << "|"
	 << setfill(' ') << endl
	 << "                                    ";
  }
  size_t starsAdded = 0;
  uint64_t basesMod = 0;
  uint64_t propSlice = (seqLen / 50);
  map<int64_t, set<pair<string,int64_t>>> repeatDeltas;
  uint64_t currentChunk = 0;
  for(uint64_t i = 0; i < seqLen; i++){
    currentChunk = i / basesPerChunk;
    if(isRepetitive[i]){
      Kmer tkf(i, KT_F);
      auto tkff = kmerStore.find(tkf); // fetch first repeat loc from kmer store
      for(uint64_t it : repetitivePoss[tkff->getPos()]){
	if(i != it){
          int64_t deltaBin = pwLogLin(it - i, yBins, doLog=doLog, circular=circular);
          repeatDeltas[currentChunk].insert(make_pair("F", deltaBin));
	}
      }
    }
    if(isComplement[i]){
      Kmer tkc(i, KT_C);
      auto tkf = kmerStore.find(tkc);
      for(uint64_t it : repetitivePoss[tkf->getPos()]){
        int64_t deltaBin = pwLogLin(it - i, yBins, doLog=doLog, circular=circular);
        repeatDeltas[currentChunk].insert(make_pair("C", deltaBin));
      }
    }
    if(isRevcomp[i]){
      Kmer tkrc(i, KT_RC);
      auto tkf = kmerStore.find(tkrc);
      for(uint64_t it : repetitivePoss[tkf->getPos()]){
        int64_t deltaBin = pwLogLin(it - i, yBins, doLog=doLog, circular=circular);
        repeatDeltas[currentChunk].insert(make_pair("RC", deltaBin));
      }
    }
    if(isReverse[i]){
      Kmer tkr(i, KT_R);
      auto tkf = kmerStore.find(tkr);
      for(uint64_t it : repetitivePoss[tkf->getPos()]){
        int64_t deltaBin = pwLogLin(it - i, yBins, doLog=doLog, circular=circular);
        repeatDeltas[currentChunk].insert(make_pair("R", deltaBin));
      }
    }
    if((++basesMod > propSlice) && (starsAdded < 50)){
      R_CheckUserInterrupt();
      basesMod = 0;
      if(!silent){
	cerr << "*";
      }
      starsAdded++;
    }
  }
  if(!silent){
    while(starsAdded < 50){
      cerr << "*";
      starsAdded++;
    }
    cerr << endl;
  }
  // output to File
  if(!silent){
    cerr << endl
         << "Writing to file:                    |" << setfill('-') << setw(49) << "|"
	 << setfill(' ') << endl
	 << "                                    ";
  }
  starsAdded = 0;
  basesMod = 0;
  propSlice = (repeatDeltas.size() / 50);
  for(auto it : repeatDeltas){
    for(auto itd : it.second){
      uint64_t posEnd = (it.first + 1) * basesPerChunk + kmerSize - 1;
      fprintf(of, "%s\t%ld\t%ld\t%s\t%ld\t%s\n",
              seqShortID.c_str(), // chromosome name
              it.first * basesPerChunk, // region start
              (posEnd > seqLen) ? seqLen : posEnd, // region end
              itd.first.c_str(), // repeat class
              itd.second, // delta bin [i.e. Y plot location]
              (itd.second < 0) ? "-" : "+"); // delta direction
    }
    if((++basesMod > propSlice) && (starsAdded < 50)){
      R_CheckUserInterrupt();
      basesMod = 0;
      if(!silent){
	cerr << "*";
      }
      starsAdded++;
    }
  }
  if(!silent){
    while(starsAdded < 50){
      cerr << "*";
      starsAdded++;
    }
    cerr << endl;
  }
  fclose(of);
  return(true);
}

// [[Rcpp::export]]
bool writeDiffOutput(string outFileName, uint64_t basesPerChunk=1,
		     bool append=false){
  if(!repeatsHashed){
    cerr << "Error: repeats have not yet been hashed\n";
    return(false);
  }
  // Note: cstdio used because it's faster
  // https://stackoverflow.com/a/2085777/3389895
  FILE* of = fopen(outFileName.c_str(), (append) ? "a" : "w");
  if(of == NULL){
    cerr << "Error: File could not be opened for writing\n";
    return(false);
  }
  if(!append){
    fprintf(of, "Name\tBase\tType\tDeltas\n");
  }
  map<char, set<int64_t>> repeatDeltas;
  uint64_t currentChunk = 0;
  cerr << "Finding other repeats:   |" << setfill('-') << setw(49) << "|"
       << setfill(' ') << endl
       << "                         ";
  for(uint64_t i = 0; i < seqLen; i++){
    if(i / basesPerChunk > currentChunk){
      // output to File
      for(auto it : repeatDeltas){
	fprintf(of, "%s\t%lu\t%s\t",
		seqID.c_str(),
		currentChunk * basesPerChunk,
		((it.first == 'X') ? "RC" : string(1, it.first)).c_str());
	bool first=true;
	for(auto itd : it.second){
	  fprintf(of, "%s%ld", (first ? "" : ";"), itd);
	  first = false;
	}
	fprintf(of, "\n");
      }
      currentChunk = i / basesPerChunk;
      repeatDeltas.clear();
    }
    if(isRepetitive[i]){
      Kmer tkf(i, KT_F);
      auto tkff = kmerStore.find(tkf); // fetch first repeat loc from kmer store
      for(uint64_t it : repetitivePoss[tkff->getPos()]){
	if(i != it){
	  repeatDeltas['F'].insert(it - i);
	}
      }
    }
    if(isComplement[i]){
      Kmer tkc(i, KT_C);
      auto tkf = kmerStore.find(tkc);
      for(uint64_t it : repetitivePoss[tkf->getPos()]){
	repeatDeltas['C'].insert(it - i);
      }
    }
    if(isRevcomp[i]){
      Kmer tkrc(i, KT_RC);
      auto tkf = kmerStore.find(tkrc);
      for(uint64_t it : repetitivePoss[tkf->getPos()]){
	repeatDeltas['X'].insert(it - i);
      }
    }
    if(isReverse[i]){
      Kmer tkr(i, KT_R);
      auto tkf = kmerStore.find(tkr);
      for(uint64_t it : repetitivePoss[tkf->getPos()]){
	repeatDeltas['R'].insert(it - i);
      }
    }
  }
  // output to File
  for(auto it : repeatDeltas){
    fprintf(of, "%s\t%lu\t%s\t",
            seqID.c_str(),
            currentChunk * basesPerChunk,
            ((it.first == 'X') ? "RC" : string(1, it.first)).c_str());
    bool first=true;
    for(auto itd : it.second){
      fprintf(of, "%s%ld", (first ? "" : ";"), itd);
      first = false;
    }
    fprintf(of, "\n");
  }
  fclose(of);
  return(true);
}


// [[Rcpp::export]]
List getMissingRanges(){
  vector<int64_t> rangeStarts;
  vector<int64_t> rangeEnds;
  int64_t currentStart = -1;
  if(!repeatsHashed){
    cerr << "Error: repeats have not yet been hashed\n";
    return(false);
  }
  for(uint64_t i = 0; i < seqLen; i++){
    if(nearMissing[i] && (currentStart == -1)){
      // start of a missing region
      currentStart = i;
    } else if(currentStart != -1) {
      // end of a missing region
      rangeStarts.push_back(currentStart);
      rangeEnds.push_back(i - 1);
      currentStart = -1;
    }
  }
  return(List::create(_["starts"] = rangeStarts,
		      _["ends"] = rangeEnds));
}


// [[Rcpp::export]]
List getDiffResults(uint64_t bpc, uint64_t yBins,
		    bool doLog=true, bool circular=false){
  if(!repeatsHashed){
    cerr << "Error: repeats have not yet been hashed\n";
    return(false);
  }
  map<char, unordered_set<int64_t>> repeatDeltas;
  uint64_t currentChunk = 0;
  uint64_t chunkPos = currentChunk * bpc;
  string chunkPosStr = to_string(chunkPos);
  cerr << "Calculating differences: |" << setfill('-') << setw(49) << "|"
       << setfill(' ') << endl
       << "                         ";
  size_t starsAdded = 0;
  uint64_t basesMod = 0;
  uint64_t propSlice = (seqLen / 50);
  vector<char> typeVec;
  vector<int64_t> xVec;
  vector<int64_t> yVec;
  for(uint64_t i = 0; i < seqLen; i++){
    if(i / bpc > currentChunk){
      // add to output array
      for(auto it : repeatDeltas){
	char deltaType = it.first;
	for(auto itd : it.second){
	  typeVec.push_back(deltaType);
	  xVec.push_back(chunkPos);
	  yVec.push_back(itd);
	}
      }
      repeatDeltas.clear();
      currentChunk = i / bpc;
      chunkPos = currentChunk * bpc;
      chunkPosStr = to_string(chunkPos);
    }
    if(isRepetitive[i]){
      Kmer tkf(i, KT_F);
      auto tkff = kmerStore.find(tkf); // fetch first repeat loc from kmer store
      if(tkff == kmerStore.end()){
        cout << endl << "======" << endl;
        cout << "Expected forward kmer not found at position " << i << endl;
        cout << tkf.getSequence();
        cout << endl << "Please report this issue";
        cout << endl << "======" << endl;
      } else {
        for(uint64_t it : repetitivePoss[tkff->getPos()]){
          if(i != it){
            repeatDeltas['F'].insert(pwLogLin(it - i, yBins,
                                              doLog=doLog, circular=circular));
          }
        }
      }
    }
    if(isComplement[i]){
      Kmer tkc(i, KT_C);
      auto tkf = kmerStore.find(tkc);
      if(tkf == kmerStore.end()){
        cout << endl << "======" << endl;
        cout << "Expected complement kmer not found at position " << i << endl;
        cout << tkc.getSequence();
        cout << endl << "Please report this issue";
        cout << endl << "======" << endl;
      } else {
        for(uint64_t it : repetitivePoss[tkf->getPos()]){
          repeatDeltas['C'].insert(pwLogLin(it - i, yBins,
                                            doLog=doLog, circular=circular));
        }
      }
    }
    if(isRevcomp[i]){
      Kmer tkrc(i, KT_RC);
      auto tkf = kmerStore.find(tkrc);
      if(tkf == kmerStore.end()){
        cout << endl << "======" << endl;
        cout << "Expected reverse complement kmer not found at position " << i << endl;
        cout << tkrc.getSequence();
        cout << endl << "Please report this issue";
        cout << endl << "======" << endl;
      } else {
        for(uint64_t it : repetitivePoss[tkf->getPos()]){
          repeatDeltas['X'].insert(pwLogLin(it - i, yBins,
                                            doLog=doLog, circular=circular));
        }
      }
    }
    if(isReverse[i]){
      Kmer tkr(i, KT_R);
      auto tkf = kmerStore.find(tkr);
      if(tkf == kmerStore.end()){
        cout << endl << "======" << endl;
        cout << "Expected reverse kmer not found at position " << i << endl;
        cout << tkr.getSequence();
        cout << endl << "Please report this issue";
        cout << endl << "======" << endl;
      } else {
        for(uint64_t it : repetitivePoss[tkf->getPos()]){
          repeatDeltas['R'].insert(pwLogLin(it - i, yBins,
                                            doLog=doLog, circular=circular));
        }
      }
    }
    if((++basesMod > propSlice) && (starsAdded < 50)){
      R_CheckUserInterrupt();
      basesMod = 0;
      cerr << "*";
      starsAdded++;
    }
  }
  while(starsAdded < 50){
    cerr << "*";
    starsAdded++;
  }
  cerr << endl;
  // add to output array
  cerr << "Hash calculated " << hashCount << " times\n";
  cerr << "Equality compared " << compCount << " times\n";
  cerr << "Converting to DataFrame...";
  for(auto it : repeatDeltas){
    char deltaType = it.first;
    for(auto itd : it.second){
      typeVec.push_back(deltaType);
      xVec.push_back(chunkPos);
      yVec.push_back(itd);
    }
  }
  repeatDeltas.clear();
  cerr << " done!\n";
  return(List::create(_["name"] = seqID.substr(0, seqID.find(' ')),
		      _["len"] = seqLen,
		      _["blockSize"] = bpc,
		      _["chunks"] = DataFrame::create(_["type"] = typeVec,
						      _["x"] = xVec,
						      _["y"] = yVec)));
}

// [[Rcpp::export]]
bool countKmers(bool ordered=true){
  if(!ordered){
    kmerCounts.clear();
    uint64_t maxCount = 0;
    uint64_t maxPos = -1;
    for(uint64_t i = 0; (i <= (seqLen - kmerSize)); i++){
      Kmer tk(i, KT_F);
      kmerCounts[tk]++;
      uint64_t kct = kmerCounts[tk];
      if(kct > maxCount){
	maxCount = kct;
	maxPos = tk.getPos();
      }
    }
    if(maxPos >= 0){
      Kmer tk(maxPos, KT_F);
      cout << "Counted! Max count == " << maxCount << "; " <<
	tk.getSequence() << " [" << maxPos << "]" << endl;
    }
    return(true);
  } else {
    kmerCountsOrdered.clear();
    uint64_t maxCount = 0;
    uint64_t maxPos = -1;
    for(uint64_t i = 0; (i <= (seqLen - kmerSize)); i++){
      Kmer tk(i, KT_F);
      kmerCountsOrdered[tk]++;
      uint64_t kct = kmerCountsOrdered[tk];
      if(kct > maxCount){
	maxCount = kct;
	maxPos = tk.getPos();
      }
    }
    if(maxPos >= 0){
      Kmer tk(maxPos, KT_F);
      cout << "Counted! Max count == " << maxCount << "; " <<
	tk.getSequence() << " [" << maxPos << "]" << endl;
    }
    return(true);
  }
}

// [[Rcpp::export]]
void showKmers(bool ordered=true, size_t limit=30){
  if(!ordered){
    cout << "Kmer count total: " << kmerCounts.size() << endl;
    for(pair<Kmer, uint64_t> ik : kmerCounts){
      cout << "Kmer: "
	   << ik.first.getPos() << "(" << ik.first.getSequence()
	   << " - "
	   << hex << setfill('0') << setw(32)
           << Kmer::makeHash(ik.first.getPos(), ik.first.getType())
	   << ")" << dec << "; count: " << ik.second << endl;
      if(--limit <= 0){
	return;
      }
    }
  } else {
    cout << "Kmer count total: " << kmerCountsOrdered.size() << endl;
    for(pair<Kmer, uint64_t> ik : kmerCountsOrdered){
      cout << "Kmer: " << ik.first.getPos() << "(" << ik.first.getSequence()
	   << " - "
	   << hex << setfill('0') << setw(32)
           << Kmer::makeHash(ik.first.getPos(), ik.first.getType())
	   << ")" << "; count: " << ik.second << endl;
      if(--limit <= 0){
	return;
      }
    }
  }
}


// [[Rcpp::export]]
string getSeqSig(){
  string res = getID() + ":";
  for(uint64_t i = 0; ((i < 32) && (i < seqLen)); i++){
    res += getBase(i);
  }
  if(seqLen > 32){
    if(seqLen > 64){
      res += "...";
    }
    for(uint64_t i = (seqLen < 64) ? 32 : seqLen-32; i < seqLen; i++){
      res += getBase(i);
    }
  }
  if(seqLen > 64){
    res += " (" + to_string(seqLen) + " bases)";
  }
  return(res);
}

// [[Rcpp::export]]
bool ltKmer(uint64_t tPos1, uint64_t tPos2, string type1 = "F"){
  uint64_t or1 =
    (type1 == "F")  ? KT_F  :
    (type1 == "C")  ? KT_C  :
    (type1 == "R")  ? KT_R  :
    (type1 == "RC") ? KT_RC : KT_N;
  Kmer tk1(tPos1, or1);
  Kmer tk2(tPos2, KT_F);
  return(tk1 < tk2);
}

// [[Rcpp::export]]
bool eqKmer(uint64_t tPos1, uint64_t tPos2, string type1 = "F"){
  uint64_t or1 =
    (type1 == "F")  ? KT_F  :
    (type1 == "C")  ? KT_C  :
    (type1 == "R")  ? KT_R  :
    (type1 == "RC") ? KT_RC : KT_N;
  Kmer tk1(tPos1, or1);
  Kmer tk2(tPos2, KT_F);
  return(tk1 == tk2);
}
