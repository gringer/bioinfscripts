#!/usr/bin/env perl
use warnings;
use strict;

use Pod::Usage; ## uses pod documentation in usage code
use Getopt::Long qw(:config auto_help pass_through);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);
use IO::Compress::Gzip; ## for creating demultiplexed files
use File::Spec; ## for getting calling code directory location
use File::Temp; ## for temporary files and directories
use IPC::Open3; ## for redirecting STDERR from called commands
use IPC::Open2; ## for redirecting STDOUT from called commands
use Time::HiRes qw(time); ## for measuring sub-second time
use sigtrap qw(handler signal_handler normal-signals); ## catching interrupts and exiting cleanly

## DENTAL - DEmultiplex Nanopore via TrAined LAST

our $VERSION = "0.4";

=head1 NAME

fastq-dental.pl -- Use LAST to demultiplex and split nanopore reads

=head1 SYNOPSIS

./fastq-dental.pl <directory> [options]

=head2 OPTIONS

=over 2

=item B<-mat> I<<file>>

File containing LAST training matrix. If not present, it will be
created based on the first read batch.

=item B<-muxdir> I<<directory>>

Directory for storing demultiplexed reads (default: I<demultiplexed>).

=item B<-barcode> I<<file>>

Barcode fasta file, used for binning reads.

=item B<-adapter> I<<file>>

Adapter fasta file, used for splitting and/or filtering reads.

=item B<-onlyAdapter> / B<-orient>

Only process adapter sequences (e.g. for read orientation).

=item B<-maxbcdist>

Change the maximum distance that barcodes can be before the read is
considered chimeric (default: 50). This allows an alternative PacBio &
Illumina-like combinatorial barcoding strategy where barcode
combinations span the entire read. Set to -1 to disable chimeric split
barcode detection entirely.

=item B<-bclookup> I<<lookupFile>>

Translate barcodes using demultiplexed IDs from I<lookupFile>. The
file can be either comma or space-separated; the first field in each
line is used as the barcode ID (as produced from demultiplexing
without translation), and the remaining text is used for the
translated name.

=item B<-stringency> I<<float>>

Change the mapping stringency (i.e. LAST argument -D); defaults to
1e6. Lower values use a more stringent mapping, reducing the allowance
for error.

=item B<-help>

Only display this help message.

=back

=head1 DESCRIPTION

DENTAL - DEmultiplex aNd spliT nAnopore reads with LAST.

The entire read is scanned for potential barcode and adapter
sequences, which are used to distribute reads into barcode bins, and
split reads based on identified read boundaries.

=head1 DETAIL

=over 2

=item Barcodes

Multiple barcodes within 50bp of each other will be combined together
I<in order> for read binning. Chimeric reads with multiple barcodes
that are separated by more than 50bp will be binned into an additional
"BCchim" fastq file. Reads with I<no> barcodes will be binned into
their own separate "BCnone" fastq file. Reads with no detected
barcodes or adapters will be binned into their own separate
"BCnoadapt" fastq file.

A default nanopore barcode file will be used if not specified. Use
-nobarcode if this shouldn't be done (TODO: not currently
implemented).

If the demultiplexing output directory already contains demultiplexed
reads in it, the new reads will be appended to the end of existing
files.

Note: for proper function, barcode sequences should be at least 20bp,
should represent the variant portion of the barcode, and should not
overlap with any other adapter sequences.

For the barcodes, everything up to the first underscore in the header
is treated as the barcode key that will be used in output file names,
and everything after that up to the first space (or end of line) is
used to define a specific barcode.

If you're unsure about the correct structure of the barcodes and
adapters, you can either exclude the adapter sequences entirely, or
inspect a few of the reads to try to work it out. Here is an example
one-liner that will give the 10 most frequently occurring prefixes and
suffixes of a barcode sequence:

=over 2

$ zcat reads.fastq.gz | grep '<barcode_sequence>' |
  perl -pe 's/^.*?((.{,10})(<barcode_sequence>)(.{,10})).*$/\2-$3-$4/' |
  sort | uniq -c | sort -rn | head -n 10

=back

=item Adapters

Adapter names can be given additional qualifiers attached to the
sequence name in brackets to define how adapters should appear in
reads:

    >sequence_id [qual1;qual2;...;qualN]

    * F - sequence appears on the forward strand of a read (e.g. VNP)
    * R - sequence appears on the reverse strand of a read (e.g. SSP)
    * U - sequence is essential and appears once per read (e.g. Y-adapter)
    * S - reads should be split and/or trimmed to exclude this sequence
    * M-nnnn - adapter is followed by a molecular identifier with form nnnn
      [nnnn accepts any base characters or ambiguous bases]

Where adapter use is inconsistent with the rules, reads will be binned
into an additional "inconsistent" bin. Adapters defined as unique (U)
may be used for read splitting.

If there are multiple adapters with the I<U> flag, any one of them alone
will be accepted as a valid read

Note: for proper function, adapter sequences should not overlap with
any other adapter sequences, and should not include any substantial
low-complexity portions (e.g. polyT >12bp).

[TODO: only F/R have been implemented]

=back

=cut

my $verbose = 0;
my $batchSize = 10000; ## process 10000 reads at once initially
my $maxBatchSize = 1000000; ## maximum number of reads to process in a single batch
my $readCount = 0;
my $adapterOnly = 0;
my $numThreads = 12;
my $stringency = "1e6";

my ($volume, $directory, $file) = File::Spec->splitpath(__FILE__);
my $barcodeFileName = ${directory}."/dental_db/barcode_base.fa";
my $adapterFileName = ${directory}."/dental_db/adapter_seqs.fa";

my $tmpOutDir = File::Temp -> newdir("dentalXXXXXX");
my $trainMatName = my $origTrainMatName = ${tmpOutDir}."/trained.mat";
my $muxDir = "demultiplexed";
my $ignoreQuals = 0;
my $maxBCDist = 50;
my $bcLookupFileName = "";
my %bcLookup = ();

sub getMeanQual {
  my ($qual) = @_;
  if(length($qual) == 0){
    return(0);
  }
  my $qBase = 33;
  my @qspl = split("", $qual);
  my $qualTotal = 0;
  my $qualCount = 0;
  foreach my $qual (@qspl) {
    $qualTotal += 10 ** (-(ord($qual) - $qBase) / 10);
    $qualCount++;
  }
  return(sprintf("%0.1f", (-10 * log($qualTotal / $qualCount) / log(10))));
}

sub signal_handler {
  my ($sigName) = @_;
  printf(STDERR "Interrupt received (SIG%s); deleting temporary files... ", $sigName);
  File::Temp::cleanup();
  print(STDERR "done.\n");
  exit(1);
};

sub rc {
  my ($seq) = @_;
  # work on both unmasked and masked sequences
  $seq =~ tr/ACGTUYRSWMKDVHBXNacgtuyrswmkdvhbxn/TGCAARYSWKMHBDVXNtgcaaryswkmhbdvxn/;
  return(scalar(reverse($seq)));
}

GetOptions("verbose!" => \$verbose, "batch=i" => \$batchSize,
           "maxbatch=i" => \$maxBatchSize, "onlyAdapter|orient!" => \$adapterOnly,
           "mat=s" => \$trainMatName, "muxdir=s" => \$muxDir,
           "stringency=s" => \$stringency,
           "threads=i" => \$numThreads,
           "bclookup=s" => \$bcLookupFileName,
           "barcode=s" => \$barcodeFileName, "ignorequals!" => \$ignoreQuals,
           "adapter=s" => \$adapterFileName, "maxbcdist=i" => \$maxBCDist)
    or pod2usage(1);

if($adapterOnly && ($muxDir eq "demultiplexed")){
  $muxDir = "oriented";
}

if(!($trainMatName eq $origTrainMatName)){
  if(-e $trainMatName){
    printf(STDERR "Using existing trained matrix: %s\n", $trainMatName);
  } else {
    printf(STDERR "Saving trained matrix here: %s\n", $trainMatName);
  }
}

if(!(-d $muxDir)){
  if(-e $muxDir){
    pod2usage("Error: specified demultiplex directory '$muxDir' is not a directory\n");
  } else {
    mkdir($muxDir);
  }
}

if($bcLookupFileName){
  if(-e $bcLookupFileName){
    # read sequence IDs from input file
    printf(STDERR "Attempting to read barcode lookups ".
           "from '$bcLookupFileName'\n");
    my $bcLookupFile = new IO::Uncompress::Gunzip "$bcLookupFileName" or
        pod2usage("Unable to open $bcLookupFileName\n");
    while(<$bcLookupFile>){
      chomp;
      if (/^(.+?)(\s+|,)(.+)$/){
        $bcLookup{$1} = $3;
      }
    }
    close($bcLookupFile);
  } else {
    pod2usage("Error: barcode lookup file '$bcLookupFileName' ".
              "can't be found\n");
  }
}

# unknown commands are expected to be files or directories
my @files = ();
my @dirs = ();
while(@ARGV){
  my $arg = shift(@ARGV);
  if(-e $arg){
    push(@files, $arg);
  } elsif(-d $arg) {
    push(@files, $arg);
  } else {
    pod2usage("Error: '$arg' is not a file, directory, or ".
          "command line argument\n");
  }
}

@ARGV = @files;

# use stdin if no files supplied
if(!@ARGV){
  @ARGV = '-' unless (-t STDIN);
}

## test to make sure LAST is installed
my $cmdOut = qx(lastal -V);
if(!$cmdOut || ($cmdOut !~ /^lastal/)){
  pod2usage("Error: LAST is not installed; cannot continue\n");
} else {
  print(STDERR "LAST installed: ${cmdOut}\n");
}

## Parse barcode and adapter files; combine into new file
print(STDERR "Parsing barcodes and adapters... ");
my %barcodes = ();
my %adapters = ();
my %adapterModes = ();

my $seqName = "";
my $searchSeqs = "";

if(-e $adapterFileName){
  open(my $adapterFile, $adapterFileName) or die "Could not open $adapterFileName: $!";
  while(<$adapterFile>){
    chomp; chomp;
    if(/^>(.*?)(\s|$)(.*\[(.*?)\])?/){
      $seqName = $1;
      if(defined($4)){ # check for adapter flag sequences
        my @flags = split(/;/, $4);
        foreach my $flag (@flags) {
          if($flag =~ /^M-(.*)$/){
            $adapterModes{$seqName}{"M"} = $1;
          } else {
            $adapterModes{$seqName}{$flag} = 1;
          }
        }
      }
    $adapters{$seqName} = "";
      ## attach identifier to adapter sequence
      s/^>/>:ad:/;
      $searchSeqs .= $_ . "\n";
    } elsif ($seqName){
    $searchSeqs .= $_ . "\n";
    $adapters{$seqName} .= $_;
    }
  }
  close($adapterFile);
}

#if(!$adapterOnly){
  $seqName = "";
  open(my $barcodeFile, $barcodeFileName) or die "Could not open $barcodeFileName: $!";
  while(<$barcodeFile>){
    chomp; chomp;
    if(/^>(.*)?(\s|$)(.*\[(.*?)\])?/){
      $seqName = $1;
      my $flags = $2;
      $barcodes{$seqName} = "";
      ## attach identifier to barcode sequence
      s/^>/>:bc:/;
      $searchSeqs .= $_ . "\n";
    } elsif ($seqName){
      $searchSeqs .= $_ . "\n";
      $barcodes{$seqName} .= $_;
    }
  }
  close($barcodeFile);
#}

## write merged sequence to combined file
my $searchFileName = ${tmpOutDir}."/search_seqs.fa";
open(my $searchFile, '>', $searchFileName) or die "Could not write to $searchFileName: $!";
print($searchFile $searchSeqs);
close($searchFile);

## create LAST index
$cmdOut = qx(lastdb -uRY4 -R01 ${searchFileName} ${searchFileName});
if(-e "${searchFileName}.suf"){
  print(STDERR "done; successfully generated LAST database.\n");
} else {
  pod2usage("Error: LAST adapter database couldn't be generated; cannot continue\n");
}

sub processSeq {
  my ($shortID, $seqID, $seq, $qual, $dataStoreRef) = @_;
  $seqID =~ s/\s+$//; # remove trailing whitespace
  if($qual){
    $dataStoreRef -> {$shortID}{"header"} = sprintf("@%s", $seqID);
    $dataStoreRef -> {$shortID}{"seq"} = $seq;
    $dataStoreRef -> {$shortID}{"qual"} = $qual;
    return 1;
  } else {
    return 0;
  }
}

## (1) Process files $batchSize reads at a time
sub batchProcess {
  my ($batchIDRef, $dataStoreRef, $bcCountRef) = @_;
  my $batchFQ = "";
  my %intBcCounts = ();
  my $startTime = 0;
  my $readsToProcess = scalar(@{$batchIDRef});
  my $readCountFrag = $readsToProcess / 10;
  foreach my $batchID (@{$batchIDRef}){
    $batchFQ .= $dataStoreRef -> {$batchID}{"header"} . "\n" .
                $dataStoreRef -> {$batchID}{"seq"} . "\n+\n" .
                $dataStoreRef -> {$batchID}{"qual"} . "\n";
  }
  if(!(-e $trainMatName)){
    $startTime = time;
    printf(STDERR "Training LAST using first read batch (%d reads):\n", scalar(@{$batchIDRef}));
    # my $trainFileName = ${tmpOutDir}."/first_batch.fq";
    # open(my $trainFile, '>', $trainFileName) or die "Could not write to $trainFileName: $!";
    # print($trainFile $batchFQ);
    # close($trainFile);
    my ($lastInput, $lastOutput);
    my $pid = open2($lastOutput, $lastInput,
        	    "last-train", "-Q", "1", "-P", $numThreads, $searchFileName);
    print($lastInput $batchFQ);
    close($lastInput);
    my $lastLineSeen = 0;
    my $trainedOutput = "";
    while(<$lastOutput>){
      # report progress on seeing 'percent identity' line
      if(/^# substitution percent identity/){
        print(STDERR "  ".$_);
      }
      if(/^#last/){
        $lastLineSeen = 1;
      }
      # on seeing lines beginning with '#last', write remainder to training matrix
      if($lastLineSeen){
        $trainedOutput .= $_;
      }
    }
    waitpid($pid, 0);
    my $child_exit_status = $? >> 8;
    close($lastOutput);
    my $timeDiff = time - $startTime;
    printf(STDERR "Done - trained first read batch in %0.1f seconds.\n", $timeDiff);
    # write last-train output to matrix file
    open(my $trainMatFile, '>', $trainMatName) or die "Could not write to $trainMatName: $!";
    print($trainMatFile $trainedOutput);
    close($trainMatFile);
  }
  ## (2) Feed reads through LAST
  my %seenBarcodes = ();
  my %seenAdapters = ();
  my %readLayout = ();
  my %readLengths = ();
  if($verbose){
    printf(STDERR "Mapping next read batch (%d reads):\n", scalar(@{$batchIDRef}));
  } else {
    printf(STDERR "Mapping next read batch (%d reads)... ", scalar(@{$batchIDRef}));
  }
  $startTime = time;
  my ($lastInput,$lastOutput);
  my $mappedOutput = "";
  my $readID = "";
  my $adapterID = "";
  my $barcodeID = "";
  my $aStart = 0;
  my $aEnd = 0;
  my $aMatchLen = 0;
  my $rStart = 0;
  my $rEnd = 0;
  my $rLen = 0;
  my $rMatchLen = 0;
  my $rStrand = "+";
  my $currentFragCount = 0;
  ## (3) Process LAST results
  my $readTempName = ${tmpOutDir}."/batch.fq";
  open(my $readTempFile, '>', $readTempName) or die "Could not write to $readTempName: $!";
  print($readTempFile $batchFQ);
  close($readTempFile);
  my $pid = open2($lastOutput, $lastInput,
        	  "lastal", "--split", "-p", $trainMatName,
                  "-D", $stringency,
                  "-P", $numThreads, $searchFileName, $readTempName);
  #print($lastInput $batchFQ);
  #close($lastInput);
  while(<$lastOutput>){
    if(/^s/){
      my @F = split(/\s+/);
      $currentFragCount++;
      if($currentFragCount > $readCountFrag){
        print(STDERR ".");
        $currentFragCount = 0;
      }
      if($F[1] =~ /^:ad:(.*)/){
        $adapterID = $1;
        $aStart = $F[2];
        $aMatchLen = $F[3];
        $aEnd = $aStart + $aMatchLen;
      } elsif($F[1] =~ /^:bc:(.*)/){
        $barcodeID = $1;
        $aStart = $F[2];
        $aMatchLen = $F[3];
        $aEnd = $aStart + $aMatchLen;
      } else {
        $readID = $F[1];
        $rStart = $F[2];
        $rMatchLen = $F[3];
        $rEnd = $rStart + $rMatchLen;
        $rStrand = $F[4];
        $rLen = $F[5];
        $readLengths{$readID} = $rLen;
        ## (4) Collate and combine back
        if($rStrand eq "-"){ ## correct for reverse complement
          $rEnd = $rLen - $rStart;
          $rStart = $rEnd - $rMatchLen;
        }
        if($barcodeID){
          ## ignore any _extra_stuff_after_the_barcode_name for database entry
          my $shortBarcode = ($barcodeID =~ s/_.*$//r);
          $seenBarcodes{$readID}{$shortBarcode} .= $rStrand;
          $readLayout{$readID}{$rStart} = sprintf("%d:%s:bc:%s", $rEnd, $rStrand, $shortBarcode);
        }
        if($adapterID){
          $seenAdapters{$readID}{$adapterID} .= $rStrand;
          $readLayout{$readID}{$rStart} = sprintf("%d:%s:ad:%s", $rEnd, $rStrand, $adapterID);
        }
        $barcodeID = "";
        $adapterID = "";
      }
    }
  }
  waitpid($pid, 0);
  close($lastOutput);
  my %adapterCounts = ();
  my $readCountBC = 0;
  my $readCountAd = 0;
  ## collect up barcode and adapter counts
  my %readBarcodes = ();
  my %foundBCs = ();
  my %foundLengths = ();
  my %foundQuals = ();
  my %foundUMIs = ();
  my $layoutInfo = "";
  ## (5) Identify adapter/barcode locations
  foreach my $readID (@{$batchIDRef}){
    my $bcDef = "BCnoadapt";
    my $shouldRC = 0;
    my $shouldNotRC = 0;
    my $RCambiguous = 0;
    my $rcSet = 0;
    my $UMIpattern = "";
    if(exists($seenAdapters{$readID})){
      foreach my $adapterID (keys %{$seenAdapters{$readID}}){
        $adapterCounts{$adapterID}++;
      }
      $readCountAd++;
      $bcDef = "BCnone";
    }
    my $lastBCPos = 0;
    my $layoutInfo = "";
    my @bcNames = ();
    my @bcDirs = ();
    $foundBCs{"BCnoadapt"} = 1;
    if(exists($readLayout{$readID})){
      foreach my $startPos (sort { $a <=> $b } (keys %{$readLayout{$readID}})){
        my $endData = $readLayout{$readID}{$startPos};
        if($endData =~ /([0-9]+):(.):ad:(.*)/){
          my $endPos = $1;
          my $adStrand = $2;
          my $adapterName = $3;
          my $oldRCRecommendation =
              ($shouldRC && !$shouldNotRC) ? "-" :
              (!$shouldRC && $shouldNotRC) ? "+" : ".";
          if(exists($adapterModes{$adapterName})){
            if(exists($adapterModes{$adapterName}{"F"})){
              $endData .= "{F}";
              $shouldRC = ($adStrand eq "-");
              $shouldNotRC = ($adStrand eq "+");
            }
            if(exists($adapterModes{$adapterName}{"R"})){
              $endData .= "{R}";
              $shouldRC = ($adStrand eq "+");
              $shouldNotRC = ($adStrand eq "-");
            }
            if(exists($adapterModes{$adapterName}{"M"})){
              $endData .= "{UMI}";
              $UMIpattern = $adapterModes{$adapterName}{"M"};
              $UMIpattern =~ s/V/[ACG]/g;
              $UMIpattern =~ s/B/[CGT]/g;
              $UMIpattern =~ s/D/[AGT]/g;
              $UMIpattern =~ s/H/[ACT]/g;
              $UMIpattern =~ s/N/[ACGT]/g;
            }
          }
          my $newRCRecommendation =
              ($shouldRC && !$shouldNotRC) ? "-" :
              (!$shouldRC && $shouldNotRC) ? "+" : ".";
          if(($oldRCRecommendation ne ".") && ($oldRCRecommendation ne $newRCRecommendation)){
            $RCambiguous = 1;
          }
        }
        if($endData =~ /:(.):bc:(.*)/){
          my $bcStrand = $1;
          my $barcodeName = $2;
          if(!($bcDef eq "BCchim")){
            if(($bcDef eq "BCnone") || ($bcDef eq "BCnoadapt")){
              # first barcode seen
              $bcDef = $barcodeName;
              push(@bcNames, $barcodeName);
              push(@bcDirs, $bcStrand);
            } else {
              if(scalar(@bcNames) == 0){
                push(@bcNames, $barcodeName);
                push(@bcDirs, $bcStrand);
                $bcDef = "BCchim";
              } elsif(($bcNames[$#bcNames] eq $barcodeName) && ($bcDirs[$#bcDirs] ne $bcStrand)){
                # next barcode, but looks like reverse complement of last barcode
                pop(@bcNames);
                pop(@bcDirs);
              } else {
                # next barcode
                push(@bcNames, $barcodeName);
                push(@bcDirs, $bcStrand);
                # close enough for combinatorial barcoding
                if(($maxBCDist == -1) || ($startPos - $lastBCPos < $maxBCDist)){
                  $bcDef .= "_".$barcodeName;
                } else {
                  # too far, and unexpected
                  $bcDef = "BCchim";
                }
              }
            }
          }
          $lastBCPos = $startPos;
        }
        my $layoutStr = $startPos."-".$endData;
        $layoutInfo .= (!$layoutInfo) ? $layoutStr : ";".$layoutStr;
      }
    }
    if($layoutInfo){
      $dataStoreRef -> {$readID}{"header"} .= " [" . $layoutInfo . ']';
    }
    if($shouldRC){
      $dataStoreRef -> {$readID}{"header"} .= " [rc]";
      $dataStoreRef -> {$readID}{"seq"} = rc($dataStoreRef -> {$readID}{"seq"});
      $dataStoreRef -> {$readID}{"qual"} = scalar(reverse($dataStoreRef -> {$readID}{"qual"}));
    }
    if(!($bcDef eq "BCchim") && 
       !($bcDef eq "BCnoadapt")){
      if($RCambiguous){
        $dataStoreRef -> {$readID}{"header"} .= " [ambiguous]";
        $bcDef = "BCambig";
      } elsif($adapterOnly && (!$shouldRC && !$shouldNotRC)){
        $bcDef = "BCundirected";
      } elsif($adapterOnly){
        $bcDef = "BCoriented";
      }
    }
    if($UMIpattern){
      if(($dataStoreRef -> {$readID}{"seq"}) =~ /($UMIpattern)/){
        $foundUMIs{$readID} = $1;
        $dataStoreRef -> {$readID}{"header"} .= " [UMI=" . $1 . "]";
      }
    }
    if(exists($bcLookup{$bcDef})){
      $bcDef = $bcLookup{$bcDef};
    }
    $readBarcodes{$readID} = $bcDef;
    $intBcCounts{$bcDef}++;
    $bcCountRef -> {$bcDef}++;
    $foundBCs{$bcDef} = 1;
  }
  ## (6) Split and output
  ## Prepare demultiplexed output read files
  foreach my $bcName (keys %foundBCs){
    my $bcFile = new IO::Compress::Gzip("${muxDir}/reads_${bcName}.fq.gz",
        				Append => 1);
    $foundBCs{$bcName} = $bcFile;
  }
  printf(STDERR " writing to %d read files...", scalar(keys(%foundBCs)));
  ## Write reads out to files
  foreach my $readID (@{$batchIDRef}){
    my $readSeq = $dataStoreRef -> {$readID}{"header"} . "\n" .
                  $dataStoreRef -> {$readID}{"seq"} . "\n+\n" .
        $dataStoreRef -> {$readID}{"qual"} . "\n";
    my $bcName = "BCnoadapt";
    if(exists($readBarcodes{$readID})){
      $bcName = $readBarcodes{$readID};
    }
    my $bcFile = $foundBCs{$bcName};
    if($bcFile){
      print($bcFile $readSeq);
    } else {
      warn(sprintf("Unable to save read '%s' to file for barcode '%s'",
                   $readID, $bcName));
    }
  }
  foreach my $bcName (keys %foundBCs){
    close($foundBCs{$bcName});
  }
  ## Write UMI information out to UMI file (if present)
  if($adapterOnly){
    my $umiFile = new IO::Compress::Gzip("${muxDir}/UMIs_all.txt.gz",
                                         Append => 1);
    foreach my $readID (keys %foundUMIs){
      printf($umiFile "%s %s\n", $readID, $foundUMIs{$readID});
    }
    close($umiFile);
  }
  ## Prepare demultiplexed output length files
  foreach my $bcName (keys %foundBCs){
    my $lengthFile = new IO::Compress::Gzip("${muxDir}/lengths_${bcName}.txt.gz",
        				    Append => 1);
    $foundLengths{$bcName} = $lengthFile;
  }
  printf(STDERR " writing to %d length files...", scalar(keys(%foundLengths)));
  ## Write lengths out to files
  foreach my $readID (@{$batchIDRef}){
    my $meanQual = ($ignoreQuals) ? "" :
        (" q".getMeanQual($dataStoreRef -> {$readID}{"qual"}));
    my $bcName = "BCnoadapt";
    if(exists($readBarcodes{$readID})){
      $bcName = $readBarcodes{$readID};
    }
    if(exists($readLengths{$readID})){
      my $lengthFile = $foundLengths{$bcName};
      if($lengthFile){
        printf($lengthFile "%-8d%s %s\n", $readLengths{$readID},
               $meanQual, $readID);
      } else {
        warn(sprintf("Unable to save length of read '%s' to length ".
                     "file for barcode '%s'", $readID, $bcName));
      }
    } else {
      my $lengthFile = $foundLengths{"BCnoadapt"};
      if($lengthFile){
        printf($lengthFile "%-8d%s %s\n",
               length($dataStoreRef -> {$readID}{"seq"}), $meanQual, $readID);
      } else {
        warn(sprintf("Unable to save length of read '%s' to length ".
                     "file for BCnoadapt", $readID));
      }
    }
  }
  foreach my $bcName (keys %foundLengths){
    close($foundLengths{$bcName});
  }
  %foundBCs = ();
  %foundLengths = ();
  if($verbose){
    print(STDERR "  Barcode counts (deduplicated per read):\n");
    while (my ($key, $value) = each (%intBcCounts)){
      printf(STDERR "    %s: %d\n", $key, $value);
    }
    printf(STDERR "  Total reads seen with any barcodes: %d\n", $readCountBC);
    print(STDERR "  Adapter counts (deduplicated per read):\n");
    while (my ($key, $value) = each (%adapterCounts)){
      printf(STDERR "    %s: %d\n", $key, $value);
    }
    printf(STDERR "  Total reads seen with any adapters: %d\n", $readCountAd);
  } else {
    my $timeDiff = time - $startTime;
    printf(STDERR " done in %0.1f seconds.\n", $timeDiff);
  }
  my $child_exit_status = $? >> 8;
}


my $inQual = 0; # false
my $seqID = "";
my $shortID = "";
my $qualID = "";
my $seq = "";
my $qual = "";
my $barcodes = "";
my $containsFQ = 0; # false
my $duplicateCount = 0;
my $processedCount = 0;
my $batchReadCount = 0;
my $seqStr = "";

my @batchIDs = ();
my %dataStore = ();
my %barcodeCounts = ();
my @processedIDs = ();

foreach my $file (@ARGV) {
  my $z = IO::Uncompress::Gunzip->new($file, MultiStream => 1) or
    pod2usage("gunzip failed: $GunzipError\n");
  while(<$z>){
    chomp; chomp;
    if (!$inQual) {
      if(/\x00/){ ## detect corrupt files, wait for next good read
        $seqID =~ s/\s.*$//;
        print(STDERR "Warning: corrupt sequence found at $seqID [NUL]\n");
        undef($seqID);
        undef($qualID);
        undef($seq);
        undef($qual);
        next;
      } elsif (/^(>|@)((.+?)(\s.*?\s*)?)$/) { ## fastq header line
        if($1 eq ">"){
          pod2usage("Error: '>' detected in sequence header; ".
        	    "fastq-dental.pl only works with fastq files\n");
        }
        my $newSeqID = $2;
        my $newShortID = $3;
        if ($seqID) {
          $seqID =~ s/\s.*$//;
          print(STDERR "Warning: corrupt sequence found at $seqID [NOQUAL]\n");
          undef($seqID);
          undef($qualID);
          undef($seq);
          undef($qual);
        }
        $seqID = $newSeqID;
        $shortID = $newShortID;
        undef($seq);
        undef($qual);
      } elsif (/^\+(.*)$/) { ## sequence / quality separator
        $inQual = 1;     # true
        $containsFQ = 1; # true
        $qualID = $1;
      } else {
        if(/@/){
          $seqID =~ s/\s.*$//;
          print(STDERR "Warning: corrupt sequence found at $seqID ".
                "[header in sequence string]\n");
          undef($seqID);
          undef($qualID);
          undef($seq);
          undef($qual);
          next;
        } else {
          $seq .= $_;
        }
      }
    } else { ## closes if(!inQual)
      if(/\x00/){ ## detect corrupt files, wait for next good read
        $seqID =~ s/ .*$//;
        print(STDERR "Warning: corrupt sequence found at $seqID [NUL]\n");
        undef($seqID);
        undef($qualID);
        undef($seq);
        undef($qual);
        $inQual = 0; # false
      } else {
        $qual .= $_;
        if (length($qual) > (length($seq) + 2)) {
          $seqID =~ s/\s.*$//;
          print(STDERR "Warning: corrupt sequence found at $seqID ".
                "[quality string too long]\n");
          undef($seqID);
          undef($qualID);
          undef($seq);
          undef($qual);
          $inQual = 0; # false
        } elsif (length($qual) >= length($seq)) {
          ## Reached the end of a quality string
          $shortID = $seqID;
          $shortID =~ s/\s.*$//;
          if(exists($dataStore{$shortID})){
              print(STDERR "Warning: duplicate read found in input with ".
        	    "sequence ID $shortID; this will be replaced\n");
          }
          if(processSeq($shortID, $seqID, $seq, $qual, \%dataStore)){
            push(@batchIDs, $shortID);
          } else {
            pod2usage(sprintf("Error: corrupt or incorrect sequence found ".
        		      "after %d reads\n", $processedCount));
          }
          $processedCount++;
          $batchReadCount++;
          if($batchReadCount % ($batchSize / 10) == 0){
            print(STDERR ".");
          }
          if($batchReadCount >= $batchSize){
            print(STDERR " ");
            batchProcess(\@batchIDs, \%dataStore, \%barcodeCounts);
            push(@processedIDs, @batchIDs);
            undef(@batchIDs);
            undef(%dataStore);
            $batchReadCount = 0;
            if(($processedCount > ($batchSize * 10)) && ($batchSize * 10 <= $maxBatchSize)){
              $batchSize = $batchSize * 10;
              print(STDERR "Increasing batch size to $batchSize\n");
            }
          }
          undef($seqID);
          undef($qualID);
          undef($seq);
          undef($qual);
          $inQual = 0; # false
        }
      }
    } ## end quality extension
  } ## end while(<$z>)
  if($z ne "-"){
    close($z);
  }
}

if($inQual || $seqID){
  $shortID = $seqID;
  $shortID =~ s/\s.*$//;
  print(STDERR "Warning: $seqID was not finished before the end of input\n");
}

if($batchReadCount >= 1){
  batchProcess(\@batchIDs, \%dataStore, \%barcodeCounts);
  push(@processedIDs, @batchIDs);
  @batchIDs = ();
  $batchReadCount = 0;
}

my $readCountBC = 0;
foreach my $key (sort(keys %barcodeCounts)){
  my $value = $barcodeCounts{$key};
  if(($key ne "BCnone") && ($key ne "BCnoadapt")){
    $readCountBC += $value;
  }
}

printf(STDERR "Done.\n");

my $countString = sprintf("Total reads seen with any barcodes: %d\n", $readCountBC);

my $countWidth = length($readCountBC) + 2;
foreach my $key (sort(keys %barcodeCounts)){
  my $value = $barcodeCounts{$key};
  $countString .= sprintf(" %${countWidth}d %s\n", $value, $key);
}

print($countString);

my $countFileName = ${muxDir}."/counts.orig.txt";
open(my $countFile, '>>', $countFileName) or die "Could not write to $countFileName: $!";
print($countFile $countString);
close($countFile);

