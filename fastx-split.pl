#!/usr/bin/env perl
use warnings;
use strict;

use Pod::Usage; ## uses pod documentation in usage code
use Getopt::Long qw(:config auto_help pass_through);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);
use IO::Compress::Gzip; ## for creating output files

$| = 1; # automatically flush output [https://perldoc.perl.org/perlvar]

our $VERSION = "0.1";

=head1 NAME

fastx-split.pl -- split reads out into separate files based on a header match pattern

=head1 SYNOPSIS

./fastx-split.pl <reads.fq> -pattern <regexp> [options]

=head2 Options

=over 2

=item B<-help>

Only display this help message

=item B<-pattern> I<regexp>

Set the match pattern to I<regexp>. If the match pattern includes a capture set,
then file names will be named based on that capture set. Otherwise, file names
will be named numerically in order of appearance of different match patterns.

=item B<-count> I<limit>

Write out at most I<limit> sequences for each match; additional
sequences are discarded.

=item B<-quiet>

Suppress warning messages when a sequence has no pattern match.

=back

=head1 DESCRIPTION

split reads out into separate files based on a header match pattern

=cut

my $quiet = 0;
my $searchPattern = ""; ## "(^.*\$)";
my $numeric = 0;
my $length = 0;
my $countLimit = 0;
my $r = 0;
my $patternsSeen = 0;
my $readsSeen = 0;
my %patternLookup = ();
my %patternsWritten = ();
my %patternFilenames = ();
my %patternFiles = ();

GetOptions("pattern=s" => \$searchPattern, "count=i" => \$countLimit,
           "quiet!" => \$quiet) or pod2usage(1);

if($searchPattern eq ""){
  print(STDERR "\nError: Pattern must be specified\n\n");
  pod2usage(1);
}

# Complain about non-file command line argument
my @files = ();
while(@ARGV){
  my $arg = shift(@ARGV);
  if(-e $arg){
    push(@files, $arg);
  } else {
    die("Unknown argument: $arg");
  }
}
@ARGV = @files;

# use stdin if no files supplied
if(!@ARGV){
  @ARGV = '-' unless (-t STDIN);
}

my %fastXStrs = ();

my $inQual = 0; # false
my $seqID = "";
my $qualID = "";
my $seq = "";
my $qual = "";
foreach my $file (@ARGV) {
  # This little gunzip dance makes sure the script can handle both
  # gzip-compressed and uncompressed input, regardless of whether
  # or not it is piped
  my $z = new IO::Uncompress::Gunzip($file, MultiStream => 1)
    or die "gunzip failed: $GunzipError\n";
  while(<$z>){
    chomp;
    chomp;
    if (!$inQual) {
      if (/^(>|@)(.*)$/) {
        my $newSeqID = $2;
        if ($seqID) {
          $readsSeen++;
          if ($seqID =~ /($searchPattern)/) {
            my $hasCapture = ($2) ? 1 : 0;
            my $matchPattern = ($hasCapture) ? $2 : $1;
            my $key = "";
            if(!exists($patternLookup{$matchPattern})){
              $patternsSeen += 1;
              $patternLookup{$matchPattern} = ($hasCapture) ? $matchPattern : ($patternsSeen);
              $patternFilenames{$matchPattern} = "split_".$patternLookup{$matchPattern}.
                  (($qual) ? ".fq.gz" : ".fa.gz");
              $patternFiles{$matchPattern} = new IO::Compress::Gzip($patternFilenames{$matchPattern},
                                                                    Append => 1)
                  or die("Unable to create a new file; try increasing the file count limit `ulimit -n <int>`, or filtering the input file");
            }
            $key = $patternLookup{$matchPattern};
            my $outFile = $patternFiles{$matchPattern};
            if (!$qual) {
              $seq =~ s/(.{100})/$1\n/g;
              $seq =~ s/\n$//;
            }
            if(!$countLimit || ($patternsWritten{$matchPattern} < $countLimit)){
              if(!$seq){
                print(STDERR "Error: $seqID has no sequence\n");
              } else {
                if($qual){
                  printf($outFile "@%s\n%s\n+\n%s\n", $seqID, $seq, $qual);
                } else {
                  printf($outFile ">%s\n%s\n", $seqID, $seq);
                }
                $patternsWritten{$matchPattern}++;
              }
            }
          } elsif(!$quiet) {
            printf(STDERR "Warning: No match for pattern '$searchPattern' for sequence '$seqID'\n");
          }
        }
        $seq = "";
        $qual = "";
        $seqID = $newSeqID;
      } elsif (/^\+(.*)$/) {
        $inQual = 1;            # true
        $qualID = $1;
      } else {
        $seq .= $_;
      }
    } else {
      $qual .= $_;
      if (length($qual) >= length($seq)) {
        $inQual = 0;            # false
      }
    }
  }
}

if ($seqID) {
  if ($seqID =~ /($searchPattern)/) {
    my $hasCapture = ($2) ? 1 : 0;
    my $matchPattern = ($hasCapture) ? $2 : $1;
    my $key = "";
    if(!exists($patternLookup{$matchPattern})){
      $patternsSeen += 1;
      $patternLookup{$matchPattern} = ($hasCapture) ? $matchPattern : ($patternsSeen);
      $patternFilenames{$matchPattern} = "split_".$patternLookup{$matchPattern}.
          (($qual) ? ".fq.gz" : ".fa.gz");
      $patternFiles{$matchPattern} = new IO::Compress::Gzip($patternFilenames{$matchPattern},
                                                            Append => 1);
    }
    $key = $patternLookup{$matchPattern};
    my $outFile = $patternFiles{$matchPattern};
    if (!$qual) {
      $seq =~ s/(.{100})/$1\n/g;
      $seq =~ s/\n$//;
    }
    if(!$countLimit || ($patternsWritten{$matchPattern} < $countLimit)){
      if($qual){
        printf($outFile "@%s\n%s\n+\n%s\n", $seqID, $seq, $qual);
      } else {
        printf($outFile ">%s\n%s\n", $seqID, $seq);
      }
      $patternsWritten{$matchPattern}++;
    }
  } elsif(!$quiet) {
    printf(STDERR "Warning: No match for pattern '$searchPattern' for sequence '$seqID'\n");
  }
}

if(!$quiet){
  printf(STDERR "Done! Seen %d reads with %d distinct pattern matches\n", $readsSeen, $patternsSeen);
}

## close up all used files
for my $key (keys(%patternLookup)){
  close($patternFiles{$key});
}
