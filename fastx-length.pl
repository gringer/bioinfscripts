#!/usr/bin/env perl
use warnings;
use strict;

use Pod::Usage; ## uses pod documentation in usage code
use Getopt::Long qw(:config auto_help pass_through);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);

$| = 1; # automatically flush output [https://perldoc.perl.org/perlvar]

our $VERSION = "0.1";

=head1 NAME

fastx-length.pl -- determine sequence lengths of the input

=head1 SYNOPSIS

./fastx-length.pl <reads.fq> [options]

=head2 Options

=over 2

=item B<-help>

Only display this help message

=item B<-md5>

Show an MD5 digest of the sequence

=item B<-fname>

Show the name of the files processed

=item B<-at>

Calculate the longest A/T homopolymer length for each read

=back

=head1 DESCRIPTION

Determines the sequence lengths of input. Also includes statistics
about the longest A/T polymer (for use in determining polyA lengths in
cDNA reads).

=cut


sub getMeanQual {
  my ($qual) = @_;
  if(length($qual) == 0){
    return(0);
  }
  my $qBase = 33;
  my @qspl = split("", $qual);
  my $qualTotal = 0;
  my $qualCount = 0;
  foreach my $qual (@qspl) {
    $qualTotal += 10 ** (-(ord($qual) - $qBase) / 10);
    $qualCount++;
  }
  return(sprintf("%0.1f", (-10 * log($qualTotal / $qualCount) / log(10))));
}

sub getATLength {
  ## This finds long regions of A or T homopolymers, allowing for some
  ## amount of interruption. The score increases by 1 with each A/T up
  ## to a maximum of 8, and reduces with other bases by 2 down to a
  ## minimum of -4. While the score remains positive, the number of As
  ## in the current region is counted.
  my ($seq) = @_;
  $seq =~ tr/uU/tT/;
  if(length($seq) == 0){
    return(0);
  }
  my $nonALimit = 3;
  my @sspl = split("", $seq);
  my $seqAMax = 0;
  my $seqTMax = 0;
  my $seqACurrent = -4;
  my $seqTCurrent = -4;
  my $currentACount = 0;
  my $currentTCount = 0;
  my $posACount = 0;
  my $posTCount = 0;
  my $tailACount = 0;
  my $tailTCount = 0;
  foreach my $base (@sspl) {
    # These are all ternary functions to [slightly] improve speed
    $seqACurrent += ($base eq 'A') ? 1 : -4;
    $seqTCurrent += ($base eq 'T') ? 1 : -4;
    $currentACount = ($base eq 'A') ? ($currentACount + 1) : 0;
    $currentTCount = ($base eq 'T') ? ($currentTCount + 1) : 0;
    if($seqACurrent > 0){
      $posACount += $currentACount; $currentACount = 0;
      $seqAMax = ($posACount > $seqAMax) ? $posACount : $seqAMax;
    } else {
      $posACount = 0;
    }
    if($seqTCurrent > 0){
      $posTCount += $currentTCount; $currentTCount = 0;
      $seqTMax = ($posTCount > $seqTMax) ? $posTCount : $seqTMax;
    } else {
      $posTCount = 0;
    }
    $seqACurrent = ($seqACurrent > 8) ? 8 : (($seqACurrent > -4) ? $seqACurrent : -3);
    $seqTCurrent = ($seqTCurrent > 8) ? 8 : (($seqTCurrent > -4) ? $seqTCurrent : -3);
  }
  return(($seqAMax > $seqTMax) ? $seqAMax : $seqTMax);
}

sub SIConvert{
  my ($val) = @_;
  my @largePrefixes = ("k", "M", "G");
  my $pID = 0;
  my $prefix = "";
  my $changed=0;
  while(($val > 1000) && ($pID < $#largePrefixes)){
    $prefix = $largePrefixes[$pID];
    $val /= 1000;
    $pID++;
  }
  return(sprintf("%.12g %s", $val, $prefix));
}

my $showMD5 = 0; # false
my $showFName = 0; # false
my $calcAT = 0; # false

GetOptions("md5!" => \$showMD5, "fname!" => \$showFName,
           "at!" => \$calcAT, ) or
    die("Error in command line arguments");

if($showMD5){
  use Digest::MD5 qw(md5_hex); ## only attempt to load if md5 is requested
}

# Complain about non-file command line argument
my @files = ();
while(@ARGV){
  my $arg = shift(@ARGV);
  if(-e $arg){
    push(@files, $arg);
  } else {
    die("Unknown argument: $arg");
  }
}
@ARGV = @files;

# use stdin if no files supplied
if(!@ARGV){
  @ARGV = '-' unless (-t STDIN);
}

my @lengths = ();
my @quals = ();
my @atLengths = ();
my $inQual = 0; # false
my $seqID = "";
my $qualID = "";
my $seq = "";
my $qual = "";
my $meanQual = 0;
my $atLength = 0;
my $seqFName = "";
foreach my $file (@ARGV) {
  # This little gunzip dance makes sure the script can handle both
  # gzip-compressed and uncompressed input, regardless of whether
  # or not it is piped
  my $z = new IO::Uncompress::Gunzip($file, MultiStream => 1)
    or die "gunzip failed: $GunzipError\n";
  while(<$z>){
    chomp;chomp;chomp;
    if (!$inQual) {
      if (/^(>|@)((.+?)( .*?\s*)?)$/) {
        my $newSeqID = $2;
        my $newShortID = $3;
        if ($seqID) {
          print(length($seq));
          push(@lengths, length($seq));
          if($qual){
            $meanQual = getMeanQual($qual);
            print(" q".$meanQual);
            push(@quals, $meanQual);
          }
          if($calcAT){
            $atLength = getATLength($seq);
            print(" at".$atLength);
            push(@atLengths, $atLength);
          }
          if($showMD5){
            print(" ".md5_hex($seq));
          }
          print(" ${seqID}");
          if($showFName){
            print(" ".$seqFName);
          }
          print("\n");
        }
        $seq = "";
        $qual = "";
        $seqID = $newShortID;
        $seqFName = $file;
      } elsif (/^\+(.*)$/) {
        $inQual = 1;            # true
        $qualID = $1;
      } else {
        $seq .= $_;
      }
    } else {
      $qual .= $_;
      if (length($qual) >= length($seq)) {
        $inQual = 0;            # false
      }
    }
  }
}

if ($seqID) {
  print(length($seq));
  if($qual){
    $meanQual = getMeanQual($qual);
    print(" q".$meanQual);
    push(@quals, $meanQual);
  }
  if($calcAT){
    $atLength = getATLength($seq);
    print(" at".$atLength);
    push(@atLengths, $atLength);
  }
  if($showMD5){
    print(" ".md5_hex($seq));
  }
  print(" ${seqID}");
  if($showFName){
    print(" ".$seqFName);
  }
  print("\n");
  push(@lengths, length($seq));
}

if(!(@lengths)){
  print(STDERR "** no sequences found **\n");
  exit;
}

## calculate statistics
@lengths = sort {$b <=> $a} (@lengths);
my $sum = 0;
my @cumLengths = map {$sum += $_} (@lengths);

my $L50LengthNum = 0;
while($cumLengths[$L50LengthNum] < ($sum * 0.5)){
  $L50LengthNum++;
}

my $L90LengthNum = 0;
while($cumLengths[$L90LengthNum] < ($sum * 0.9)){
  $L90LengthNum++;
}

my $L10LengthNum = 0;
while($cumLengths[$L10LengthNum] < ($sum * 0.1)){
  $L10LengthNum++;
}

printf(STDERR "Total sequences: %d\n", scalar(@lengths));
printf(STDERR "Total length: %sb\n", SIConvert($sum));
printf(STDERR "Longest sequence: %sb\n", SIConvert($lengths[0]));
printf(STDERR "Shortest sequence: %sb\n", SIConvert($lengths[$#lengths]));
printf(STDERR "Mean Length: %sb\n", SIConvert(sprintf("%d", ($sum) / scalar(@lengths))));
printf(STDERR "Median Length: %sb\n", SIConvert($lengths[$#lengths / 2]));
printf(STDERR "N10: %d sequences; L10: %sb\n",
       ($L10LengthNum+1), SIConvert($lengths[$L10LengthNum]));
printf(STDERR "N50: %d sequences; L50: %sb\n",
       ($L50LengthNum+1), SIConvert($lengths[$L50LengthNum]));
printf(STDERR "N90: %d sequences; L90: %sb\n",
       ($L90LengthNum+1), SIConvert($lengths[$L90LengthNum]));

if($calcAT){
  @atLengths = sort {$a <=> $b} (@atLengths);
  my $atSum = 0;
  my %atCounts = ();
  foreach my $atLen (@atLengths){
    $atCounts{$atLen}++;
    $atSum += $atLen;
  }
  my @atModes = sort { $atCounts{$a} <=> $atCounts{$b} } keys %atCounts;

  printf(STDERR "A/T homopolymer lengths:\n");
  printf(STDERR "  Minimum: %d bp\n", $atLengths[0]);
  printf(STDERR "  Maximum: %d bp\n", $atLengths[$#atLengths]);
  printf(STDERR "  Mean: %d bp\n", $atSum / scalar(@atLengths));
  printf(STDERR "  Median: %d bp\n", $atLengths[int($#atLengths / 2)]);
  printf(STDERR "  Mode: %d bp\n", $atModes[0]);
  printf(STDERR "  10th percentile: %d bp\n", $atLengths[int($#atLengths * 1 / 10)]);
  printf(STDERR "  90th percentile: %d bp\n", $atLengths[int($#atLengths * 9 / 10)]);
}

if(@quals){
  @quals = sort {$a <=> $b} (@quals);
  my $qMin = $quals[0];
  my $qMax = $quals[$#quals];
  my $Q10 = $quals[int($#quals * 1/10)];
  my $Q50 = $quals[int($#quals * 5/10)];
  my $Q90 = $quals[int($#quals * 9/10)];
  my $qSum = 0;
  my %qualCount = ();
  foreach my $qual (@quals){
    $qualCount{$qual}++;
    $qSum += $qual;
  }
  my @modes = sort { $qualCount{$a} <=> $qualCount{$b} } keys %qualCount;
  my $QMode = $modes[0];
  printf(STDERR "Predicted / modeled quality across all reads:\n");
  printf(STDERR "  Minimum: q%d\n", $qMin);
  printf(STDERR "  Maximum: q%d\n", $qMax);
  printf(STDERR "  Mean: q%d\n", $qSum / scalar(@quals));
  printf(STDERR "  Median: q%d\n", $Q50);
  printf(STDERR "  Mode: q%d\n", $QMode);
  printf(STDERR "  10th percentile: q%d\n", $Q10);
  printf(STDERR "  90th percentile: q%d\n", $Q90);
}
