#!/usr/bin/env perl
use warnings;
use strict;

use Pod::Usage; ## uses pod documentation in usage code
use Getopt::Long qw(:config auto_help pass_through);

my $pos = -1;
my $seqName = "";
my $bestFlags = "";
my $bestID = "";
my $bestSeq = "";
my $bestQual = "";
my $bestLine = "";
my $seenCount = 0;
my $targetCoverage = 100;
my %posReads = ();
my %windowReads = ();
my $covNeeded = -1;
my $output = "sam"; # can be "sam" or "fastq"

our $VERSION = "0.1";

=head1 NAME

samNormalise.pl -- randomly filter mapped reads down to a specific coverage

=head1 SYNOPSIS

samtools view -h reads.bam | ./samNormalise.pl [options]

=head2 Options

=over 2

=item B<-help>

Only display this help message

=item B<-coverage>

Change target coverage (default: 100)

=item B<-format> (sam|fastq)

Change output format (default: sam)

=back

=head1 DESCRIPTION

Extracts random sequences from those mapped at each base location,
attempting to maintain a minimum coverage threshold where possible.

=cut

GetOptions("format=s" => \$output, "coverage=i" => \$targetCoverage)
  or pod2usage(1);


sub printSeq {
  my ($id, $seq, $qual) = @_;
  if($id){
    printf("@%s\n%s\n+\n%s\n", $id, $seq, $qual);
  }
}

sub dumpSamples {
  my %posReads = %{shift @_};
  if($output eq "fastq"){
    foreach my $pos (keys(%posReads)){
      printSeq($posReads{$pos}{"id"},
               $posReads{$pos}{"seq"},
               $posReads{$pos}{"qual"});
    }
  } elsif($output eq "sam"){
    foreach my $pos (keys(%posReads)){
      print($posReads{$pos}{"line"});
    }
  }
}

sub getMappedLength {
  my $cigar = shift @_;
  my $mappedLen = 0;
  while($cigar =~ s/^([0-9]+)([MIDNSHP=X])//){
    my $subLen = $1;
    my $op = $2;
    if($op =~ /[M=XD]/){
      $mappedLen += $subLen;
    }
  }
  return($mappedLen);
}

while(<>){
  if(/^@/){
    print;
    next;
  }
  my $line = $_;
  chomp;
  my @F = split(/\t/);
  my $currentPos = $F[3];
  if($F[2] ne $seqName){
    ## New reference name, so dump buffered reads
    dumpSamples(\%posReads);
    ## then clear coverage and position buffer
    %posReads = ();
    %windowReads = ();
    $covNeeded = $targetCoverage;
    $seenCount = 0;
  }
  if($currentPos != $pos){
    ## New position, so dump sampled reads from the position buffer (if any)
    dumpSamples(\%posReads);
    ## Add sampled reads from position buffer to window buffer
    foreach my $pos (keys(%posReads)){
      $windowReads{$posReads{$pos}{"id"}}{"pos"} =
        $posReads{$pos}{"pos"};
      $windowReads{$posReads{$pos}{"id"}}{"len"} =
        $posReads{$pos}{"len"};
    }
    ## Clear position buffer
    %posReads = ();
    ## Count up all reads that cover the current location
    $covNeeded = $targetCoverage;
    foreach my $readID (keys(%windowReads)){
      my $currentCov = 0;
      if($windowReads{$readID}{"pos"} +
         $windowReads{$readID}{"len"} < $currentPos){
        ## remove any reads that don't cover the current location
        delete($windowReads{$readID});
      } else {
        ## Read still within range, so reduce needed coverage by 1
        $covNeeded--;
      }
    }
    $seenCount = 0;
  }
  if($covNeeded > 0){
    ## need to add more reads because coverage threshold is below target
    ## or keep the new sequence with probability $covNeeded/$seenCount
    $seenCount++;
    my $posToSwitch = int(rand($seenCount));
    ## keep the first reads up to the coverage limit
    if(scalar(keys(%posReads)) < $covNeeded){
      $posToSwitch = scalar(keys(%posReads));
    }
    ## Note: reads are chosen at random, rather than length-based
    if($posToSwitch < $covNeeded){
      ## Add directly to position buffer, replacing anything there already
      $posReads{$posToSwitch}{"line"} = $line;
      $posReads{$posToSwitch}{"id"} = $F[0];
      $posReads{$posToSwitch}{"seq"} = $F[9];
      $posReads{$posToSwitch}{"pos"} = $F[3];
      $posReads{$posToSwitch}{"len"} = getMappedLength($F[5]);
      $posReads{$posToSwitch}{"qual"} = $F[10];
    }
  }
  $seqName = $F[2];
  $pos = $currentPos;
}
dumpSamples(\%posReads);
