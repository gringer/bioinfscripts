library(rgl);

triangleWave <- function(toothHeight, toothWidth, traversedDist){
  (abs((traversedDist %% toothWidth) - (toothWidth/2)) - toothWidth/4) *
    (toothHeight / toothWidth);
}

phi <- runif(4000) * 90;
theta <- runif(4000) * 2 * pi;

surfacePoints <- cbind(x = cos(theta) * sin(phi), y = sin(theta) * sin(phi), z = cos(phi));



bevelAngle <- 45;
bevelTheta <- seq(0,2*pi-0.01, length.out=1440);
bevelBasePoints <- cbind(x = cos(bevelTheta) * sin(bevelAngle), 
                         y = sin(bevelTheta) * sin(bevelAngle), z = cos(bevelAngle));
inscribedRadius <- 1 * sin(bevelAngle);
bevelDist <- bevelTheta * inscribedRadius;
toothWidth <- 1*sin(bevelAngle) * 2 * pi / 11;
toothHeight <- toothWidth / (pi/6);
r = triangleWave(toothHeight = toothHeight, toothWidth = toothWidth, 
                 traversedDist = bevelDist);
toothPoints <- cbind(x = (1+r)*cos(bevelTheta) * sin(bevelAngle), 
                     y = (1+r)*sin(bevelTheta) * sin(bevelAngle), z = (1+r)*cos(bevelAngle))
plot3d(surfacePoints);
lines3d(bevelBasePoints, col="red", lwd=4);
points3d(toothPoints, col="blue", lwd=4);

dir.create("animation");
for (i in 1:90) {
  view3d(userMatrix=rotationMatrix(2*pi * i/90, 1, -1, -1));
  rgl.snapshot(filename=paste("animation/frame-",
                              sprintf("%03d", i), ".png", sep=""), top = TRUE);
}