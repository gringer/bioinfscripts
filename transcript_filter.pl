#!/usr/bin/perl
use warnings;
use strict;

## transcript_filter.pl -- processes CSV file containing mapping information to identify
## per-read trancript coverage, merging any discovered overlaps.

my @foundHeader = ();
my %fieldLookup = ();
my @fields = ();
my @lookupList = ();
my $oldQuery = "";
my %poss = ();
my %lengths = ();

while(<>){
    chomp; chomp;
    @fields = split(/,/);
    ## Look at header data first, to allow for variation in input format
    if(!scalar(@foundHeader)){
	printf("%s,%s,%s,%s,%s,%s,%s\n", ("query", "queryLength", "target", "dir",
				       "mappedTargetBases", "mappedTargetProportion", "mappedQueryProportion"));
	@foundHeader = @fields;
	for (my $i = 0; $i < scalar(@foundHeader); $i++){
	    $fieldLookup{$foundHeader[$i]} = $i;
	}
	# pre-generate array of fields that need to be looked up
	@lookupList = map {$fieldLookup{$_}} ("target", "query", "dir", "tPct", "qS", "qE", "tS", "tE", "qL", "tL");
    } else {
	my ($target, $query, $dir, $tPct, $qS, $qE, $tS, $tE, $qL, $tL) = @fields[@lookupList];
	if($oldQuery ne $query){
	    my $tqL = $lengths{$oldQuery};
	    foreach my $tTarget (keys(%poss)){
		my $ttL = $lengths{$tTarget};
		foreach my $tDir (keys(%{$poss{$tTarget}})) {
		    my @tCovered = (0) x $ttL;
		    my @qCovered = (0) x $tqL;
		    # update coverage array, merging overlaps
		    while (my ($ttS, $ttE) = each %{$poss{$tTarget}{$tDir}{"target"}}) {
			@tCovered[($ttS .. $ttE)] = (1) x ($ttE - $ttS + 1);
		    }
		    while (my ($tqS, $tqE) = each %{$poss{$tTarget}{$tDir}{"query"}}) {
			@qCovered[($tqS .. $tqE)] = (1) x ($tqE - $tqS + 1);
		    }
		    # calculate / output covered bases and proportion
		    my $ttTotal = 0;
		    map { $ttTotal += $_ } @tCovered;
		    my $tqTotal = 0;
		    map { $tqTotal += $_ } @qCovered;
		    printf("%s,%d,%s,%s,%d,%0.2f,%0.2f\n", $oldQuery, $tqL, $tTarget, $tDir, $ttTotal, $ttTotal / $ttL, $tqTotal / $tqL);
		}
	    }
	    # reset length and position hashes for a new query sequence
	    %lengths = ();
	    %poss = ();
	}
	# update length and position hashes with mapping information
	$lengths{$target} = $tL;
	$lengths{$query} = $qL;
	$poss{$target}{$dir}{"target"}{$tS} = $tE;
	$poss{$target}{$dir}{"query"}{$qS} = $qE;
	$oldQuery = $query;
    }
}
if($oldQuery ne ""){
    my $tqL = $lengths{$oldQuery};
    foreach my $tTarget (keys(%poss)){
	my $ttL = $lengths{$tTarget};
	foreach my $tDir (keys(%{$poss{$tTarget}})) {
	    my @tCovered = (0) x $ttL;
	    my @qCovered = (0) x $tqL;
	    # update coverage array, merging overlaps
	    while (my ($ttS, $ttE) = each %{$poss{$tTarget}{$tDir}{"target"}}) {
		@tCovered[($ttS .. $ttE)] = (1) x ($ttE - $ttS + 1);
	    }
	    while (my ($tqS, $tqE) = each %{$poss{$tTarget}{$tDir}{"query"}}) {
		@qCovered[($tqS .. $tqE)] = (1) x ($tqE - $tqS + 1);
	    }
	    # calculate / output covered bases and proportion
	    my $ttTotal = 0;
	    map { $ttTotal += $_ } @tCovered;
	    my $tqTotal = 0;
	    map { $tqTotal += $_ } @qCovered;
	    printf("%s,%d,%s,%s,%d,%0.2f,%0.2f\n", $oldQuery, $tqL, $tTarget, $tDir, $ttTotal, $ttTotal / $ttL, $tqTotal / $tqL);
	}
    }
}
