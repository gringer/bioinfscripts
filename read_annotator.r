#!/usr/bin/env Rscript

## read_annotator -- map adapter sequences to a read

usage <- function(){
  cat("usage: ./read_annotator.r",
      "<read fasta/fastq file> -a <adapter fasta/fastq file> [options]\n");
  cat("\nOther Options:\n");
  cat("-t <signal table>  : Lookup table for raw signal (e.g. tombo event table)\n");
  cat("-s <raw data file> : File containing raw signal data (as a 2-byte-per-sample binary file)\n");
  cat("-f5 <fast5 file>   : Fast5 file containing called sequence\n");
  cat("-w <int>           : Bases per line (default: 80)\n");
  cat("-a <fasta file>    : Adapter file\n");
  cat("-v                 : Verbose output (show match table)\n");
  cat("-help              : Only display this help message\n");
  cat("\n");
}

drawSignal <- function(rawSignal, signalPositions,
                       startPos=1, lineLength=80){
    sigStarts <- signalPositions + 1;
    sigEnds <- c(tail(signalPositions, -1), length(rawSignal)) + 1;
    sigWidths <- sigEnds - sigStarts;
    basesRemaining <- length(signalPositions) - startPos;
    sigLookups <-
        unlist(sapply(1:length(signalPositions),
                      function(x){
                          head(seq(x, x+1, length.out=(sigWidths[x]+1)), -1)}));
    while(basesRemaining > 0){
        chunkSize <- min(basesRemaining,
                         lineLength - ((startPos - 1) %% lineLength));
        startY <- floor(startPos / lineLength);
        startX <- startPos - startY * lineLength;
        sigStart <- sigStarts[startPos];
        sigEnd <- sigEnds[startPos+chunkSize-1];
        points(x=sigLookups[sigStart:sigEnd] - startY * lineLength - 0.5,
               y=startY*10 - rawSignal[sigStart:sigEnd]/80, type="l");
        startPos <- startPos + chunkSize;
        basesRemaining <- basesRemaining - chunkSize;
    }
}

drawSeq <- function(seq, startPos=1, colMap=NA, lineLength=80,
                    annotate="", dropLab=FALSE){
    ## Default to colour-able colours
    cMap <- c(A="#20A000", C="#00A0FF", G="#FFD700", T="#FF6347", N="#B3B3B3");
    if(!is.na(colMap)){
        cMap <- colMap;
    }
    mono <- (length(cMap) == 1);
    firstLine <- TRUE;
    while(length(seq) > 0){
        chunkSize <- lineLength - ((startPos - 1) %% lineLength);
        subSeq <- head(seq, chunkSize);
        seq <- tail(seq, -chunkSize);
        startY <- floor(startPos / lineLength);
        startX <- startPos - startY * lineLength;
        text(x=1:length(subSeq) + startX - 1,
             y=startY * 10, labels=subSeq,
             col=if(mono){cMap} else {cMap[subSeq]},
             vfont=c("serif","bold"), cex=1);
        toContinue <- (length(seq) > 0);
        if(annotate != ""){
            segments(x0=startX-0.5, x1=startX+length(subSeq)-0.5,
                     y0=startY * 10 - 5,
                     col=if(mono){cMap} else {par("fg")});
            if(firstLine){
                segments(x0=startX-0.5,
                         y0=startY * 10 - 5, y1=startY * 10 - 2.5,
                         col=if(mono){cMap} else {par("fg")});
                if(dropLab){
                    text(x=startX, y=startY * 10 - 6,
                         labels=annotate, cex=0.71, adj=c(0,0),
                         col=if(mono){cMap} else {par("fg")}, srt=10);
                } else {
                    text(x=startX, y=startY * 10 - 2,
                         labels=annotate, cex=0.71, adj=c(0,0),
                         col=if(mono){cMap} else {par("fg")}, srt=10);
                }
            }
            if(length(seq) > 0){
                segments(x0=startX+length(subSeq)-0.5,
                         x1=startX+length(subSeq)+1.5,
                         y0=startY * 10 - 5,
                         lty="dotted",
                         col=if(mono){cMap} else {par("fg")});
            } else {
                segments(x0=startX+length(subSeq)-0.5,
                         y0=startY * 10 - 5, y1=startY * 10 - 2.5,
                         col=if(mono){cMap} else {par("fg")});
            }
            if(!firstLine){
                segments(x0=startX-0.5,
                         x1=startX-2.5,
                         y0=startY * 10 - 5,
                         lty="dotted",
                         col=if(mono){cMap} else {par("fg")});
            }
        }
        startPos <- startPos + length(subSeq);
        firstLine <- FALSE;
    }
}

## Pre-check to make sure that LAST is installed
cPipe <- pipe("lastal -V");
if(!grepl("^lastal", readLines(cPipe))){
    cat("Error: LAST is not installed; cannot continue\n");
    close(cPipe);
    usage();
    quit(save = "no", status=0);
}
close(cPipe);

verbosity <- 0;
dnaSeqFile <- NULL;
adapterFile <- NULL;
rawSignalFile <- NULL;
rawSignal <- NULL;
rawSignals <- list();
fast5File <- NULL;
signalTableFile <- NULL;
signalPositions <- NULL;
signalPoss <- list();
seqWidth <- 130;

argLoc <- 1;
while(!is.na(commandArgs(TRUE)[argLoc])){
    if(file.exists(commandArgs(TRUE)[argLoc])){ # file existence check
      if(!is.null(dnaSeqFile)){
        cat(sprintf("warning: replacing sequence file '%s' with '%s'\n",
            dnaSeqFile, commandArgs(TRUE)[argLoc]));
      }
      dnaSeqFile <- commandArgs(TRUE)[argLoc];
  } else {
    if((commandArgs(TRUE)[argLoc] == "-help") ||
         (commandArgs(TRUE)[argLoc] == "-h")){
      usage();
      quit(save = "no", status=0);
    }
    else if(commandArgs(TRUE)[argLoc] == "-a"){
      adapterFile <- c(adapterFile, commandArgs(TRUE)[argLoc+1]);
      argLoc <- argLoc + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-t"){
      signalTableFile <- commandArgs(TRUE)[argLoc+1];
      argLoc <- argLoc + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-s"){
      rawSignalFile <- commandArgs(TRUE)[argLoc+1];
      argLoc <- argLoc + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-f5"){
      fast5File <- commandArgs(TRUE)[argLoc+1];
      argLoc <- argLoc + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-v"){
      verbosity <- verbosity + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-w"){
      seqWidth <- as.numeric(commandArgs(TRUE)[argLoc+1]);
      cat("Setting width to", seqWidth, "\n");
      argLoc <- argLoc + 1;
    }
    else {
      cat("Error: Argument '",commandArgs(TRUE)[argLoc],
          "' is not understood by this program\n\n", sep="");
      usage();
      quit(save = "no", status=0);
    }
  }
  argLoc <- argLoc + 1;
}

if((length(signalTableFile) == 0) &&
   (length(dnaSeqFile) == 0) &&
   (length(fast5File) == 0)){
    cat(sprintf("Error: no DNA sequence file specified\n"));
    usage();
    quit(save = "no", status=0);
}

if(length(rawSignalFile) != 0){
    if(!file.exists(rawSignalFile)){
        cat(sprintf("Error: signal file '%s' does not exist\n",
                    rawSignalFile));
        usage();
        quit(save = "no", status=0);
    }
    fileLen <- file.size(rawSignalFile);
    rawSignal <-
        readBin(rawSignalFile, what=integer(), size=2, signed=FALSE,
                n=fileLen/2);
}

if(length(fast5File) != 0){
    ## Load data from called fast5 file
    ## [see https://community.nanoporetech.com/posts/mapping-of-signal-to-basec#comment_25169]
    if(!file.exists(fast5File)){
        cat(sprintf("Error: fast5 file '%s' does not exist\n",
                    fast5File));
        usage();
        quit(save = "no", status=0);
    }
    cat("Loading library 'rhdf5' to read fast5 file... ");
    library(rhdf5);
    cat("done!\n");
    cat("Fetching file listing... ");
    f5Listing <- h5ls(fast5File, datasetinfo=FALSE);
    cat("done!\n");
    if(!any("Fastq" %in% f5Listing$name)){
        cat(sprintf(paste0("Error: fast5 file '%s' has no basecalls. ",
                           "Please use a basecalled fast5 file\n"), fast5File));
        usage();
        quit(save = "no", status=0);
    }
    dnaSeqFile <- tempfile(fileext=".fasta");
    firstSequence <- TRUE;
    for(movePos in which(f5Listing$name == "Move")[1]){
        basePath <- f5Listing$group[movePos];
        movePath <- paste0(basePath, "/Move");
        seqPath <- paste0(basePath, "/Fastq");
        fastqSeq <- unlist(strsplit(h5read(fast5File, seqPath), "\\n"));
        seqID <- sub("^.(.*?) .*$", "\\1", fastqSeq[1]);
        DNASeq <- fastqSeq[2];
        cat(paste0(">", seqID, "\n", DNASeq, "\n"), file=dnaSeqFile,
            append=!firstSequence);
        firstSequence <- FALSE;
        ## Extract associated move table
        stridePath <- sub("/[^/]+$", "/Summary/basecall_1d_template", basePath);
        signalStride <- h5readAttributes(fast5File, stridePath)$block_stride;
        moveTable <- cumsum(h5read(fast5File, movePath));
        moveTable <- rep(moveTable, each=signalStride);
        ## Invert to create lookup from positions to moves
        signalPositions <- match(1:max(moveTable), moveTable);
        signalPoss[[seqID]] <- signalPositions;
        ## Extract first sample width
        trimPath <- sub("Basecall_1D_(...).*$",
                        "Segmentation_\\1/Summary/segmentation", basePath);
        trimWidth <- h5readAttributes(fast5File, trimPath)$first_sample_template;
        ## Extract associated signal
        sigPath <- sub("Analyses/.*$", "Raw/Signal", basePath);
        rawSignal <- h5read(fast5File, sigPath);
        ## Trim off first sample fragment
        if(trimWidth > 0){
            rawSignal <- tail(rawSignal, -trimWidth);
        }
        rawSignals[[seqID]] <- rawSignal;
    }
}

if(length(signalTableFile) != 0){
    if(!file.exists(signalTableFile)){
        cat(sprintf("Error: signal table file '%s' does not exist\n",
                    signalTableFile));
        usage();
        quit(save = "no", status=0);
    } else {
        dnaSeqFile <- tempfile(fileext=".fasta");
        data.df <- read.table(signalTableFile);
        ## Look for a character column
        seq.cols <- names(which(sapply(data.df, class) == "factor"));
        chosen.cols <- seq.cols;
        if(length(seq.cols) > 0){
            ## Look for a column that contains only single letters
            res.cols <- sapply(chosen.cols, function(x){
                all(grepl("^[ACGTUacgtu]$",levels(data.df[[x]])));
            });
            chosen.cols <- chosen.cols[res.cols];
            ## Choose the first likely column
            chosen.cols <- head(chosen.cols, 1);
        }
        if(length(chosen.cols == 1)){
            DNASeq <- paste(data.df[[chosen.cols]], collapse="");
            seqID <- sub("\\.[^\\.]*$", "", basename(signalTableFile));
            cat(paste0(">", seqID, "\n", DNASeq, "\n"), file=dnaSeqFile);
            if(length(rawSignal) > 0){
                ## look for a signally-looking column
                sig.cols <- names(which(sapply(data.df, class) == "integer"));
                res.cols <-
                    sapply(sig.cols, function(x){
                        abs(log2(max(data.df[[x]]) / length(rawSignal)))});
                ## Choose the column with the closest matching maximum value
                ## vs the total signal data length
                sig.cols <- sig.cols[order(res.cols)[1]];
                signalPositions <- data.df[[sig.cols]];
            }
        } else {
            cat(sprintf(paste0("Error: cannot find a column that looks ",
                               "like DNA/RNA bases in '%s'\n"),
                        signalTableFile));
            usage();
            quit(save = "no", status=0);
        }
    }
}

if(!file.exists(dnaSeqFile)){
    cat(sprintf("Error: read file '%s' does not exist\n", dnaSeqFile));
    usage();
    quit(save = "no", status=0);
}

if(length(adapterFile) == 0){
    cat(sprintf("Error: no adapter file specified\n"));
    usage();
    quit(save = "no", status=0);
}

if(!all(file.exists(adapterFile))){
    cat(sprintf("Error: adapter file '%s' does not exist\n", adapterFile));
    usage();
    quit(save = "no", status=0);
}

cat("Loading R libraries...");
suppressMessages({
    library(Biostrings);
    library(RColorBrewer);
    library(digest);
    library(readr);
    library(purrr);
    library(magrittr);
});
cat(" done\n");

cat("Preparing reads...");

seqIsFasta <- (substring(scan(dnaSeqFile, what=character(),
                              nlines=1, n=1, quiet=TRUE),1,1) == ">");

dnaSeqFile <- readLines(dnaSeqFile);

adapterSeqs <- NULL;
for(f in adapterFile){
    newSeqs <- list(readLines(f));
    fName <- sprintf("adapter_%03d.fa", length(adapterSeqs));
    names(newSeqs) <- fName;
    adapterSeqs <- c(adapterSeqs, newSeqs);
}

currentDir <- getwd();

## use the temporary directory for working space
setwd(tempdir());

for(fName in names(adapterSeqs)){
    newSeqs <- adapterSeqs[[fName]];
    cat(file = fName, newSeqs, sep = "\n");
}

adapterFile <- names(adapterSeqs);

cat("#last -Q 0",
    "#last -t4.37558",
    "#last -a 10",
    "#last -A 13",
    "#last -b 5",
    "#last -B 5",
    "#last -S 1",
    "# score matrix (query letters = columns, reference letters = rows):",
    "       A      C      G      T",
    "A      5    -19     -8    -18",
    "C    -18      6    -20    -13",
    "G     -8    -23      6    -21",
    "T    -15    -12    -20      6", file="nanopore_guppy5.1.15.mat", sep="\n");

cat("#last -Q 0",
    "#last -t4.51719",
    "#last -a 10",
    "#last -A 10",
    "#last -b 5",
    "#last -B 5",
    "#last -S 1",
    "# score matrix (query letters = columns, reference letters = rows):",
    "       A      C      G      T",
    "A      6    -11    -14    -17",
    "C    -13      6    -17    -20",
    "G    -11    -17      6    -20",
    "T    -16    -17    -20      6", file="nanopore_guppy6.3.8_R10.4.1.mat", sep="\n");

inputFileName <- ifelse(seqIsFasta, "reads.fa", "reads.fq");
cat(file = inputFileName, dnaSeqFile, sep = "\n");

## Convert DNA sequence to list
seqList <- list();
inQual <- 0; # false
seqID <- "";
seq <- "";
qual <- "";
for(seqLine in dnaSeqFile){
    if(!inQual){
        newseqID <- "";
        if(startsWith(seqLine, "@") || startsWith(seqLine, ">")){
            newSeqID <- sub(" .*$", "", sub("^.", "", seqLine));
            if(nchar(seq) > 0){
                seqList[[seqID]] <- unlist(strsplit(seq, ""));
            }
            seq <- "";
            qual <- "";
            seqID <- newSeqID;
        } else if(startsWith(seqLine, "+")) {
            inQual <- TRUE;
        } else {
            seq <- paste0(seq, seqLine);
        }
    } else {
        qual <- paste0(qual, seqLine);
        if(nchar(qual) >= nchar(seq)){
            inQual <- FALSE;
        }
    }
}
if(nchar(seq) > 0){
    seqList[[seqID]] <- unlist(strsplit(seq, ""));
}
dnaSeqFile <- seqList;

##system("lastdb -uRY4 -R01 adapters.fa adapters.fa");

cat(" done\n");

cat("Mapping reads / adapters...\n");
resList <- NULL;
for(afName in adapterFile){
    system2(command="lastdb", args=c("-uRY4", "-R01", afName, afName));
    commandSeq <- sprintf("%s -j 7 %s %s | maf-convert -n tab",
                     ifelse(seqIsFasta, "-Q 0", "-Q 1"),
                     afName, inputFileName);
    system2(command="lastal", args=unlist(strsplit(commandSeq, " ")), stdout=TRUE) -> res.text;
    if(length(res.text) > 0){
        res <- read_tsv(
            pipe(paste0("lastal ", commandSeq)),
            comment="#", show_col_types = FALSE,
            col_types="nciiciciicicc",
            col_names=c("score",
                        "name1","start1","alnSize1","strand1","seqSize1",
                        "name2","start2","alnSize2","strand2","seqSize2",
                        "blocks", "EG2"));
        resList <- c(resList, list(res));
    } else {
        resList <- c(resList, list(NULL));
    }
}
names(resList) <- adapterFile;

res <- list_rbind(resList);

if(nrow(res) == 0){
    cat("No alignments found, sorry!\n");
    quit(save = "no", status=0);
}

res <- res[order(res$seqSize1),];
res$end1 <- res$start1 + res$alnSize1;
res$end2 <- ifelse(res$strand2 == "+", res$start2 + res$alnSize2, res$seqSize2 - res$start2);
res$start2Alt <- res$end2 - res$alnSize2;
cat(" done\n");

if(verbosity > 0){
    options(width=130);
    print(res[order(res$name2),c("name1", "start1", "end1", "seqSize1", "name2", "strand2", "start2Alt", "end2", "seqSize2")]);
}

match.cols <- brewer.pal(8, "Accent");

match.cols <-
    sapply(c(res$name1, paste(res$name1,"[RC]")),
           function(x){sum(as.integer(digest(x, raw=TRUE))) %% 8})+1;
match.cols[!is.na(match.cols)] <- brewer.pal(8, "Accent")[match.cols];

setwd(currentDir);

sw10 <- seqWidth / 10;

cat("Annotating reads...\n");
writtenSeqs <- NULL;
numSeqs <- length(unique(res$name2));
countWidth <- ceiling(log10(numSeqs));
readCount <- 1;
for(readName in names(dnaSeqFile)){
    readNameShort <- sub(" .*$","",readName);
    slens <- length(dnaSeqFile[[readName]]);
    res.sub <- res %>% subset(name2 == readNameShort);
    if(nrow(res.sub) == 0){
        next;
    }
    png(sprintf(paste0("annotated_reads_%0",countWidth,"d.png"), readCount),
        width=seqWidth * 13 + 250, height=(slens / seqWidth + 20) * 30, pointsize=20);
    cat(sprintf(paste0("  %s [%",countWidth,"d of %d]\n"),
                readNameShort, readCount, numSeqs));
    ## Default to solarised black background
    par(bg="#002b36", fg="#839496", col="#839496",
        col.axis="#839496", col.lab="#839496",
        col.main="#839496", col.sub="#839496",
        lwd=2, mar=c(0.5,5,5,0.5));
    plot(NA,xlim=c(0,seqWidth+1), ylim=c(slens / sw10 + 2, -10),
         axes=FALSE, ylab = "Sequence Location (bp)",
         xlab="", main=gsub("(.{40,80}) ", "\\1\n", readName, perl=TRUE));
    axis(2, at=seq(0,(slens / sw10 + 2), by=10), labels = seq(0,(slens / sw10 + 2), by=10) * sw10+1, las=2);
    drawSeq(dnaSeqFile[[readName]], lineLength=seqWidth);
    if(length(rawSignal) > 0){
        if(is.null(rawSignals[[readName]])){
            drawSignal(rawSignal, signalPositions, lineLength=seqWidth);
        } else {
            drawSignal(rawSignals[[readName]],
                       signalPoss[[readName]], lineLength=seqWidth);
        }
    }
    alnDirs <- res.sub$strand2;
    readStartPos <- res.sub$start2;
    readLen <- res.sub$alnSize2;
    readStartPosRC <- readLen - readStartPos;
    for(p in seq_along(alnDirs)){
        origAdaptName <- res.sub$name1[p];
        adaptName <- res.sub$name1[p];
        alnDirs.p <- alnDirs[p];
        readStartPos.p <- readStartPos[p];
        adaptStartPos.p <- res.sub$start1[p];
        readLen.p <- readLen[p];
        readMatchLen.p <- res.sub$alnSize2[p];
        adaptMatchLen.p <- res.sub$alnSize1[p];
        adaptLen.p <- res.sub$seqSize1[p];
        readLen.p <- res.sub$seqSize2[p];
        adaptName.p <- res.sub$name1[p];
        if(alnDirs.p == "-"){
            adaptName.p <- paste(adaptName.p, "[RC]");
        }
        adaptName.p <- paste0(adaptName.p, ":", adaptStartPos.p+1, "-",
                         adaptStartPos.p + adaptMatchLen.p+1);
        if(alnDirs.p == "-"){
            subSeq <- dnaSeqFile[[readName]][(readLen.p - readStartPos.p - readMatchLen.p):(readLen.p - readStartPos.p)];
            drawSeq(subSeq, startPos=readLen.p - readStartPos.p - readMatchLen.p, col=match.cols[origAdaptName],
                    annotate=adaptName.p, lineLength=seqWidth);
        } else {
            subSeq <- dnaSeqFile[[readName]][(readStartPos.p+1):(readStartPos.p+readMatchLen.p)];
            drawSeq(subSeq, startPos=readStartPos.p+1, col=match.cols[origAdaptName],
                    annotate=adaptName.p, lineLength=seqWidth);
        }
    }
    invisible(dev.off());
    readCount <- readCount+1;
}
cat("all done!\n");


cat("Write directory: ", getwd(), "\n", sep="");
if(readCount >= 3){
    cat(" created [ ",
    sprintf(paste0("'annotated_reads_%0",countWidth,"d.png'"), 1),
    " .. ",
    sprintf(paste0("'annotated_reads_%0",countWidth,"d.png'"), readCount-1),
    " ]\n", sep="");
} else {
cat(" created ",
    sprintf(paste0("'annotated_reads_%0",countWidth,"d.png'"), 1),
    "\n", sep="");
}
