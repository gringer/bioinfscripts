#!/usr/bin/env Rscript

suppressMessages(library(viridis));
suppressMessages(library(tidyverse));

options(dplyr.summarise.inform = FALSE);

## Read length distribution plotter; expects input files named 'lengths_<id>.txt[.gz]'
## The first [space-separated] column will be processed as sequence lengths
## If a second field exists of the form "q[0-9]+", it will be processed as read qualities

usage <- function() {
    cat("usage: ./length_plot.r lengths_[0-9]*.txt [options]\n");
    cat("\nOther Options:\n");
    cat("-n             : Plot values\n");
    cat("-h             : Plot horizontally\n");
    cat("-c             : Combine plots\n");
    cat("-m <file1>,... : Load mapping statistics from files\n");
    cat("-r <int> <int> : Set graph range\n");
    cat("-help          : Only display this help message\n");
    cat("\n");
}

scriptArgs <- commandArgs(TRUE);

fileNames <- NULL;
mappingFileNames <- NULL;
mappingQualities <- NULL;
plotVals <- TRUE;
plotHoriz <- TRUE;
plotCombined <- FALSE;
plotRange <- NULL;

## identify files explicitly mentioned on the command line
argLoc <- 1;

while(!is.na(scriptArgs[argLoc])) {
  if(scriptArgs[argLoc] == "-n") {
    plotVals <- FALSE;
  }
  else if(scriptArgs[argLoc] == "-h") {
    plotHoriz <- FALSE;
  }
  else if(scriptArgs[argLoc] == "-c") {
    plotCombined <- TRUE;
  }
  else if(scriptArgs[argLoc] == "-r") {
    plotRange <- as.numeric(scriptArgs[c(argLoc+1, argLoc+2)]);
    argLoc <- argLoc + 2;
  }
  else if(scriptArgs[argLoc] == "-m") {
    mappingFileNames <- unlist(strsplit(scriptArgs[argLoc+1], ","));
    argLoc <- argLoc + 1;
  }
  else if(file.exists(scriptArgs[argLoc])) {
      fileNames <- c(fileNames, scriptArgs[argLoc]);
  } else {
      cat("Error: bad argument, or file not found:", scriptArgs[argLoc],"\n");
      usage();
      quit(save="no");
  }
  argLoc <- argLoc + 1;
}

## fall-through to use files in the current directory
if(length(fileNames) == 0) {
    fileNames <- list.files(pattern="lengths_.*\\.txt(\\.gz)?");
}

if(length(fileNames) == 0) {
    cat("Error: no files to process!\n");
    usage();
    quit(save = "no", status=0);
}

valToSci <- function(val, unit = "") {
    sci.prefixes <- c("", "k", "M", "G", "T", "P", "E", "Z", "Y");
    units <- rep(paste(sci.prefixes,unit,sep=""), each=3);
    logRegion <- floor(log10(val))+1;
    conv.units <- units[logRegion];
    conv.div <- 10^rep(0:(length(sci.prefixes)-1) * 3, each = 3)[logRegion];
    conv.val <- val / conv.div;
    conv.val[val == 0] <- 0;
    conv.units[val == 0] <- unit;
    return(sprintf("%s %s",conv.val,conv.units));
}

#mappingFileNames <- list.files(pattern="^LAST_");

mapping.tbl <- tibble(readName = character(), refName = character(), 
                      dir = character(), len = numeric(),
                      start = numeric(), end = numeric(),
                      gci = numeric(), mapQuality = numeric());
mappingQualities <- NULL;

if(length(mappingFileNames) > 0) {
  ## load readID / quality from mapping files
  for(fName in mappingFileNames) {
    if(file.exists(fName)) {
        cat("Loading read mapping qualities from '", fName, "'... ", sep="");
        file.data <- read_csv(fName, show_col_types=FALSE);
      mapping.tbl <- 
        bind_rows(mapping.tbl, read_csv(fName, show_col_types=FALSE) %>% 
                               mutate(gci = ifelse(gci >= 100, 100 * ((qML-0.5) / qML), gci),
                                      mapQuality = -10 * log10((100-gci)/100)) %>%
                               arrange(qML) %>%
                               distinct(query, .keep_all=TRUE) %>%
                    ## accuracy is limited to at least 0.5 errors across the entire length
                    select(readName=query, refName=target, dir, len=tL, start=tS, end=tE, gci, mapQuality)); 
      cat("done\n");
    }
  }
}

## generate fibonacci bins for data storage
sampled.lengths <- NULL;
hasQual <- FALSE;
hasATLength <- FALSE;
data.all <- tibble(bc = character(), readLength = numeric(), readQuality = numeric(), ATLength = numeric(),
                   readName = character());
qual.data <- NULL;
lengthStats <- data.frame(bc=NULL, n=NULL, mean=NULL, l10=NULL, l50=NULL, l90=NULL);
qualStats <- data.frame(bc=NULL, min=NULL, max=NULL, mean=NULL, median=NULL,
                        mode=NULL, p10=NULL, p90=NULL);
## read from files
dens.mat <- sapply(fileNames, function(x) {
  cat("Loading length statistics from '", x, "'... ", sep="");
    seqd.label <- sub(".txt(.gz)?$","",
                      sub("^lengths_(called_)?(barcode)?","",basename(x)));
  file.data <- read_table(x, col_names=FALSE, guess_max = 10000, show_col_types = FALSE);
  readNames <- unlist(file.data[,ncol(file.data)]);
  length.data <- file.data$X1;
  hasQualLocal <- ((ncol(file.data) > 1) && all(grepl("^q[0-9]+", head(file.data$X2))));
  hasQual <<- hasQual || hasQualLocal;
  hasATLengthLocal <- (((ncol(file.data) > 1) && hasQualLocal && all(grepl("^at[0-9]+", head(file.data$X3)))) ||
                       ((ncol(file.data) > 1) && !hasQualLocal && all(grepl("^at[0-9]+", head(file.data$X2)))));
  hasATLength <<- hasATLength || hasATLengthLocal;
  if(hasQualLocal) {
      qual.data <- as.numeric(sub("^q", "", file.data$X2));
      if(hasATLengthLocal) {
          at.data <- as.numeric(sub("^at", "", file.data$X3));
          data.all <<- dplyr::bind_rows(data.all,
                                        tibble(bc = seqd.label, readLength = length.data,
                                               readQuality = qual.data, ATLength = at.data,
                                               readName = file.data$X4));
      } else {
          data.all <<- dplyr::bind_rows(data.all,
                                        tibble(bc = seqd.label, readLength = length.data,
                                               readQuality = qual.data,
                                               readName = file.data$X3));
        }
    } else {
        if(length(length.data) > 20) {
            if(hasATLengthLocal) {
                at.data <- as.numeric(sub("^at", "", file.data$X2));
                data.all <<- dplyr::bind_rows(data.all,
                                              tibble(bc = seqd.label, readLength = length.data, ATLength = at.data,
                                                     readName = file.data$X3));
            } else {
                data.all <<- dplyr::bind_rows(data.all,
                                              tibble(bc = seqd.label, readLength = length.data,
                                                     readName = file.data$X2));
            }
        } else {
            cat(" done [no data; discarded]\n");
            return(NULL);
        }
    }
    len.sort <- sort(length.data, decreasing=TRUE);
    len.cumSum <- cumsum(as.numeric(len.sort));
    len.total <- sum(length.data);
    len.mean <- round(len.total / length(length.data));
    len.l10 <- len.sort[min(which(len.cumSum >= len.total * 0.1))];
    len.l50 <- len.sort[min(which(len.cumSum >= len.total * 0.5))];
    len.l90 <- len.sort[min(which(len.cumSum >= len.total * 0.9))];
    lengthStats <<- rbind(lengthStats,
                       data.frame(bc=seqd.label, n=length(length.data), mean=len.mean,
                                  l10=len.l10, l50=len.l50, l90=len.l90));
    if(hasQualLocal) {
        qualStats <<-
            rbind(qualStats,
                  data.frame(
                      bc=seqd.label,
                      min=paste0("q", min(qual.data)), max=paste0("q", max(qual.data)),
                      mean=paste0("q", round(mean(qual.data))),
                      median=paste0("q", round(median(qual.data))),
                      mode=names(sort(table(qual.data), decreasing=TRUE))[1],
                      p10=quantile(qual.data, 0.1), p90=quantile(qual.data, 0.9)));
    }
    cat(" done\n");
});

data.all %>% pull(bc) %>% unique() -> fileNames;
rownames(lengthStats) <- lengthStats$bc;
lengthStats$bc <- NULL;
cat("Length statistics:\n");
print(lengthStats);
if(hasQual) {
    rownames(qualStats) <- qualStats$bc;
    qualStats$bc <- NULL;
    cat("Quality statistics:\n");
    print(qualStats);
}

options(scipen=15);

data.all %>%
    group_by(bc, readLength) %>%
    summarise(readCount = n()) %>%
    ungroup() %>%
    group_by(bc) %>%
    arrange(readLength) %>%
    mutate(calledBases = readCount * readLength) %>%
    mutate(readProportion = readCount / sum(readCount)) %>%
    mutate(baseProportion = calledBases / sum(calledBases)) %>%
    mutate(normalisedBases = calledBases / max(calledBases)) %>%
    mutate(cumulativeReadProportion = c(100, head((1 - cumsum(readProportion)) * 100, -1))) %>%
    mutate(cumulativeBaseProportion = c(100, head((1 - cumsum(baseProportion)) * 100, -1))) %>%
    arrange(readLength) -> data.countSummary;

options(width=150);

data.countSummary %>%
    filter(cumulativeReadProportion < 99) %>%
    group_by(bc) %>%
    summarise(minReadLength = min(readLength)) %>%
    pull(minReadLength) %>%
    min() -> readLengthMin;

data.countSummary %>%
    filter(cumulativeBaseProportion > 1) %>%
    group_by(bc) %>%
    summarise(maxReadLength = max(readLength)) %>%
    pull(maxReadLength) %>%
    max() -> readLengthMax;

if(length(plotRange) == 0) {
    plotRange <- c(readLengthMin, readLengthMax);
}
cat("Plot range (bp): ", plotRange, "\n");

data.all %>%
    filter(readLength >= plotRange[1], readLength <= plotRange[2]) %>%
    mutate(bc = factor(bc)) -> data.all;

lineCols <- (viridis_pal(option="H", begin=0.1, end=0.9))(length(fileNames));

outFileName <- sprintf("%s_sequence_QC.pdf", Sys.Date());
pdf(outFileName, width=11, height=8);
cat("Creating plots...\n");
if(hasQual) {
  ## Create quality vs length plot
  cat(" Length vs Quality plot\n");
  dataRange <- range(data.all$readQuality, na.rm=TRUE);  
  data.all %>%
    filter(!is.na(readQuality)) %>%  
    mutate(readLength = log10(readLength)) %>%
    ggplot() +
    aes(x = readLength, y = readQuality) +
    xlab("Read Length") +
    ylab("Predicted / Modeled Read Quality") +
    ggtitle("Predicted / Modeled Read Quality vs Length Distribution") +
    ylim(min(dataRange), max(dataRange)) +
    geom_hex(bins=diff(dataRange), mapping=aes(fill=after_stat(density))) +
    scale_fill_gradientn(colours = colorRampPalette(viridis(250), bias=3)(250), na.value="black") +
    scale_x_continuous(labels = function(x) {paste0(valToSci(10^x, "bp"))},
                       minor_breaks = function(x) {
                         x <- 10^x;
                         breakPoss <- rep(1:9,17) * 10^rep(0:16, each=9);
                         log10(breakPoss[(breakPoss >= x[1]) & (breakPoss <= x[2])])},
                       breaks = function(x) {
                         x <- 10^x;
                         breakPoss <- rep(c(1,2,5),17) * 10^rep(0:16, each=3);
                         log10(breakPoss[(breakPoss >= x[1]) & (breakPoss <= x[2])])}
                       ) +
    theme_bw() +
    theme(panel.grid.minor = element_line(linewidth = 0.25, color="grey90"),
          panel.grid.major = element_line(linewidth = 0.75, color="grey90"),
          aspect.ratio = 1,
          axis.text.x = element_text(angle = 45, hjust=1)) +
    facet_wrap(~ bc) -> out.plot;
    print(out.plot);
    ## cat(" Quality distribution plot\n");
    ## data.all %>%
    ##     filter(!is.na(readQuality)) %>% print();
    ## data.all %>%
    ##     filter(!is.na(readQuality)) %>%
    ##     aes(x = readQuality, group=bc, col=bc) +
    ##     xlab("Predicted / Modeled Read Quality") +
    ##     ggtitle("Predicted / Modeled Read Quality Distribution") +
    ##     ylim(min(data.all$readQuality), max(data.all$readQuality)) +
    ##     geom_density() +
    ##     scale_color_gradientn(colours = colorRampPalette(viridis(250), bias=3)(250), na.value="black") +
    ##     theme_bw() -> out.plot;
    ## print(out.plot);
    if(nrow(mapping.tbl) > 0) {
        cat(" Mapped vs Predicted Quality plot\n");
        mapRange <- range(mapping.tbl$mapQuality, na.rm=TRUE);
        graphRange <- c(min(mapRange, dataRange), max(mapRange, dataRange));
        data.all %>%
            filter(!is.na(readQuality)) %>%  
            inner_join(mapping.tbl, by="readName") %>%
            filter(!is.na(mapQuality)) %>%
            ggplot() +
            aes(x = readQuality, y = mapQuality) +
            xlab("Predicted / Modeled Quality") +
            ylab("Mapped Quality") +
            xlim(min(graphRange), max(graphRange)) +
            ylim(min(graphRange), max(graphRange)) +
            ggtitle("Mapped vs Predicted Quality Distribution") +
            geom_hex(bins=diff(graphRange),
                     mapping=aes(fill=after_stat(density))) +
            geom_abline(slope = 1, intercept = 0, colour="#00000030", linewidth=3) +
            scale_fill_gradientn(colours = colorRampPalette(viridis(250), bias=3)(250)) +
            theme_bw() +
            theme(panel.grid.minor = element_line(linewidth = 0.25, color="grey90"),
                  panel.grid.major = element_line(linewidth = 0.75, color="grey90"),
                  aspect.ratio = 1,
                  axis.text.x = element_text(angle = 45, hjust=1)) +
            facet_wrap(~ bc) -> out.plot;
        print(out.plot);
    }
} else {
    cat("No quality data found\n");
}

print(warnings());

## Read count
cat(" Read count\n");
data.countSummary %>%
  filter(readLength >= plotRange[1], readLength <= plotRange[2]) %>%
  ggplot() +
  aes(x = readLength, y = readCount, group = bc, col=bc) +
  xlab("Read Length") +
  ylab("Sequenced reads") +
  geom_line() +
  scale_y_continuous(labels = function(x) {paste0(valToSci(x))}) +
  scale_x_log10(labels = function(x) {paste0(valToSci(x, "bp"))},
                minor_breaks = function(x) {
                    breakPoss <- rep(1:9,17) * 10^rep(0:16, each=9);
                    breakPoss[(breakPoss >= x[1]) & (breakPoss <= x[2])]},
                breaks = function(x) {
                    breakPoss <- rep(c(1,2,5),17) * 10^rep(0:16, each=3);
                    breakPoss[(breakPoss >= x[1]) & (breakPoss <= x[2])]},
                limits = plotRange) +
  scale_colour_viridis_d(option="H", begin=0.1, end=0.9, name="Read Group") +
  theme_bw(base_size=18) +
  theme(panel.grid.minor = element_line(linewidth = 0.25, color="grey90"),
        panel.grid.major = element_line(linewidth = 0.25, color="grey90"),
        legend.position.inside = c(0.99, 0.99),
        legend.background = element_rect(fill = "#FFFFFFA0"),
        legend.justification = c("right", "top"),
        legend.text = element_text(size=12),
        legend.title = element_blank(),
        legend.box.just = "right",
        legend.spacing.y = unit(0, "mm"),
        axis.text.x = element_text(angle = 45, hjust=1)) -> out.plot;
print(out.plot);

## Base count
cat(" Called bases\n");
data.countSummary %>%
  filter(readLength >= plotRange[1], readLength <= plotRange[2]) %>%
  ggplot() +
  aes(x = readLength, y = calledBases, group = bc, col=bc) +
  xlab("Read Length") +
  ylab("Sequenced bases") +
    scale_y_continuous(labels = function(x) {
        paste0(valToSci(x, "bp"))
    }) +
  geom_line() +
  scale_x_log10(labels = function(x) {paste0(valToSci(x, "bp"))},
                minor_breaks = function(x) {
                    breakPoss <- rep(1:9,17) * 10^rep(0:16, each=9);
                    breakPoss[(breakPoss >= x[1]) & (breakPoss <= x[2])]},
                breaks = function(x) {
                    breakPoss <- rep(c(1,2,5),17) * 10^rep(0:16, each=3);
                    breakPoss[(breakPoss >= x[1]) & (breakPoss <= x[2])]},
                limits = plotRange) +
  scale_colour_viridis_d(option="H", begin=0.1, end=0.9, name="Read Group") +
  theme_bw(base_size=18) +
  theme(panel.grid.minor = element_line(linewidth = 0.25, color="grey90"),
        panel.grid.major = element_line(linewidth = 0.25, color="grey90"),
        legend.position.inside = c(0.99, 0.99),
        legend.background = element_rect(fill = "#FFFFFFA0"),
        legend.justification = c("right", "top"),
        legend.text = element_text(size=12),
        legend.title = element_blank(),
        legend.box.just = "right",
        legend.spacing.y = unit(0, "mm"),
        axis.text.x = element_text(angle = 45, hjust=1)) -> out.plot;
print(out.plot);

## Base count
cat(" Called base proportion\n");
data.countSummary %>%
  filter(readLength >= plotRange[1], readLength <= plotRange[2]) %>%
  ggplot() +
  aes(x = readLength, y = baseProportion, group = bc, col=bc) +
  xlab("Read Length") +
  ylab("Sequenced Base Proportion") +
  geom_line() +
    scale_x_log10(labels = function(x) {
        paste0(valToSci(x, "bp"))
    },
                minor_breaks = function(x) {
                    breakPoss <- rep(1:9,17) * 10^rep(0:16, each=9);
                    breakPoss[(breakPoss >= x[1]) & (breakPoss <= x[2])]},
                breaks = function(x) {
                    breakPoss <- rep(c(1,2,5),17) * 10^rep(0:16, each=3);
                    breakPoss[(breakPoss >= x[1]) & (breakPoss <= x[2])]},
                limits = plotRange) +
  scale_colour_viridis_d(option="H", begin=0.1, end=0.9, name="Read Group") +
  theme_bw(base_size=18) +
  theme(panel.grid.minor = element_line(linewidth = 0.25, color="grey90"),
        panel.grid.major = element_line(linewidth = 0.25, color="grey90"),
        legend.position.inside = c(0.99, 0.99),
        legend.background = element_rect(fill = "#FFFFFFA0"),
        legend.justification = c("right", "top"),
        legend.text = element_text(size=12),
        legend.title = element_blank(),
        legend.box.just = "right",
        legend.spacing.y = unit(0, "mm"),
        axis.text.x = element_text(angle = 45, hjust=1)) -> out.plot;
print(out.plot);

## Cumulative base proportion
cat(" Cumulative base proportion\n");
data.countSummary %>%
  filter(readLength >= plotRange[1], readLength <= plotRange[2]) %>%
  group_by(bc) %>%
  summarise(L10 = head(readLength[cumulativeBaseProportion <= 10],1),
            L50 = head(readLength[cumulativeBaseProportion <= 50],1),
            L90 = head(readLength[cumulativeBaseProportion <= 90],1)) %>%
  pivot_longer(starts_with("L"), names_to="lStat", values_to="readLength") %>%
  mutate(lStat = factor(lStat, levels = c("L90", "L50", "L10")),
         percentile = sub("^L", "", lStat) %>% as.numeric()) -> percentile.points;
data.countSummary %>%
  filter(readLength >= plotRange[1], readLength <= plotRange[2]) %>%
  ggplot() +
  aes(x = readLength, y = cumulativeBaseProportion, group = bc, col=bc) +
  xlab("Read Length") +
  ylab("Cumulative Sequenced Bases (%)") +
  geom_line() +
  geom_point(aes(y=percentile, x=readLength), data=percentile.points, pch=21) +
  geom_segment(aes(y=0, yend=percentile, x=readLength, xend=readLength, linetype=lStat), 
               data=percentile.points) +
  scale_y_continuous(breaks=0:10 * 10) +
  scale_x_log10(labels = function(x) {paste0(valToSci(x, "bp"))},
                minor_breaks = function(x) {
                    breakPoss <- rep(1:9,17) * 10^rep(0:16, each=9);
                    breakPoss[(breakPoss >= x[1]) & (breakPoss <= x[2])]},
                breaks = function(x) {
                    breakPoss <- rep(c(1,2,5),17) * 10^rep(0:16, each=3);
                    breakPoss[(breakPoss >= x[1]) & (breakPoss <= x[2])]},
                limits = plotRange) +
  scale_colour_viridis_d(option="H", begin=0.1, end=0.9, name="Read Group") +
  theme_bw(base_size=18) +
  theme(panel.grid.minor = element_line(linewidth = 0.25, color="grey95"),
        panel.grid.major = element_line(linewidth = 0.25, color="grey95"),
        legend.position.inside = c(0.99, 0.99),
        legend.background = element_rect(fill = "#FFFFFFA0"),
        legend.justification = c("right", "top"),
        legend.text = element_text(size=12),
        legend.title = element_blank(),
        legend.box.just = "right",
        legend.spacing.y = unit(0, "mm"),
        axis.text.x = element_text(angle = 45, hjust=1))  +
  guides(colour = guide_legend(order = 1),
         linetype = guide_legend(order = 2)) -> out.plot;
print(out.plot);

## Digital electrophoresis ##
## Set up grid and tick marks
xBreaks <- 10^seq(log10(plotRange[1]),
                  log10(plotRange[2]),
                  length.out=256);
breakLimits <- log10(range(xBreaks));
tickPoss <- rep(1:9, 17) * 10 ^ rep(0:16, each=9); 
tickPoss <- tickPoss[between(log10(tickPoss), breakLimits[1], breakLimits[2])];
names(tickPoss) <- valToSci(tickPoss, "bp");
tickPoss <- (log10(tickPoss) - breakLimits[1]) / (diff(breakLimits)) * 256;
majorPoss <- rep(c(1,2,5), 17) * 10 ^ rep(0:16, each=3);
majorPoss <- majorPoss[between(log10(majorPoss), breakLimits[1], breakLimits[2])];
names(majorPoss) <- valToSci(majorPoss, "bp");
majorPoss <- (log10(majorPoss) - breakLimits[1]) / (diff(breakLimits)) * 256;
## show raw plot
cat(" Raw digital electrophoresis\n");
data.countSummary %>%
    ungroup() %>%
    mutate(gridX = cut(readLength, c(0,xBreaks,Inf), right = TRUE, labels=c(xBreaks, Inf))) %>%
    complete(bc, gridX) %>%
    group_by(gridX, bc) %>%
    summarise(calledBases = sum(calledBases), na.rm=TRUE) %>%
    ungroup() %>%
    mutate(calledBases = calledBases / max(calledBases, na.rm=TRUE)) %>%
    mutate(bc = gsub("_", " ", bc)) %>%
    ggplot() +
    aes(x = as.numeric(gridX), y = bc, fill = calledBases) +
    ggtitle("Digital Electrophoresis (raw densities)") +
    xlab("Read Length") +
    ylab("") +
    geom_raster() +
    scale_x_continuous(minor_breaks = tickPoss, breaks = majorPoss) +
    scale_fill_gradientn(colours = colorRampPalette(hsv(h=27/360,s=1,v=seq(0,1,by=0.001)), bias=2.25)(250), na.value="black") +
    guides(fill = "none") +
    geom_hline(yintercept = (2:length(unique(data.countSummary$bc))-0.5), lwd=2, color="white") +
    theme_bw(base_size=18) +
    theme(panel.border = element_blank(),
          panel.background = element_blank(),
          aspect.ratio = 1,
          panel.grid.minor.x = element_line(color="#80808040"),
          panel.grid.major.y = element_blank(),
          panel.grid.major.x = element_line(color = "#80808080",
                                            linewidth = 0.75,
                                            linetype = 2),
          panel.ontop = TRUE,
          axis.text.x = element_text(angle = 45, hjust=1)) -> out.plot;
print(out.plot) |> suppressWarnings();

## show normalised plot
cat(" Normalised digital electrophoresis\n");
data.countSummary %>%
  group_by(bc) %>%
  ungroup() %>%
  mutate(gridX = cut(readLength, c(0,xBreaks,Inf), right = TRUE, labels=c(xBreaks, Inf))) %>%
  complete(bc, gridX) %>%
    mutate(bc = gsub("_", " ", bc), gridX = as.numeric(gridX)) %>%
    group_by(gridX, bc) %>%
    summarise(normalisedBases = sum(normalisedBases), na.rm=TRUE) %>%
    ungroup() %>%
    group_by(bc) %>%
    mutate(normalisedBases = normalisedBases / max(normalisedBases, na.rm=TRUE)) %>%
  ggplot() +
  aes(x = gridX, y = bc, fill = normalisedBases) +
    ggtitle("Digital Electrophoresis (lane-normalised)") +
  xlab("Read Length") +
  ylab("") +
  geom_raster() +
  scale_x_continuous(minor_breaks = tickPoss, breaks = majorPoss) +
  scale_fill_gradientn(colours = colorRampPalette(hsv(h=27/360,s=1,v=seq(0,1,by=0.001)), bias=2.25)(250), na.value="black") +
  guides(fill = "none") +
  geom_hline(yintercept = (2:length(unique(data.countSummary$bc))-0.5), lwd=2, color="white") +
  theme_bw(base_size=18) +
  theme(panel.border = element_blank(),
        panel.background = element_blank(),
        aspect.ratio = 1,
        panel.grid.minor.x = element_line(color="#80808040"),
        panel.grid.major.y = element_blank(),
        panel.grid.major.x = element_line(color = "#80808080",
                                          linewidth = 0.75,
                                          linetype = 2),
        panel.ontop = TRUE,
        axis.text.x = element_text(angle = 45, hjust=1)) -> out.plot;
print(out.plot);

if(hasATLength) {
    ## A/T homopolymer length
    data.all %>%
        filter(ATLength > 1) %>%
        filter(!is.na(ATLength)) %>%
        group_by(bc, ATLength) %>%
        summarise(readCount = n()) %>%
        group_by(bc) %>%
        mutate(readProportion = readCount / sum(readCount)) %>%
        mutate(normalisedReads = readProportion / max(readProportion[ATLength > 0])) %>%
        mutate(cumulativeReadProportion = c(100, head((1 - cumsum(readProportion)) * 100, -1))) %>%
        arrange(ATLength) %>%
        ungroup() -> data.at.countSummary;
    
    data.at.countSummary %>%
        group_by(bc) %>%
        summarise(minATL = min(ATLength[readCount > 10]), maxATL = quantile(ATLength[cumulativeReadProportion > 1], 0.99),
                  countMax = sum(readCount[ATLength >= maxATL])) %>%
        pull(maxATL) %>%
        max() -> maxATLength;
    print(maxATLength);
    
    cat(" A/T Homopolymer read proportion\n");
    data.at.countSummary %>%
        mutate(ATLength = pmin(ATLength, maxATLength)) %>%
        ggplot() +
        aes(x = ATLength, y = readProportion, group = bc, col=bc) +
        xlab("Sequenced A/T Homopolymer Length") +
        ylab("Read Proportion") +
        geom_line() +
        scale_colour_viridis_d(option="H", begin=0.1, end=0.9, name="Read Group") +
        theme_bw(base_size=18) +
        theme(panel.grid.minor = element_line(linewidth = 0.25, color="grey95"),
              panel.grid.major = element_line(linewidth = 0.25, color="grey95"),
              legend.position.inside = c(0.99, 0.99),
              legend.background = element_rect(fill = "#FFFFFFA0"),
              legend.justification = c("right", "top"),
              legend.text = element_text(size=12),
              legend.title = element_blank(),
              legend.box.just = "right",
              legend.spacing.y = unit(0, "mm"),
              axis.text.x = element_text(angle = 45, hjust=1)) -> out.plot;
    print(out.plot);
  
    cat(" A/T Homopolymer cumulative read proportion\n");
    data.at.countSummary %>%
        mutate(ATLength = pmin(ATLength, maxATLength)) %>%
        ggplot() +
        aes(x = ATLength, y = cumulativeReadProportion, group = bc, col=bc) +
        xlab("Sequenced A/T Homopolymer Length") +
        ylab("Cumulative Reads (%)") +
        geom_line() +
        scale_y_continuous(breaks=0:10 * 10) +
        scale_colour_viridis_d(option="H", begin=0.1, end=0.9, name="Read Group") +
        theme_bw(base_size=18) +
        theme(panel.grid.minor = element_line(linewidth = 0.25, color="grey95"),
              panel.grid.major = element_line(linewidth = 0.25, color="grey95"),
              legend.position.inside = c(0.99, 0.99),
              legend.background = element_rect(fill = "#FFFFFFA0"),
              legend.justification = c("right", "top"),
              legend.text = element_text(size=12),
              legend.title = element_blank(),
              legend.box.just = "right",
              legend.spacing.y = unit(0, "mm"),
              axis.text.x = element_text(angle = 45, hjust=1)) -> out.plot;
    print(out.plot);
  
  ## show normalised plot
    cat(" Normalised AT Length digital electrophoresis\n");
    data.at.countSummary %>%
        mutate(ATLength = pmin(ATLength, maxATLength)) %>%
        ungroup() %>%
        mutate(gridX = cut(ATLength, seq(0, max(ATLength), length.out=min(max(ATLength), 512)),
                           include.lowest = TRUE, right=TRUE,
                           labels = tail(seq(0, max(ATLength), length.out=min(max(ATLength), 512)), -1))) %>%
        complete(bc, gridX) %>%
        mutate(bc = gsub("_", " ", bc)) %>%
        ggplot() +
        ggtitle("A/T Homopolymer Digital Electrophoresis") +
        aes(x = as.numeric(gridX), y = bc, fill = normalisedReads) +
        xlab("Sequenced A/T Homopolymer Length") +
        ylab("") +
        geom_raster() +
        scale_fill_gradientn(colours = colorRampPalette(hsv(h=27/360,s=1,v=seq(0,1,by=0.001)), bias=1.25)(250),
                             na.value="black") +
        guides(fill = "none") +
        geom_hline(yintercept = (2:length(unique(data.countSummary$bc))-0.5), lwd=2, color="white") +
        theme_bw(base_size=18) +
        theme(panel.border = element_blank(),
              panel.background = element_blank(),
          aspect.ratio = 1,
          panel.grid.minor.x = element_line(color="#80808040"),
          panel.grid.major.y = element_blank(),
          panel.grid.major.x = element_line(color = "#80808080",
                                            linewidth = 0.75,
                                            linetype = 2),
          panel.ontop = TRUE,
          axis.text.x = element_text(angle = 45, hjust=1)) -> out.plot;
    print(out.plot);
}

if(nrow(mapping.tbl) > 0) {
    cat(" Subsampled mapping profile plots:\n");
    for(loopBC in (data.all %>% pull(bc) %>% unique())) {
        data.all %>%
            filter(bc == loopBC) %>%
            inner_join(mapping.tbl, by="readName") %>%
            group_by(refName) %>%
            filter(n() > 10) -> sub.tbl;
        data.all %>%
            filter(bc == loopBC) %>%
            inner_join(mapping.tbl, by="readName") %>%
            group_by(refName) %>%
            distinct(len) %>%
            pull(len, name=refName) -> targetLengths;
        sub.tbl %>%
            summarise(readCount = n()) %>%
            arrange(readCount) %>%
            ungroup() %>%
            slice_head(n=10) %>%
            pull(readCount, name=refName) -> readCount;
        cat("   ", loopBC, "\n");
        scaleMin <- 0;
        scaleMax <- max(targetLengths);
        if(nrow(sub.tbl) > 10) {
            sub.tbl %>% 
                group_by(refName) %>%
                filter(refName %in% names(readCount), n() > 10) %>%
                slice_sample(n=1000) %>%
                group_by(refName, bc, dir) %>%
                arrange(refName, bc, dir, ifelse(dir == "+", end, start),
                        ifelse(dir == "-", end, start)) %>%
                mutate(row = 1:n()) -> plot.tbl;
            titleLabel <- sprintf("%s (%s reads sampled from %s reads)", loopBC, 
                                        paste(pmin(1000, readCount), collapse = " / "),
                                        paste(readCount, collapse = " / "));
            plot.tbl %>%
                ggplot() +
                ggtitle(label = titleLabel) +
                aes(x = start, xend = end, y = ifelse(dir == "+", row, -row),
                    yend = ifelse(dir == "+", row, -row),
                    col = dir) +
                scale_color_viridis_d(option="H", begin=0.1, end=0.9) +
#                xlim(scaleMin, scaleMax) +
                xlab("Sequence Location") +
                ylab("Read Number") +
                geom_segment(linewidth = 0.5) +
                theme_bw() +
                facet_wrap(~ refName, scales = "free") -> out.plot; 
            print(out.plot);
        }
    }
}

mapping.tbl.tmp <- mapping.tbl

mapping.tbl.tmp %>%
    filter(dir == "+") -> mapping.tbl
if(nrow(mapping.tbl) > 0) {
    cat(" Subsampled mapping profile plots (forward only):\n");
    for(loopBC in (data.all %>% pull(bc) %>% unique())) {
        data.all %>%
            filter(bc == loopBC) %>%
            inner_join(mapping.tbl, by="readName") %>%
            group_by(refName) %>%
            filter(n() > 10) -> sub.tbl;
        data.all %>%
            filter(bc == loopBC) %>%
            inner_join(mapping.tbl, by="readName") %>%
            group_by(refName) %>%
            distinct(len) %>%
            pull(len, name=refName) -> targetLengths;
        sub.tbl %>%
            summarise(readCount = n()) %>%
            arrange(readCount) %>%
            ungroup() %>%
            slice_head(n=10) %>%
            pull(readCount, name=refName) -> readCount;
        cat("   ", loopBC, "\n");
        scaleMin <- 0;
        scaleMax <- max(targetLengths);
        if(nrow(sub.tbl) > 10) {
            sub.tbl %>% 
                group_by(refName) %>%
                filter(refName %in% names(readCount), n() > 10) %>%
                slice_sample(n=1000) %>%
                group_by(refName, bc, dir) %>%
                arrange(refName, bc, dir, ifelse(dir == "+", end, start),
                        ifelse(dir == "-", end, start)) %>%
                mutate(row = 1:n()) -> plot.tbl;
            titleLabel <- sprintf("%s (forward only; %s reads sampled from %s reads)", loopBC, 
                                        paste(pmin(1000, readCount), collapse = " / "),
                                        paste(readCount, collapse = " / "));
            plot.tbl %>%
                ggplot() +
                ggtitle(label = titleLabel) +
                aes(x = start, xend = end, y = ifelse(dir == "+", row, -row),
                    yend = ifelse(dir == "+", row, -row),
                    col = dir) +
                scale_color_viridis_d(option="H", begin=0.1, end=0.9, direction = -1) +
#                xlim(scaleMin, scaleMax) +
                xlab("Sequence Location") +
                ylab("Read Number") +
                geom_segment(linewidth = 0.5) +
                theme_bw() +
                facet_wrap(~ refName, scales = "free") -> out.plot; 
            print(out.plot);
        }
    }
}

mapping.tbl.tmp %>%
    filter(dir == "-") -> mapping.tbl
if(nrow(mapping.tbl) > 0) {
    cat(" Subsampled mapping profile plots (reverse only):\n");
    for(loopBC in (data.all %>% pull(bc) %>% unique())) {
        data.all %>%
            filter(bc == loopBC) %>%
            inner_join(mapping.tbl, by="readName") %>%
            group_by(refName) %>%
            filter(n() > 10) -> sub.tbl;
        data.all %>%
            filter(bc == loopBC) %>%
            inner_join(mapping.tbl, by="readName") %>%
            group_by(refName) %>%
            distinct(len) %>%
            pull(len, name=refName) -> targetLengths;
        sub.tbl %>%
            summarise(readCount = n()) %>%
            arrange(readCount) %>%
            ungroup() %>%
            slice_head(n=10) %>%
            pull(readCount, name=refName) -> readCount;
        cat("   ", loopBC, "\n");
        scaleMin <- 0;
        scaleMax <- max(targetLengths);
        if(nrow(sub.tbl) > 10) {
            sub.tbl %>% 
                group_by(refName) %>%
                filter(refName %in% names(readCount), n() > 10) %>%
                slice_sample(n=1000) %>%
                group_by(refName, bc, dir) %>%
                arrange(refName, bc, dir, ifelse(dir == "+", end, start),
                        ifelse(dir == "-", end, start)) %>%
                mutate(row = 1:n()) -> plot.tbl;
            titleLabel <- sprintf("%s (reverse only; %s reads sampled from %s reads)", loopBC, 
                                        paste(pmin(1000, readCount), collapse = " / "),
                                        paste(readCount, collapse = " / "));
            plot.tbl %>%
                ggplot() +
                ggtitle(label = titleLabel) +
                aes(x = start, xend = end, y = ifelse(dir == "+", row, -row),
                    yend = ifelse(dir == "+", row, -row),
                    col = dir) +
                scale_color_viridis_d(option="H", begin=0.1, end=0.9) +
#                xlim(scaleMin, scaleMax) +
                xlab("Sequence Location") +
                ylab("Read Number") +
                geom_segment(linewidth = 0.5) +
                theme_bw() +
                facet_wrap(~ refName, scales = "free") -> out.plot; 
            print(out.plot);
        }
    }
}

cat(sprintf("done! Created '%s'\n", outFileName));
invisible(dev.off());

