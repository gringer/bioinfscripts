maxRad <- 3;
lineWidth <- 20;

library(viridis);
library(rgl);

concentricWaveH <- function(t, period, zpos){
  ## forms a hemispherical wave in both X and Z directions
  XcyclePos <- ((t / (period / 2)) %% 1) * pi;
  Zadj <- ((zpos / (period)) %% 1 - 0.5) * 2;
  Zscale <- sqrt(1 - Zadj^2);
  cycleZdir <- sign(((zpos / (period * 2) + 1) %% 1 - 0.5) * 2);
  cycleXStart <- floor(t / (period / 2)) * period/2 + period/4;
  cycleYdir <- -(floor(t / (period / 2)) %% 2 - 0.5) * 2;
  matrix(c(x = -(period/4) * cos(XcyclePos) * Zscale + cycleXStart,
           y =  ((period/4) * sin(XcyclePos) * cycleYdir * Zscale * cycleZdir),
           z=rep(zpos, length=length(t))), ncol=3);
}

concentricWaveV <- function(t, period, zpos){
  ## forms a hemispherical wave in both X and Z directions
  XcyclePos <- ((t / (period / 2)) %% 1) * pi;
  Zadj <- ((zpos / (period)) %% 1 - 0.5) * 2;
  Zscale <- sqrt(1 - Zadj^2);
  cycleZdir <- sign(((zpos / (period * 2) + 1) %% 1 - 0.5) * 2);
  cycleXStart <- floor(t / (period / 2)) * period/2 + period/4;
  cycleYdir <- -(floor(t / (period / 2)) %% 2 - 0.5) * 2;
  matrix(c(x =  ((period/4) * sin(XcyclePos) * cycleYdir * Zscale * cycleZdir),
           y = -(period/4) * cos(XcyclePos) * Zscale + cycleXStart,
           z=rep(zpos, length=length(t))), ncol=3);
}

c1 <- "#FF000040"; ##viridis(301, alpha=0.5);
c2 <- "#0000FF40"; ##viridis(301, option="H", alpha=0.5);

library(viridis);

plot3d(NA, xlim=c(-1.25,1.25), ylim=c(-1.25,1.25), zlim=c(-2.75,2.75), pch=16,
       box=FALSE, axes=FALSE);
xShift <- sqrt(3) / 2;
for(frame in 1:100){
  zpos <-  ((frame-1) / 40) - 1;
  waveDir <- floor((zpos+0.5) %% 2);
  c2 <- viridis(40, option="H")[(frame - 1 + 20) %% 40 + 1];
  for(blobs in -1:1){
    if(waveDir == 0){
      lines3d(t(t(concentricWaveV(seq(-1,0.4999,by=0.01), 1, zpos)) * c(1,1,1) + c(0 + blobs * 0.5,0,0)),
              col=c2, pch=16, lwd=3);
      #lines3d(t(t(concentricWaveV(seq(-1,0.4999,by=0.01), 1, zpos)) * c(-1,1,1) + c(0 + (blobs+1) * 0.5,0,0)), 
      #        col=c2, pch=16, lwd=3);
    } else {
      #lines3d(t(t(concentricWaveH(seq(-0.5,0.7499,by=0.01), 1, zpos)) * c(1,1,1) + c(0.25,-0.25 + blobs * 0.5,0)),
      #        col=c2, pch=16, lwd=3);
      lines3d(t(t(concentricWaveH(seq(-0.5,0.7499,by=0.01), 1, zpos)) * c(-1,1,1) + c(0.25,-0.25 + blobs * 0.5,0)), 
              col=c2, pch=16, lwd=3);
    }
  }
  rgl.snapshot(sprintf("frame_%03d.png",frame));
}

concentricRipple <- function(startPos, endPos, zpos, dir=0, numPieces = 100){
  ## forms a double hemispherical wave with distance-correcting arc ripples
  ## in both X (or Y) and Z directions
  ## |<----- one period ----->|
  ##     /---\
  ## B /   A  \B /\       /\
  ##\/         \/B \  A  /B 
  ##                \---/
  ## A: primary (sphere) radius; B: secondary (ripple) radius
  ## Note: to avoid printing on air, the secondary ripple (B) will be squashed
  ##       vertically if its calculated radius is greater than the primary radius
  Zadj <- (zpos %% 1 - 0.5) * 2;
  Zscale <- sqrt(1 - Zadj^2);
  primaryRadius <- Zscale/4;
  cycleZdir <- sign(((zpos / 2 + 1) %% 1 - 0.5) * 2);
  secondaryRadius <- (1 - Zscale) / 8;
  distPrimary <- (primaryRadius * pi);
  distSecondary <- (secondaryRadius * pi);
  distRemaining <- (distPrimary * 2 + distSecondary * 4);
  waveCount <- (endPos - startPos);
  chunkDist <- distRemaining / ceiling(numPieces / waveCount);
  xPoss <- 0; yPoss <- 0;
  if(distPrimary == 0) {
    ## Primary curve doesn't exist, so make a straight line
    xPoss <- seq(0, 1, length.out = ceiling(numPieces / waveCount));
    yPoss <- rep(0, length(xPoss));
  } else if(distSecondary == 0) {
    ## Ripple is not necessary, so make a full S curve without ripple
    primaryRadius <- 0.25;
    distPrimary <- primaryRadius * pi;
    distRemaining <- distPrimary * 2;
    chunkDist <- distRemaining / ceiling(numPieces / waveCount);
    ripplePoints <- 
      unique(sort(c(distPrimary, seq(0, distRemaining, 
                                     length.out=ceiling(numPieces / waveCount)))));
    xPoss <- c(-cos(ripplePoints[ripplePoints <= distPrimary] / primaryRadius) *
                 primaryRadius + primaryRadius,
               -cos((ripplePoints[ripplePoints > distPrimary] - distPrimary) /
                      primaryRadius) * primaryRadius + primaryRadius * 3);
    yPoss <- c(sin(ripplePoints[ripplePoints <= distPrimary] / primaryRadius) *
                 primaryRadius,
               -sin((ripplePoints[ripplePoints > distPrimary] - distPrimary) /
                      primaryRadius) * primaryRadius);
  } else {
    ## Create the cycle for a single wave
    maxSecondaryOffset <- min(primaryRadius, secondaryRadius);
    criticalDists <- 
      c(cumsum(c(0, distSecondary, distPrimary, distSecondary, distSecondary,
                 distPrimary)), distRemaining);
    criticalRadiuss <- c(secondaryRadius, primaryRadius, secondaryRadius,
                         secondaryRadius, primaryRadius, secondaryRadius);
    lineOffsets <- c(maxSecondaryOffset, primaryRadius, maxSecondaryOffset,
                     maxSecondaryOffset, primaryRadius, maxSecondaryOffset);
    radialCentres <- c(0 + secondaryRadius,
                       0.25, 0.25 + (primaryRadius + secondaryRadius),
                       0.75 - (primaryRadius + secondaryRadius), 0.75,
                       1 - secondaryRadius);
    ripplePoints <- unique(sort(c(criticalDists, cumsum(rep(chunkDist, numPieces-1)))));
    ripplePointRegions <- cut(ripplePoints, criticalDists,
                              labels=FALSE, include.lowest=TRUE);
    for(region in 1:6){
      yFlip <- ((region+1) %% 2 - 0.5) * 2;
      xPoss <- 
        c(xPoss, -cos((ripplePoints[ripplePointRegions == region] - 
                         criticalDists[region]) / (criticalRadiuss[region])) * 
            criticalRadiuss[region] + radialCentres[region]);
      yPoss <- 
        c(yPoss, sin((ripplePoints[ripplePointRegions == region] - 
                        criticalDists[region]) / (criticalRadiuss[region])) * 
            lineOffsets[region] * yFlip);
    }
  }
  ## apply offsets and scaling
  xPoss <- xPoss * (endPos - startPos) + startPos;
  ## trim away positions not within the range
  yPoss <- yPoss[(xPoss >= startPos) & (xPoss <= endPos)];
  xPoss <- xPoss[(xPoss >= startPos) & (xPoss <= endPos)];
  ## write out final positions
  if(dir == 0){
    matrix(c(x = xPoss, y = yPoss * cycleZdir, z=rep(zpos, length(xPoss))), ncol=3);
  } else {
    matrix(c(x = yPoss * cycleZdir, y = xPoss, z=rep(zpos, length(xPoss))), ncol=3);
  }
}

library(rgl);
library(viridis);

plot3d(NA, xlim=c(-1.25,1.25), ylim=c(-1.25,1.25), zlim=c(-2.75,2.75), pch=16,
       box=FALSE, axes=FALSE);
xShift <- sqrt(3) / 2;
for(frame in 1:100){
  zpos <-  ((frame-1) / 40) - 1;
  waveDir <- floor((zpos+0.5) %% 2);
  c2 <- viridis(40, option="H")[(frame - 1 + 20) %% 40 + 1];
  for(blobs in -1:2){
    if(waveDir == 0){
      lines3d(t(t(concentricRipple(-1, 0, zpos, dir=0)) *
                  c(1,1,1) + c(-0.25,0 + blobs * 0.5,0)),
              col=c2, pch=16, lwd=3);
      lines3d(t(t(concentricRipple(0, 1, zpos, dir=0)) *
                  c(1,1,1) + c(-0.25,0 + blobs * 0.5,0)),
              col=c2, pch=16, lwd=3);
    } else {
      lines3d(t(t(concentricRipple(-1, 0, zpos, dir=1)) *
                  c(1,1,1) + c(0 + blobs * 0.5 - 0.5,0.25,0)),
              col=c2, pch=16, lwd=3);
      lines3d(t(t(concentricRipple(0, 1, zpos, dir=1)) *
                  c(1,1,1) + c(0 + blobs * 0.5 - 0.5,0.25,0)),
              col=c2, pch=16, lwd=3);
    }
  }
  rgl.snapshot(sprintf("frame_%03d.png",frame));
}
