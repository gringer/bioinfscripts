#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long qw(:config auto_help pass_through);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);

my $covAdjust = 1;

GetOptions("adjust=i" => \$covAdjust, ) or
  die("Error in command line arguments");

# unknown commands are treated as identifiers
my @fileNames = ();
while(@ARGV){
  my $arg = shift(@ARGV);
  if(-e $arg){
    push(@fileNames, $arg);
  } else {
    die(sprintf("Error: unknown argument: %s\n", $arg));
  }
}

if(scalar(@fileNames) < 2){
    die(sprintf("Error: need at least two files to work with!\n"));
}

my @files = ();
my @fchr = ();
my @fstart = ();
my @fend = ();
my @fdepth = ();

foreach my $fileName (@fileNames){
  open(my $fDesc, "<", $fileName);
  push(@files, $fDesc);
  push(@fchr, "");
  push(@fstart, 0);
  push(@fend, 0);
  push(@fdepth, 0);
}

my $fref = $files[0];
while(<$fref>){
  chomp;
  my ($chr, $start, $end, $depth) = split(/\t/);
  $fchr[0] = $chr;
  $fstart[0] = $start;
  $fend[0] = $end;
  $fdepth[0] = $depth;
  for(my $i = 1; $i <= $#files; $i++){
    my $fnext = $files[$i];
    my ($chr, $start, $end, $depth) = split(/\t/, <$fnext>);
    $fchr[$i] = $chr;
    $fstart[$i] = $start;
    $fend[$i] = $end;
    $fdepth[$i] = $depth;
    printf("%s [%d %d %d] %s [%d %d %d]\n",
           $fchr[0], $fstart[0], $fend[0], $fdepth[0],
           $chr, $start, $end, $depth);
  }
}
close($fref);
