#!/usr/bin/env perl
use warnings;
use strict;

use Pod::Usage; ## uses pod documentation in usage code
use Getopt::Long qw(:config auto_help pass_through);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);
use Time::HiRes qw(time); ## for measuring sub-second time

our $VERSION = "0.5";

=head1 NAME

fastx-kmapper.pl -- kmer-based lightweight mapper for filtered short sequences, amplicons and plasmids

=head1 SYNOPSIS

./fastx-kassembler.pl -ref <refs.fq> <reads.fq> [options]

=head2 Options

=over 2

=item B<-help>

Only display this help message

=item B<-best>

Only show the best mapping result, ignoring 'None' unless there are absolutely no hits

=item B<-ref> I<refSeq>

Load / create kmer mapping table from I<refSeq>

=item B<-forward>

Only map forward-direction kmers (for strand-specific mapping)

=item B<-kmerLength> I<kLen>

Set length of kmers for counting and mapping to I<kLen> bases (default 32)

=item B<-verbose>

Show additional output to help with understanding the counting / mapping process

=back

=cut

my $verboseLevel = 0;
my $count = 10000; ## default number of extension attempted
my $sequencesRead = 0;
my %seqKmerPresent = (); # how many times is a particular kmer present in a sequence
my %kmerSeenCount = (); # number of times a given kmer has been found
my %seqKmerCount = (); # number of kmers found in a sequence
my $totalSeqsSeen = 0;
my $totalKmersSeen = 0;
my $bestOnly = 0;
my $kLen = 17;
my $forwardOnly = 0;
my $refFileName = "";
my @refOrder = ("None");
$seqKmerCount{"None"} = 0;

sub rc {
  my ($seq) = @_;
  $seq =~ tr/ACGT/TGCA/;
  return(scalar(reverse($seq)));
}

sub countKmers {
  my ($seq, $seqID) = @_;
  if(length($seq) < ($kLen-1)){
    printf(STDERR "Warning: length of ${seqID} (%d) is too short for the kmer length (%d); minimum length required is %d. This sequence will be ignored.\n", length($seq), $kLen, $kLen-1);
    return;
  }
  for(my $i = 0; $i < (length($seq) - $kLen); $i++){
    # despite appearances, this seems to be pretty fast
    my $kmer = substr($seq, $i, $kLen);
    $seqKmerPresent{$seqID}{$kmer}++;
    $kmerSeenCount{$kmer}++;
    $seqKmerCount{$seqID}++;
    $totalKmersSeen++;
  }
  if($verboseLevel > 0){
    printf(STDERR " %d kmers seen for %s\n", $seqKmerCount{$seqID}, $seqID);
  }
  $totalSeqsSeen++;
  push(@refOrder, $seqID);
}

sub mapReads {
  my ($read, $readID) = @_;
  my %readKmersFwd = ();
  my %readKmersRev = ();
  my $readKmerCount = 0;
  for(my $i = 0; $i < (length($read) - $kLen); $i++){
    # despite appearances, this seems to be pretty fast
    $readKmersFwd{substr($read, $i, $kLen)}++;
    $readKmerCount++;
  }
  if(!$forwardOnly){
    $read = rc($read);
    for(my $i = 0; $i < (length($read) - $kLen); $i++){
      # despite appearances, this seems to be pretty fast
      $readKmersRev{substr($read, $i, $kLen)}++;
    }
  }  
  ## Kmers can be classified into multiple classes:
  ##   kmers in the reference sequence
  ##   1.  - present in the query sequence
  ##   2.  - not present in the query sequence
  ##   kmers in the query sequence
  ##       - present in the reference sequence [same as #1]
  ##       - not present in the reference sequence
  ##   3.      - present in another reference sequence
  ##   4.      - not present in another reference sequence [contribute to a "not in reference set" likelihood]
  ## The final probability of a match is calculated as...
  ##   (number of kmers matching a reference sequence / total number of kmers in the reference sequence) *
  ##   (number of kmers matching a reference sequence / total number of kmers in the query sequence)
  ## [with a small fudge factor to stop divisions by zero]
  ## TODO: Add direction
  my %foundKmers = ();
  my %foundKmersFwd = ();
  my %foundKmersRev = ();
  my %foundKmerCount = ();
  my %kmerProb = ();
  my %kmerProbFwd = ();
  my %kmerProbRev = ();
  my $fudgeFactor = 0.125;
  my %direction = ();
  foreach my $refName (@refOrder){
    $foundKmersFwd{$refName} = 0;
    $foundKmersRev{$refName} = 0;
    $direction{$refName} = "+";
  }
  foreach my $kmer (keys(%readKmersFwd)) {
    if(!exists($kmerSeenCount{$kmer})){
      $foundKmersFwd{"None"} += $readKmersFwd{$kmer};
      next;
    }
    ## We have a kmer which is somewhere in the set of known kmers for the reference sequence. What is the likelihood that it is from a particular sequence?
    foreach my $seqID (keys(%seqKmerPresent)){
      if(exists($seqKmerPresent{$seqID}{$kmer})){
        # Don't add more kmers than are present in the reference sequence
        my $addCount = (($seqKmerPresent{$seqID}{$kmer} < $readKmersFwd{$kmer}) ? $seqKmerPresent{$seqID}{$kmer} : $readKmersFwd{$kmer});
        # Adjust for kmer uniqueness across all reference sequences
        $foundKmersFwd{$seqID} += $addCount * ($addCount / $kmerSeenCount{$kmer});
        $foundKmerCount{$seqID}++;
      }
    }
  }
  foreach my $kmer (keys(%readKmersRev)) {
    if(!exists($kmerSeenCount{$kmer})){
      $foundKmersRev{"None"} += $readKmersRev{$kmer};
      next;
    }
    ## We have a kmer which is somewhere in the set of known kmers for the reference sequence. What is the likelihood that it is from a particular sequence?
    foreach my $seqID (keys(%seqKmerPresent)){
      if(exists($seqKmerPresent{$seqID}{$kmer})){
        # Don't add more kmers than are present in the reference sequence
        my $addCount = (($seqKmerPresent{$seqID}{$kmer} < $readKmersRev{$kmer}) ? $seqKmerPresent{$seqID}{$kmer} : $readKmersRev{$kmer});
        # Adjust for kmer uniqueness across all reference sequences
        $foundKmersRev{$seqID} += $addCount * ($addCount / $kmerSeenCount{$kmer});
        $foundKmerCount{$seqID}++;
      }
    }
  }
  ## [Now there should be sufficient information to make a judgement on read likelihood]
  ## For 'None', look at what proportion of the read was unclassified
  $seqKmerCount{"None"} = $readKmerCount;
  ## Multiply kmer-based completeness of the reference sequence by the kmer-based completeness of the read
  foreach my $seqID (@refOrder){
    $kmerProbFwd{$seqID} = ($foundKmersFwd{$seqID} + $fudgeFactor) / ($seqKmerCount{$seqID} + $fudgeFactor) *
        ($foundKmersFwd{$seqID} + $fudgeFactor) / ($readKmerCount + $fudgeFactor);
    $kmerProbRev{$seqID} = ($foundKmersRev{$seqID} + $fudgeFactor) / ($seqKmerCount{$seqID} + $fudgeFactor) *
        ($foundKmersRev{$seqID} + $fudgeFactor) / ($readKmerCount + $fudgeFactor);
  }
  if($forwardOnly){
    %foundKmers = %foundKmersFwd;
    %kmerProb = %kmerProbFwd;
  } else {
    foreach my $seqID (@refOrder){
      if($seqID eq "None"){
        # Invert the logic for 'None'; we want the *least* likely direction for no found kmers
        $kmerProb{"None"} = ($kmerProbFwd{$seqID} < $kmerProbRev{$seqID}) ? $kmerProbFwd{$seqID} : $kmerProbRev{$seqID};
        $foundKmers{"None"} = ($foundKmersFwd{$seqID} < $foundKmersRev{$seqID}) ? $foundKmersFwd{$seqID} : $foundKmersRev{$seqID};
      } else {
        $kmerProb{$seqID} = ($kmerProbFwd{$seqID} > $kmerProbRev{$seqID}) ? $kmerProbFwd{$seqID} : $kmerProbRev{$seqID};
        $foundKmers{$seqID} = ($foundKmersFwd{$seqID} > $foundKmersRev{$seqID}) ? $foundKmersFwd{$seqID} : $foundKmersRev{$seqID};
        $direction{$seqID} = ($foundKmersFwd{$seqID} > $foundKmersRev{$seqID}) ? "+" :
            ($foundKmersFwd{$seqID} < $foundKmersRev{$seqID}) ? "-" : ".";
      }
    }
    $direction{"None"} = ".";
  }
  my @sortedProbs = sort { $kmerProb{$b} <=> $kmerProb{$a} } keys(%kmerProb);
  if($verboseLevel == 0){
    # report most and second most likely originating sequence, together with associated probability
    if($bestOnly){
      if($readKmerCount != $foundKmers{"None"}){
        @sortedProbs = grep {$_ ne "None"} @sortedProbs;
      }
      printf("%s,%d,".
             "%s,%s,%d,%d,%0.4f\n", $readID, $readKmerCount,
             $sortedProbs[0], $direction{$sortedProbs[0]}, $foundKmers{$sortedProbs[0]}, $seqKmerCount{$sortedProbs[0]}, $kmerProb{$sortedProbs[0]});
    } else {
      printf("%s,%d,".
             "%s,%s,%d,%d,%0.4f,".
             "%s,%s,%d,%d,%0.4f\n", $readID, $readKmerCount,
             $sortedProbs[0], $direction{$sortedProbs[0]}, $foundKmers{$sortedProbs[0]}, $seqKmerCount{$sortedProbs[0]}, $kmerProb{$sortedProbs[0]},
             $sortedProbs[1], $direction{$sortedProbs[1]}, $foundKmers{$sortedProbs[1]}, $seqKmerCount{$sortedProbs[1]}, $kmerProb{$sortedProbs[1]});
    }
  } else {
    # report probabilites for all sequences
    printf("%s", $readID);
    foreach my $seqID (@refOrder){
      printf(",%d", $kmerProb{$seqID});
    }
    print("\n");
  }
}


GetOptions("forward!" => \$forwardOnly, "kmerlength=i" => \$kLen,
           "best!" => \$bestOnly, "ref=s" => \$refFileName,
           "verbose!" => \$verboseLevel, "count=i" => \$count) or
    pod2usage(1);

if(!$refFileName){
  print(STDERR "Error: no reference file provided\n");
  pod2usage(1);
} else {
  my $startTime = time;
  my $inQual = 0; # false
  my $seqID = "";
  my $qualID = "";
  my $seq = "";
  my $qual = "";
  # work on both gzip-compressed and uncompressed input, regardless of whether
  # or not it is piped
  my $z = new IO::Uncompress::Gunzip($refFileName, MultiStream => 1)
      or die "gunzip failed: $GunzipError\n";
  while(<$z>){
    chomp;
    chomp;
    if (!$inQual) {
      if (/^(>|@)(.*?)( .*$|$)/) {
        my $newSeqID = $2;
        if ($seqID) {
          if($verboseLevel > 0){
            print(STDERR "Counting kmers for $seqID\n");
          }
          countKmers($seq, $seqID);
        }
        $seq = "";
        $qual = "";
        $seqID = $newSeqID;
      } elsif (/^\+(.*)$/) {
        $inQual = 1;            # true
        $qualID = $1;
      } else {
        $seq .= $_;
      }
    } else {
      $qual .= $_;
      if (length($qual) >= length($seq)) {
        $inQual = 0;            # false
        if ($seqID) {
          countKmers($seq, $seqID);
          $seqID = "";
          $seq = "";
          $qual = "";
        }
      }
    }
  }
  if ($seqID) {
    if($verboseLevel > 0){
      print(STDERR "Counting kmers for last sequence, $seqID\n");
    }
    countKmers($seq, $seqID);
  }
  if(scalar(keys(%seqKmerPresent)) == 0){
    print(STDERR "Error: no sequences seen\n");
    pod2usage(1);
  }
  my $timeDiff = time - $startTime;
  printf(STDERR "Processed %d kmers from %d reference sequences in %0.1f seconds\n", $totalKmersSeen, $totalSeqsSeen, $timeDiff);
}

# Complain about non-file command line argument
my @files = ();
while(@ARGV){
  my $arg = shift(@ARGV);
  if(-e $arg){
    push(@files, $arg);
  } else {
    print(STDERR "Unknown argument: $arg\n");
    pod2usage(1);
  }
}
@ARGV = @files;

# use stdin if no files supplied
if(!@ARGV){
  @ARGV = '-' unless (-t STDIN);
}

if($verboseLevel > 0){
  print("query,reference");
  foreach my $seqID (@refOrder){
    printf(",%s", $seqID);
  }
  print("\n");
} else {
  if($bestOnly){
    print("query,queryKmers,".
          "bestRef,bestDir,bestMatchCount,bestRefKmers,bestRefProb\n");
  } else {
    print("query,queryKmers,".
          "bestRef,bestDir,bestMatchCount,bestRefKmers,bestRefProb,".
          "nextRef,nextDir,nextMatchCount,nextRefKmers,nextRefProb\n");
  }
}

my $startTime = time;
my $inQual = 0; # false
my $seqID = "";
my $qualID = "";
my $seq = "";
my $qual = "";
my $totalReadsSeen = 0;
foreach my $file (@ARGV) {
  # This little gunzip dance makes sure the script can handle both
  # gzip-compressed and uncompressed input, regardless of whether
  # or not it is piped
  my $z = new IO::Uncompress::Gunzip($file, MultiStream => 1)
    or die "gunzip failed: $GunzipError\n";
  while(<$z>){
    chomp;
    chomp;
    if (!$inQual) {
      if (/^(>|@)(.*?)( .*$|$)/) {
        my $newSeqID = $2;
        if ($seqID) {
          mapReads($seq, $seqID);
          $totalReadsSeen++;
        }
        $seq = "";
        $qual = "";
        $seqID = $newSeqID;
      } elsif (/^\+(.*)$/) {
        $inQual = 1;            # true
        $qualID = $1;
      } else {
        $seq .= $_;
      }
    } else {
      $qual .= $_;
      if (length($qual) >= length($seq)) {
        $inQual = 0;            # false
        if ($seqID) {
          mapReads($seq, $seqID);
          $totalReadsSeen++;
          $seqID = "";
          $seq = "";
          $qual = "";
        }
      }
    }
  }
}

if ($seqID) {
  mapReads($seq, $seqID);
  $totalReadsSeen++;
}
my $timeDiff = time - $startTime;
printf(STDERR "Mapped %d reads in %0.1f seconds\n", $totalReadsSeen, $timeDiff);
