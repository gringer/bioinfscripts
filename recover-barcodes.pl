#!/usr/bin/env perl

use warnings;
use strict;

use IO::Uncompress::Gunzip qw(gunzip $GunzipError);

## recover-barcodes.pl - attempt to recover partially-failed Rhapsody
## cell barcode assignments.

## input: text file containing cell=[ACGTN]{27}... QCPass=......,
##   representing cell assignments and their reliability

## output: text file modified with any recovered barcodes

## The program progresses in a streamed fashion by storing all the
## detected perfect-QC barcodes into a hash, together with links from
## the component barcodes. When a non-perfect QC barcode is detected,
## the program uses the links from valid barcode sequences to try to
## find an existing perfect-QC match. If a near-enough match is found
## (two substitutions or one INDEL), then the annotation is modified
## to represent the match.

## This is an approach to work around ambiguity issues woth Rhapsody
## cell barcodes; some barcodes pairs don't match the "4 subsitutions
## or 2 INDELs" distance rule, so barcodes cannot be properly assigned
## without additional context.

# unknown commands are treated as identifiers
my @files = ();
while(@ARGV){
  my $arg = shift(@ARGV);
  if(-e $arg){
    push(@files, $arg);
  } else {
    print(STDERR "File not found: $arg\n");
  }
}
@ARGV = @files;

# use stdin if no files supplied
if(!@ARGV){
  @ARGV = '-' unless (-t STDIN);
}

## Distance calculation, returns 1/0 depending on whether the expected
## distance criteria is passed: no more than 2 substitutions, or no more
## than one insertion/deletion
sub seqMatches {
  my ($query, $target) = @_;
  my $distScore = 0;
  my $insScore = 0;
  my $insOffset = 0;
  my $delScore = 0;
  my $delOffset = 0;
  ## Add perfect-matching ends so that distance scores aren't increased
  $query = substr($target,0,1).$query.substr($target,-1,1);
  $target = substr($query,0,1).$target.substr($query,-1,1);
  for(my $i = 1; $i < (length($query)-1); $i++){
    if(substr($query, $i, 1) ne substr($target, $i, 1)){
      $distScore++;
    }
    if(($insOffset <= 1) &&
       (substr($query, $i + $insOffset, 1) ne substr($target, $i, 1))){
      $insScore++;
      $insOffset++;
    }
    if(($delOffset <= 1) &&
       (substr($query, $i - $delOffset, 1) ne substr($target, $i, 1))){
      $delScore++;
      $delOffset++;
    }
  }
  if($insScore <= 1){
    return 1;
  } elsif($delScore <= 1){
    return 1;
  } elsif($distScore <= 2){
    return 1;
  } else {
    return 0;
  }
}

my %barcodeSeqs = ();
my %goodBarcodes = ();

foreach my $file (@ARGV) {
  # This little gunzip dance makes sure the script can handle both
  # gzip-compressed and uncompressed input, regardless of whether
  # or not it is piped
  my $z = new IO::Uncompress::Gunzip($file, MultiStream => 1) or
    pod2usage("gunzip failed: $GunzipError\n");
  while(<$z>){
    chomp; chomp;
    my $line = $_;
    if(/cell=([ACGTN]{9})([ACGTN]{9})([ACGTN]{9}).*QCPass=(...)/){
      my $cellBC = $1.$2.$3;
      my $BC1 = "1-".$1;
      my $BC2 = "2-".$2;
      my $BC3 = "3-".$3;
      my $barcodeQC = $4;
      if($barcodeQC eq "TTT"){ # correct barcode; add links to store
        $barcodeSeqs{$BC1}{$BC2}{$cellBC} = 1;
        $barcodeSeqs{$BC1}{$BC3}{$cellBC} = 1;
        $barcodeSeqs{$BC2}{$BC3}{$cellBC} = 1;
        $goodBarcodes{$cellBC}++;
      } elsif($barcodeQC eq "TTF"){ ## BC3 missing
        if(exists($barcodeSeqs{$BC1}{$BC2})){
          my @bcCandidates = keys(%{$barcodeSeqs{$BC1}{$BC2}});
          my @validCandidates = grep {seqMatches($_, $cellBC)} @bcCandidates;
          if(scalar(@validCandidates) == 1){ # single unique match
            $cellBC = $validCandidates[0];
            $line =~ s/cell=.{27}/"cell=".$cellBC/e;
            $line =~ s/QCPass=.../QCPass=TTT/;
            $line .= " corrected=T";
          }
        }
      } elsif($barcodeQC eq "TFT"){ ## BC2 missing
        if(exists($barcodeSeqs{$BC1}{$BC3})){
          my @bcCandidates = keys(%{$barcodeSeqs{$BC1}{$BC2}});
          my @validCandidates = grep {seqMatches($_, $cellBC)} @bcCandidates;
          if(scalar(@validCandidates) == 1){ # single unique match
            $cellBC = $validCandidates[0];
            $line =~ s/cell=.{27}/"cell=".$cellBC/e;
            $line =~ s/QCPass=.../QCPass=TTT/;
            $line =~ " corrected=T";
          }
        }
       } elsif($barcodeQC eq "FTT"){ ## BC1 missing
        if(exists($barcodeSeqs{$BC2}{$BC3})){
          my @bcCandidates = keys(%{$barcodeSeqs{$BC2}{$BC3}});
          my @validCandidates = grep {seqMatches($_, $cellBC)} @bcCandidates;
          if(scalar(@validCandidates) == 1){ # single unique match
            $cellBC = $validCandidates[0];
            $line =~ s/cell=.{27}/"cell=".$cellBC/e;
            $line =~ s/QCPass=.../QCPass=TTT/;
            $line .= " corrected=T";
          }
        }
      }
    }
    print($line."\n");
  }
}
