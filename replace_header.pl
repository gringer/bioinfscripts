#!/usr/bin/env perl

use Getopt::Long qw(:config auto_help pass_through);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);

our $VERSION = "0.1";

## replace_header.pl -- Replaces sequence names in fasta and gff files
## expects a two-column file as 'lookup', with col1 -> changed into -> col2

$lookupFileName = "";
$fastaFileName = "";
$gffFileName = "";

sub usage {
  printf(STDERR "Usage: ./replace_header.pl -lookup <name> (-fasta <name> | -gff <name>)\n");
}

GetOptions("lookup=s" => \$lookupFileName,
           "fasta=s" => \$fastaFileName,
           "gff=s" => \$gffFileName)
  or exit(1);

my %lookups = ();

if($lookupFileName){
  # read sequence IDs from input file
  if(!$quiet){
    printf(STDERR "Attempting to read from input file '$lookupFileName'\n");
  }
  my $lookupFile = new IO::Uncompress::Gunzip "$lookupFileName" or
    die("Unable to open $lookupFileName\n");
  while(<$lookupFile>){
    chomp;
    s/^[>@]//;
    if(/^(.*?)[\s,](.*)$/){
      $lookups{$1} = $2;
    }
  }
  close($lookupFile);
} else {
  warn("Error: no lookup file specified");
  usage();
  exit(1);
}

if($fastaFileName){
  # read sequence IDs from input file
  if(!$quiet){
    printf(STDERR "Attempting to read from input file '$fastaFileName'\n");
  }
  my $fastaFile = new IO::Uncompress::Gunzip "$fastaFileName" or
    die("Unable to open $fastaFileName\n");
  while(<$fastaFile>){
    if(/^[>]/){
      s/^>(.*?)(\s|$)/">" . $lookups{$1} . " $1" . $2/e;
    }
    print();
  }
  close($fastaFile);
}

if($gffFileName){
  # read sequence IDs from input file
  if(!$quiet){
    printf(STDERR "Attempting to read from input file '$gffFileName'\n");
  }
  my $gffFile = new IO::Uncompress::Gunzip "$gffFileName" or
    die("Unable to open $gffFileName\n");
  while(<$gffFile>){
    if(!/^#/){
      s/^(.*?)\t/$lookups{$1} . "\t"/e;
    }
    print();
  }
  close($gffFile);
}
