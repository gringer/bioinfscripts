#!/usr/bin/env perl
use warnings;
use strict;

use Pod::Usage; ## uses pod documentation in usage code
use Getopt::Long qw(:config auto_help pass_through);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);
use IO::Compress::Gzip; ## for creating demultiplexed files

$| = 1; # automatically flush output [https://perldoc.perl.org/perlvar]

our $VERSION = "0.1";

=head1 NAME

fastx-fetch.pl -- Retrieve sequences from a fastq/fasta file

=head1 SYNOPSIS

./fastx-fetch.pl <reads.fq> [options]

=head2 Options

=over 2

=item B<-help>

Only display this help message

=item B<-idfile> I<idFileName>

Retrieve sequence IDs from I<idFileName>

=item B<-quiet>

Don't report additional information to standard error

=item B<-reverse> or B<-v>

Invert selection logic: exclude specified sequences

=item B<-unique>

Output only the first sequences with the same sequence ID (up to first space)

=item B<-minLength> I<len>

Only output sequences that are at least as long as I<len>

=item B<-maxLength> I<len>

Only output sequences that are at most as long as I<len>

=item B<-count> I<rCount>

Stop after I<rCount> reads

=item B<-bases> I<rBases>

Stop after I<rBases> bases

=item B<-qualityFilter> I<qFilt>

Remove reads with a quality lower than I<qFilt>. If I<rCount> is
specified, choose the longest reads instead of the first reads.

=item B<-lengthFilter> I<lFilt>

Remove reads with a length shorter than I<lFilt>. If I<rCount> is
specified, choose the highest-quality reads instead of the first
reads.

=item B<-nametrim> I<pattern>

Remove text matching I<pattern> from the sequence names

=item B<-trim> I<len>

Remove I<len> bases from I<both> sides of the sequence

=item B<-triml> I<len>

Remove I<len> bases from the left (start) of the sequence

=item B<-trimr> I<len>

Remove I<len> bases from the right (end) of the sequence

=back

=head1 DESCRIPTION

Retrieve sequences from a fastq/fasta file.

=cut

my $idFileName = "";
my $quiet = 0;
my $minLength = 1;
my $maxLength = 10 ** 12; # 1 Tbp
my $basesToFind = 0;
my $countMode = 0;
my $invert = 0; # invert logic
my $trim = 0;
my $triml = 0;
my $trimr = 0;
my $trimString = "";
my $unique = 0;
my $prefix = "reads";
my $chimeric = 0;
my $oneLine = 0;
my $qualityFilter = 0;
my $lengthFilter = 0;
my $readsToFind = 0;
my $totalReads = 0;
my $totalBases = 0;
my $maxCount = 0;
my $sortReads = 0;
my %doRC = ();

sub rc {
  my ($seq) = @_;
  $seq =~ tr/ACGTUYRSWMKDVHBXN-/TGCAARYSWKMHBDVXN-/;
  # work on masked sequences as well
  $seq =~ tr/acgtuyrswmkdvhbxn/tgcaaryswkmhbdvxn/;
  return(scalar(reverse($seq)));
}

sub getMeanQual {
  my ($qual) = @_;
  if(length($qual) == 0){
    return(0);
  }
  my $qBase = 33;
  my @qspl = split("", $qual);
  my $qualTotal = 0;
  my $qualCount = 0;
  foreach my $qual (@qspl) {
    $qualTotal += 10 ** (-(ord($qual) - $qBase) / 10);
    $qualCount++;
  }
  return(int(-10 * log($qualTotal / $qualCount) / log(10)));
}

sub processSeq {
  my ($seqID, $seq, $qual, $containsFQ) = @_;
  if($doRC{$seqID}){
    $seq = rc($seq);
    $qual = scalar(reverse($qual));
  }
  if($trim > 0){
    $seq = substr($seq, $trim, length($seq)-($trim * 2));
    if($qual){
      $qual = substr($qual, $trim, length($qual)-($trim * 2));
    }
  }
  if($trimr > 0){
    $seq = substr($seq, 0, -$trimr);
    if($qual){
      $qual = substr($qual, 0, -$trimr);
    }
  }
  if($triml > 0){
    $seq = substr($seq, $triml);
    if($qual){
      $qual = substr($qual, $triml);
    }
  }
  if($qual){
    return(sprintf("@%s\n%s\n+\n%s\n", $seqID, $seq, $qual));
  } elsif(!$containsFQ) {
    if(!$oneLine){
      $seq =~ s/(.{100})/$1\n/g;
    }
    $seq =~ s/\n$//;
    return(sprintf(">%s\n%s\n", $seqID, $seq));
  }
}

GetOptions("idfile=s" => \$idFileName, "quiet!" => \$quiet,
           "reverse|v!" => \$invert, "trim=i" => \$trim,
           "triml=i" => \$triml, "trimr=i" => \$trimr,
           "qualityFilter=i" => \$qualityFilter, "lengthFilter=i" => \$lengthFilter,
           "unique!" => \$unique, "oneline!" => \$oneLine,
           "minLength=i" => \$minLength, "maxLength=i" => \$maxLength,
           "bases=i" => \$basesToFind, "count|c=i" => \$readsToFind, "nametrim=s" => \$trimString)
  or pod2usage(1);

if(($readsToFind > 0) && ($basesToFind > 0)){
  printf(STDERR "Error: 'count' and 'bases' stop options don't work together");
  pod2usage(1);
}

if(($readsToFind < 0) || ($basesToFind < 0)){
  printf(STDERR "Error: 'count' and 'bases' values can't be negative");
  pod2usage(1);
}

$maxCount = $readsToFind;

if($trim){
  $minLength = $minLength + $trim * 2;
  $maxLength = $maxLength + $trim * 2;
}

if($triml){
  $minLength = $minLength + $triml;
  $maxLength = $maxLength + $triml;
}

if($trimString){
  printf(STDERR "Will remove text matching '(%s)' from the sequence IDs",
         $trimString) if (!$quiet);
  $trimString =~ s/^\|//;
  $trimString = "($trimString)";
}

my %idsToFind = ();
my %outFiles = ();
my %idsSeen = ();
my %readCache = ();
my @cacheOrder = ();
my @cacheStats = ();
my $cacheSorted = 0;
my $minStat = 0;

# unknown commands are treated as identifiers
my @files = ();
while(@ARGV){
  my $arg = shift(@ARGV);
  if(-e $arg){
    push(@files, $arg);
  } else {
    if($arg =~ /:rc$/){
      $arg =~ s/:rc$//;
      $doRC{$arg} = 1;
    }
    $idsToFind{$arg} = 0;
  }
}
@ARGV = @files;

# use stdin if no files supplied
if(!@ARGV){
  @ARGV = '-' unless (-t STDIN);
}

if($invert){
  printf(STDERR "Excluding IDs, rather than selecting\n") if(!$quiet);
  $readsToFind = 0;
}

if($idFileName){
  # read sequence IDs from input file
  if(!$quiet){
    printf(STDERR "Attempting to read from input file '$idFileName'\n");
  }
  my $idFile = new IO::Uncompress::Gunzip "$idFileName" or
    pod2usage("Unable to open $idFileName\n");
  while(<$idFile>){
    chomp;
    s/^[>@]//;
    s/[\s,].*$//;
    $idsToFind{$_} = 0;
  }
  close($idFile);
}

## If reads have been specified, we can put an upper limit on the number of reads to find
if(keys(%idsToFind) && !$invert){
  $readsToFind = scalar(keys(%idsToFind));
}

if(!$quiet && (scalar(keys(%idsToFind)) > 0)){
  printf(STDERR "Read %d identifiers\n", scalar(keys(%idsToFind)));
}

## Add a dummy key so that the hash isn't completely deleted when depleted
if(keys(%idsToFind)){
  $idsToFind{"zombieValue"} = 1;
}

my $inQual = 0; # false
my $seqID = "";
my $qualID = "";
my $seq = "";
my $qual = "";
my $containsFQ = 0; # false
my $duplicateCount = 0;
my $corruptCount = 0;
foreach my $file (@ARGV) {
  # This little gunzip dance makes sure the script can handle both
  # gzip-compressed and uncompressed input, regardless of whether
  # or not it is piped
  my $z = new IO::Uncompress::Gunzip($file, MultiStream => 1) or
    pod2usage("gunzip failed: $GunzipError\n");
 while(<$z>){
    chomp;
    chomp;
    if (!$inQual) {
      if(/\x00/){ ## detect corrupt files, wait for next good read
        $seqID =~ s/\s.*$//;
        print(STDERR "Warning: corrupt sequence found at $seqID [NUL]\n");
        $seqID = "";
        $qualID = "";
        $seq = "";
        $qual = "";
        next;
      } elsif (/^(>|@)((.+?)(\s.*?\s*)?)$/) { ## reached the start of a new sequence
        my $newSeqID = $2;
        my $newShortID = $3;
        my $testSeqID = $newSeqID;
        my $testShortID = $newShortID;
        if($trimString){
          $testShortID =~ s/$trimString//;
          $testSeqID =~ s/$trimString//;
        }
        my $lengthStat = length($seq);
        my $qualityStat = 0;
        if($qualityFilter || $lengthFilter){
          $qualityStat = getMeanQual($qual);
        }
        if(($lengthStat < $minLength) || ($lengthStat > $maxLength) ||
           ($lengthFilter && ($lengthStat < $lengthFilter)) ||
           ($qualityFilter && ($qualityStat < $qualityFilter))){
          $seqID = ""; $seq = ""; $qual = "";
        }
        ## read has been chosen to include based on its sequence ID,
        ## and not filtered out due to other statistics
        if ($seqID) {
          if(($qualityFilter || $lengthFilter) && ($readsToFind || $basesToFind)){
            ## accept all sequences if the count threshold hasn't been reached
            if( ($readsToFind && ($totalReads < $readsToFind)) ||
                ($basesToFind && ($totalBases < $basesToFind)) ){
              $readCache{$seqID} = {"seq" => processSeq($seqID, $seq, $qual, $containsFQ),
                                        "stat" => $lengthFilter ? $qualityStat : $lengthStat,
                                        "len" => $lengthStat };
              push(@cacheOrder, $seqID);
              push(@cacheStats, $lengthFilter ? $qualityStat : $lengthStat);
              $totalBases += $lengthStat;
              $totalReads++;
            } elsif ($seq && ($qualityFilter || $lengthFilter)) { ## sort by the other statistic, and discard the worst
              if(!$cacheSorted){
                printf(STDERR "Reached read %s limit [%d]; initial sorting of read statistics array of length %d... ",
                       ($readsToFind ? "count" : "length"), ($readsToFind ? $totalReads : $totalBases), scalar(@cacheOrder));
                ## do a slow, but efficient initial sort
                @cacheOrder = sort { $readCache{$a}{"stat"} <=> $readCache{$b}{"stat"} } @cacheOrder;
                @cacheStats = map { $readCache{$_}{"stat"} } @cacheOrder;
                $cacheSorted = 1;
                print(STDERR "done\n");
                #printf(STDERR join(";", @cacheStats) . "\n");
              }
              ## Note: statistics are reversed from the filter because
              ## the filter has already been applied, and the other
              ## statistic is used for sorting
              my $checkStat = $lengthFilter ? $qualityStat : $lengthStat;
              ## if the statistic is too low, no further processing is needed
              if($checkStat > $minStat){
                #printf(STDERR "reached count limit, but found a value to insert... ");
                ## binary search to find appropriate insertion position
                my $minPos = 0;
                my $maxPos = $#cacheStats + 1;
                my ($midPos, $bStat);
                while ($minPos < $maxPos) {
                  $midPos = ($maxPos + $minPos) >> 1;
                  $bStat = $cacheStats[$midPos];
                  #printf(STDERR "checking at %d (%s vs %s)... ", $midPos, $bStat, $checkStat);
                  if($checkStat < $bStat){
                    $maxPos = $midPos;
                  } else {
                    $minPos = $midPos+1;
                  }
                }
                #printf(STDERR "trying to insert at %d... ", $minPos);
                ## add new value before minPos and update total bases
                $readCache{$seqID} = {"seq" => processSeq($seqID, $seq, $qual, $containsFQ),
                                          "stat" => $lengthFilter ? $lengthStat : $qualityStat,
                                          "len" => $lengthStat };
                splice(@cacheOrder, $minPos, 0, $seqID);
                splice(@cacheStats, $minPos, 0, $checkStat);
                $totalBases += $lengthStat;
                $totalReads++;
                ## remove smallest value from array and update total bases
                while(($readsToFind && (($totalReads - 1) > $readsToFind)) || ($basesToFind && (($totalBases - $readCache{$cacheOrder[0]}{"len"}) > $basesToFind))){
                  my $readToRemove = shift(@cacheOrder);
                  $totalReads--;
                  $totalBases -= $readCache{$readToRemove}{"len"};
                  #printf(STDERR "removing read with length %d; new totalBases = $totalBases\n", $readCache{$readToRemove}{"len"});
                  delete($readCache{$readToRemove});
                  shift(@cacheStats);
                  #printf(STDERR "done\n");
                  $minStat = $cacheStats[0];
                  #printf(STDERR join(";", @cacheStats) . " [min = %d]\n", $minStat);
                }
              }
            }
          } else { ## No additional filters / sorting to apply
            print(processSeq($seqID, $seq, $qual, $containsFQ));
            $totalReads++;
            $totalBases += length($seq);
          }
          if (!$invert && $readsToFind && ($totalReads >= $readsToFind)) {
            print(STDERR "Should stop now... we have seen $totalReads reads\n") if (!$quiet);
            if($z ne "-"){
              close($z);
            }
            $seqID = "";
            last;
          }
        }
        $seq = "";
        $qual = "";
        if ((!(keys(%idsToFind)) ||
             exists($idsToFind{$testSeqID}) ||
             exists($idsToFind{$testShortID})) xor $invert) {
          if((exists($idsToFind{$testSeqID}) ||
              exists($idsToFind{$testShortID})) && ($totalReads < $readsToFind)){
            #print(STDERR "Found another ID to get, with $totalReads total reads; $readsToFind reads to find\n");
          }
          delete $idsToFind{$testSeqID};
          delete $idsToFind{$testShortID};
          if(exists($idsSeen{$newShortID})){
            $duplicateCount++;
            if($unique){
              $seqID = "";
            } else {
              if($duplicateCount < 20){
                print(STDERR "Warning: Sequence ID '${newShortID}' has ".
                      "already been seen. Use the '-u' option to exclude.\n");
              } elsif($duplicateCount == 20){
                print(STDERR "[Other duplicate sequences were seen]\n");
              }
              $seqID = $newSeqID;
            }
          } else {
            $seqID = $newSeqID;
          }
          $idsSeen{$newShortID} = 1;
        } else {
          $seqID = "";
        }
      } elsif (/^\+(.*)$/) {
        $inQual = 1;     # true
        $containsFQ = 1; # true
        $qualID = $1;
      } else {
        if(/@/){
          $seqID =~ s/\s.*$//;
          if($corruptCount < 20){
            print(STDERR "Warning: corrupt sequence found at $seqID ".
                  "[header in sequence string]\n");
          } elsif($corruptCount == 20){
            print(STDERR "[Other corrupt sequences were seen]\n");
          }
          $corruptCount++;
          $seqID = "";
          $qualID = "";
          $seq = "";
          $qual = "";
          next;
        } else {
          $seq .= $_;
        }
      }
    } else {
      if(/\x00/){ ## detect corrupt files, wait for next good read
        $seqID =~ s/\s.*$//;
        if($corruptCount < 20){
          print(STDERR "Warning: corrupt sequence found at $seqID [NUL]\n");
        } elsif($corruptCount == 20){
          print(STDERR "[Other corrupt sequences were seen]\n");
        }
        $corruptCount++;
        $seqID = "";
        $qualID = "";
        $seq = "";
        $qual = "";
        $inQual = 0; # false
      } else {
        $qual .= $_;
        if (length($qual) > (length($seq) + 2)) {
          $seqID =~ s/\s.*$//;
          if($corruptCount < 20){
            print(STDERR "Warning: corrupt sequence found at $seqID ".
                  "[quality string too long]\n");
          } elsif($corruptCount == 20){
            print(STDERR "[Other corrupt sequences were seen]\n");
          }
          $seqID = "";
          $qualID = "";
          $seq = "";
          $qual = "";
          $inQual = 0; # false
        } elsif (length($qual) >= length($seq)) {
          $inQual = 0; #false
        }
      }
    }
  } ## end while (<$z>)
  if($z ne "-"){
    close($z);
  }
}

## Filter out the last read (if necessary)
my $lengthStat = length($seq);
my $qualityStat = getMeanQual($qual);
if((length($seq) < $minLength) || (length($seq) > $maxLength) ||
   ($lengthFilter && ($lengthStat < $lengthFilter)) ||
   ($qualityFilter && ($qualityStat < $qualityFilter))){
  $seqID = ""; $seq = ""; $qual = "";
}

## slot in last read if cache is already full
if($seqID && ($qualityFilter || $lengthFilter)){
  my $checkStat = $qualityFilter ? getMeanQual($qual) : length($seq);
  if($checkStat <= $minStat){
    $seqID = ""; $seq = ""; $qual = "";
  } elsif(($readsToFind && ($totalReads >= $readsToFind)) ||
          ($basesToFind && ($totalBases >= $basesToFind))){
    delete($readCache{shift(@cacheOrder)});
    shift(@cacheStats);
  }
}

## Show cached reads (if any)
foreach my $cacheSeqID (reverse(@cacheOrder)) {
  print($readCache{$cacheSeqID}{"seq"});
}

## Show last read (if not otherwise filtered)
if ($seqID) {
  if(($qualityFilter || $lengthFilter) && ($readsToFind || $basesToFind)){
    ## accept all sequences if the count threshold hasn't been reached
    if( ($readsToFind && ($totalReads < $readsToFind)) ||
        ($basesToFind && ($totalBases < $basesToFind)) ){
      print(processSeq($seqID, $seq, $qual, $containsFQ));
      $totalBases += length($seq);
      $totalReads++;
    }
  } else {
      print(processSeq($seqID, $seq, $qual, $containsFQ));
      $totalBases += length($seq);
      $totalReads++;
  }
}

if(!$quiet){
  if(!$unique && ($duplicateCount > 1)){
    print(STDERR "${duplicateCount} duplicate IDs were seen and not excluded. ".
          "Use the '-u' option to exclude duplicate sequences.\n");
  } elsif($unique) {
    print(STDERR "Number of duplicates excluded: $duplicateCount\n");
  }
}

print(STDERR "Done! Processed ${totalReads} reads and ${totalBases} bases.\n") if (!$quiet);
