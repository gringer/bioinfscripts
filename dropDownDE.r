#!/usr/bin/Rscript

library(tidyverse);
library(ggrepel);

data.tbl <- read_csv("DE_shrunk_VST_GRCm38_CG_4T1only_2020-Jan-29.csv.gz");

chr.tbl <- read_csv("mm38.csv") %>%
    mutate(ChrStart = cumsum(c(0, head(ChrLength, -1))),
           ChrEnd = cumsum(ChrLength),
           ChrMid = (ChrStart + ChrEnd) / 2);

data.tbl %>%
    filter(dir == "fwd") %>%
    mutate(midPos = (Start + End) / 2) %>%
    inner_join(chr.tbl) %>%
    mutate(genomeMid = ChrStart + midPos) %>%
    mutate(Chr = factor(Chr,
                        levels = c("1", "2", "3", "4", "5", "6", "7",
                                   "8", "9", "10", "11", "12", "13",
                                   "14", "15", "16", "17", "18", "19",
                                   "X", "Y")),
           L2FC = -`L2FC.Line-WT-p0`,
           adjL2FC = L2FC +
               ifelse(sign(L2FC==0), 1,
                      L2FC + sign(L2FC)*1)) %>%
    arrange(-abs(L2FC)) %>%
    ## Only keep the most extreme FC result for each gene
    distinct(Gene, .keep_all=TRUE) -> merged.tbl;

colLookup = c("#FF8472B0", "#A05248B0", "#808080B0", "#6CA0FFB0", "#4364A0B0");

## load font
library(showtext);
font_add("Optima", "~/.fonts/optima/OPTIMA.TTF");
font_add("Biolinum",
         "/usr/share/fonts/opentype/LinLibertine/LinBiolinum_Rah.ttf");
showtext_auto();

##cairo_pdf("manhattanDE_Jan29Data_2020-May_30.pdf", width=8, height=18);
svg("manhattanDE_Jan29Data_2020-May_30.svg", width=8, height=18);
par(bty="n", family = "Biolinum", xpd=NA, cex=1.5, mar=c(5, 0.5, 2, 0.5),
    bg=NA);
plot(genomeMid ~ L2FC, xlim=c(-10, 10),
     ylim=c(max(chr.tbl$ChrEnd), 0),
     data=merged.tbl, pch=19,
     col=colLookup[((as.integer(merged.tbl$Chr) + 1) %% 2 + 1) *
                   sign(merged.tbl$L2FC) + 3],
     axes=FALSE, ann=FALSE, cex=0.71)
rect(xleft=-0.5,xright=0.5,
     ytop=-0.1 * max(chr.tbl$ChrEnd),
     ybottom=1.1 * max(chr.tbl$ChrEnd),
     col="#FFFFFFB0", border=NA);
text(x=0, y=chr.tbl$ChrMid, chr.tbl$Chr, cex=1.25);
segments(x0=-0.4, x1=0.4, y0=c(0, chr.tbl$ChrEnd), lwd=2);
axis(1);
mtext(expression(log[2]~Fold~Change), side=1, line=3, cex=2);
text(x=c(-7.5,7.5), cex=1.5,
     y=-0.035 * max(chr.tbl$ChrEnd),
     labels = c(
         "More Expressed\nIn 4T1-WT",
         "More Expressed\nIn 4T1-ρ0"));
arrows(x0=c(-0.5, 0.5), x1=c(-2.5, 2.5), y0=-0.035 * max(chr.tbl$ChrEnd),
       lwd=2);
## Add in labels
merged.tbl %>%
    filter(L2FC <= -4) -> left.tbl;
merged.tbl %>%
    filter(L2FC >= 4) -> right.tbl;
text(x=left.tbl$L2FC, y=left.tbl$genomeMid, labels=left.tbl$Gene,
     pos = 2, offset=0.3);
text(x=right.tbl$L2FC, y=right.tbl$genomeMid, labels=right.tbl$Gene,
     pos = 4, offset=0.3);
invisible(dev.off());


##cairo_pdf("manhattanDE_Jan29Data_2020-May_30.pdf", width=8, height=18);
svg("horiz_manhattanDE_Jan29Data_2021-Jun_16.svg", width=18, height=6);
par(bty="n", family = "Biolinum", xpd=NA, cex=1.5, mar=c(0.5, 5, 2, 0.5),
    bg=NA, clip=NA);
plot(L2FC ~ genomeMid, ylim=c(-10, 10),
     xlim=c(0,max(chr.tbl$ChrEnd)),
     data=merged.tbl, pch=19,
     col=colLookup[((as.integer(merged.tbl$Chr) + 1) %% 2 + 1) *
                   sign(merged.tbl$L2FC) + 3],
     axes=FALSE, ann=FALSE, cex=0.71)
rect(ytop=-0.5,ybottom=0.5,
     xleft=-0.1 * max(chr.tbl$ChrEnd),
     xright=1.1 * max(chr.tbl$ChrEnd),
     col="#FFFFFFB0", border=NA);
text(y=0, x=chr.tbl$ChrMid, chr.tbl$Chr, cex=1.25);
segments(y0=-0.4, y1=0.4, x0=c(0, chr.tbl$ChrEnd), lwd=2);
axis(2);
mtext(expression(log[2]~Fold~Change), side=2, line=3, cex=2);
text(y=c(-7.5,7.5), cex=1.5, pos=2,
     x=max(chr.tbl$ChrEnd),
     labels = c(
         "More Expressed\nIn 4T1-WT",
         "More Expressed\nIn 4T1-ρ0"));
arrows(y0=c(-0.5, 0.5), y1=c(-2.5, 2.5), x0=max(chr.tbl$ChrEnd),
       lwd=2);
## Add in labels
merged.tbl %>%
    filter(L2FC <= -4) -> left.tbl;
merged.tbl %>%
    filter(L2FC >= 4) -> right.tbl;
text(y=left.tbl %>% distinct(Gene, .keep_all=TRUE) %>% pull(L2FC),
     x=left.tbl %>% distinct(Gene, .keep_all=TRUE) %>% pull(genomeMid),
     labels=left.tbl %>% distinct(Gene, .keep_all=TRUE) %>% pull(Gene),
     pos = 1, offset=0.3);
text(y=right.tbl %>% distinct(Gene, .keep_all=TRUE) %>% pull(L2FC),
     x=right.tbl %>% distinct(Gene, .keep_all=TRUE) %>% pull(genomeMid),
     labels=right.tbl %>% distinct(Gene, .keep_all=TRUE) %>% pull(Gene),
     pos = 3, offset=0.3);
invisible(dev.off());
