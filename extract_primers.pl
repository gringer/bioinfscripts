#!/usr/bin/env perl

## This code is preserved for interest only; see `primeCoverage.py` for a better approach

use warnings;
use strict;

use Pod::Usage; ## uses pod documentation in usage code
use Getopt::Long qw(:config auto_version auto_help);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);

our $VERSION = "0.4";

=head1 NAME

extract_primers.pl -- generates / fetches primer sequences from an
input fasta file

=head1 SYNOPSIS

./extract_primers.pl [options]

=head2 Options

=over 2

=item B<-minlength>

Minimum length of primer to generate, excluding prefix/suffix (default 22)

=item B<-maxlength>

Maximum length of primer to generate, excluding prefix/suffix (default 30)

=item B<-mintemp>

Minimum temperature of primer to generate, excluding prefix/suffix (default 60)

=item B<-temp>

Target melt temperature (default 61)

=item B<-maxtemp>

Maximum length of primer to generate, excluding prefix/suffix (default 62)

=item B<-minamplicon>

Minimum amplicon length (default 300)

=item B<-count>

Number of primers to generate (default 1)

=item B<-fwdPrefix>

Prefix sequence for forward primers (default I<TTTCTGTTGGTGCTGATATTGC>)

=item B<-revPrefix>

Prefix sequence for reverse primers (default I<ACTTGCCTGTCGCTCTATCTTC>)

=item B<-showtm> I<type>

Show melting temperature of (p)refix, p(r)imer region, or (e)ntire sequence,
or a certain distance from the start of the sequence (+bp) or the end
of the sequence (-bp).

=back

=head1 DESCRIPTION

Note: when more than one primer is generated, each new primer is
checked to make sure there are no duplicates.

=cut

my $minLength = 22;
my $maxLength = 30;
my $minAmplicon = 300;
my $minTemp = 60;
my $targetTemp = 61;
my $maxTemp = 62;
my $count = 1;
my $fwdPrefix = "TTTCTGTTGGTGCTGATATTGC";
my $revPrefix = "ACTTGCCTGTCGCTCTATCTTC";
my $showTm = "r"; # primer region only

GetOptions('minlength=i' => \$minLength,
           'maxlength=i' => \$maxLength,
           'minamplicon=i' => \$minAmplicon,
           'mintemp=i' => \$minTemp,
           'temp=i' => \$targetTemp,
           'maxtemp=i' => \$maxTemp,
           'count=i'=> \$count,
           'fwdprefix=s' => \$fwdPrefix,
           'revprefix=s' => \$revPrefix,
           'showtm=s' => \$showTm,
          ) or pod2usage(1);

# Derived from code from
# http://www.simgene.com/Oligo_Calc/OligoCalcObj.js
# Note: assumes no ambiguous bases
sub getTm {
  my ($seq) = @_;
  if(length($seq) == 0){
    return undef;
  }
  $seq =~ tr/ //d;
  $seq = uc($seq);
  my $GCMin = ($seq =~ tr/CGS//);
  if(length($seq) < 14){
    return((2 * (length($seq)-$GCMin) + 4 * ($GCMin)));
  } else {
    return((64.9 + 41 * (($GCMin - 16.4) / length($seq))));
  }
}

sub getSATm {
  my ($seq, $saltConc) = @_;
  if(length($seq) == 0){
    return undef;
  }
  $seq =~ tr/ //d;
  $seq = uc($seq);
  my $GCMin = ($seq =~ tr/CGS//);
  my $fGCMin = ($GCMin / length($seq)) * 100;
  if (length($seq) < 14) {
    return((2 * (length($seq)-$GCMin) + 4 * ($GCMin)+
	    21.6+(7.21*log($saltConc/1000))));
  }
  else {
    ## from http://www.simgene.com/Oligo_Calc/OligoCalcObj.js
    #return((100.5 + (0.41*$fGCMin) - (820 / length($seq))+
    # (7.21*log($saltConc/1000))));
    ## from primer3 source code [https://github.com/primer3-org/primer3/blob/master/src/oligotm.h#L57]
    81.5 + 16.6 * (log($saltConc / 1000) / log(10)) + (0.41*$fGCMin) -
	600/length($seq);
  }
}

sub processSeq {
  my ($seqID, $seq, $qual, $aPr, $pNum) = @_;
  my %aP = %{$aPr};
  $seqID =~ s/ .*$//;
  my $lastPos = 0;
  for my $i ($minAmplicon..length($seq)-$minLength){
    if((($i - $lastPos) < $minAmplicon)){
      next;
    }
    my $subSeq = substr($seq, $i, $minLength);
    my $last4 = substr($subSeq, -4);
    my $Tm = getTm($subSeq);
    my $SATm = getSATm($subSeq, 65);
    if(1 || (($SATm + 0.5) >= $minTemp) && (($SATm - 0.5) <= $maxTemp)){
      $lastPos = $i;
      printf(">Primer_%s_%ibp_%03d [%s]", $seqID, $i, ++$pNum, $last4);
      if($showTm =~ /r/){
	printf(" [Primer Tm %0.2f °C; ".
	       "%0.2f °C in 50mM Na+]",
	       $Tm, $SATm);
      }
      printf("\n%s\n", $subSeq);
    }
  }
  return($pNum);
}

my %addedPrimers = ();
my $primerNum = 0;

my $inQual = 0; # false
my $seqID = "";
my $qualID = "";
my $seq = "";
my $qual = "";
my $barcodes = "";
my $containsFQ = 0; # false
my $duplicateCount = 0;
foreach my $file (@ARGV) {
  # This little gunzip dance makes sure the script can handle both
  # gzip-compressed and uncompressed input, regardless of whether
  # or not it is piped
  my $z = new IO::Uncompress::Gunzip($file, MultiStream => 1) or
    pod2usage("gunzip failed: $GunzipError\n");
  while(<$z>){
    chomp;
    chomp;
    if (!$inQual) {
      if(/\x00/){ ## detect corrupt files, wait for next good read
        $seqID =~ s/ .*$//;
        print(STDERR "Warning: corrupt sequence found at $seqID [NUL]\n");
        $seqID = "";
        $qualID = "";
        $seq = "";
        $qual = "";
        next;
      } elsif (/^(>|@)((.+?)( .*?\s*)?)$/) {
        my $newSeqID = $2;
        my $newShortID = $3;
        my $testSeqID = $newSeqID;
        my $testShortID = $newShortID;
	if($seqID){
	  $primerNum =
	      processSeq($seqID, $seq, $qual, \%addedPrimers, $primerNum);
	}
        $seq = "";
        $qual = "";
	$seqID = $newSeqID;
      } elsif (/^\+(.*)$/) {
        $inQual = 1;     # true
        $containsFQ = 1; # true
        $qualID = $1;
      } else {
        if(/@/){
          $seqID =~ s/ .*$//;
          print(STDERR "Warning: corrupt sequence found at $seqID ".
                "[header in sequence string]\n");
          $seqID = "";
          $qualID = "";
          $seq = "";
          $qual = "";
          next;
        } else {
          $seq .= $_;
        }
      }
    } else {
      if(/\x00/){ ## detect corrupt files, wait for next good read
        $seqID =~ s/ .*$//;
        print(STDERR "Warning: corrupt sequence found at $seqID [NUL]\n");
        $seqID = "";
        $qualID = "";
        $seq = "";
        $qual = "";
        $inQual = 0; # false
      } else {
        $qual .= $_;
        if (length($qual) > (length($seq) + 2)) {
          $seqID =~ s/ .*$//;
          print(STDERR "Warning: corrupt sequence found at $seqID ".
                "[quality string too long]\n");
          $seqID = "";
          $qualID = "";
          $seq = "";
          $qual = "";
          $inQual = 0; # false
        } elsif (length($qual) >= length($seq)) {
            $inQual = 0;            # false
        }
      }
    }
  }
}

if ($seqID) {
  processSeq($seqID, $seq, $qual, \%addedPrimers, $primerNum);
}

=head1 AUTHOR

David Eccles (gringer) 2014-2020 <bioinformatics@gringene.org>

=head1 LICENSE

Copyright (c) 2014-2020 David Eccles (gringer) <bioinformatics@gringene.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=head1 AVAILABILITY

The most recent version of this code can be found at

https://gitlab.com/gringer/bioinfscripts/blob/master/extract_primers.pl


