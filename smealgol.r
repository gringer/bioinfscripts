#!/usr/bin/env Rscript
## SMEAlgoL - Single Molecule Error Algorithm using LAST

## Calculate mapped read accuracy from LAST results using Heng Li's
## gap-compressed identity formula
## [https://lh3.github.io/2018/11/25/on-the-definition-of-sequence-identity\#gap-compressed-identity]

argLoc <- 1;

usage <- function(){
  cat("usage: cat <last_results> | ./smealgol.r",
      "-ref <string> [options]\n");
  cat("\nOther Options:\n");
  cat("-help              : Only display this help message\n");
  cat("\n");
}

refName <- "";
MAFFileName <- "stdin";

while(!is.na(commandArgs(TRUE)[argLoc])){
  if(file.exists(commandArgs(TRUE)[argLoc])){ # file existence check
      MAFFileName <- commandArgs(TRUE)[argLoc];
  } else {
    if(commandArgs(TRUE)[argLoc] == "-help"){
      usage();
      quit(save = "no", status=0);
    }
    else if(commandArgs(TRUE)[argLoc] == "-ref"){
      refName <- commandArgs(TRUE)[argLoc+1];
      argLoc <- argLoc + 1;
    }
    else {
      cat("Error: Argument '",commandArgs(TRUE)[argLoc],
          "' is not understood by this program\n\n", sep="");
      usage();
      quit(save = "no", status=0);
    }
  }
  argLoc <- argLoc + 1;
}

input <- file(MAFFileName, 'r');

library(tidyverse);

readBuffer <- NULL;
firstOutput <- TRUE;

row <- readLines(input, n=1);
while(length(row) > 0){
    if(grepl("^[a] ", row)){
        ## start of a new alignment
        readBuffer <- NULL;
    }
    if(grepl("^[s] ", row)){
        readBuffer <- c(readBuffer, row);
        if(length(readBuffer) == 2){
            readBuffer %>%
                gsub(" +", "\t", .) %>%
                read_delim(delim="\t", col_names=c("type", "ID", "start", "matchLen",
                                                   "dir", "seqLen", "alignedSeq")) -> long.aln.tbl;
            X <- unlist(strsplit(long.aln.tbl$alignedSeq[1], ""));
            Y <- unlist(strsplit(long.aln.tbl$alignedSeq[2], ""));
            paste(ifelse(X == Y, "=",
                  ifelse(X == '-' | Y == '-', "I",
                         "X")), collapse="") %>%
                gsub("I+","I",.) %>% # do INDEL gap compression
                strsplit("") %>%
                unlist() %>%
                factor(levels = c("=","X","I")) %>%
                table -> mcc;
            gci <- round(mcc["="] / sum(mcc), 4);
            if(firstOutput){
                cat(paste0("ID.",1:2), paste0("start.",1:2),
                    paste0("matchLen.",1:2),
                    paste0("seqLen.",1:2), "gapCompIdent", sep=",");
                cat("\n");
                firstOutput <- FALSE;
            }
            cat(long.aln.tbl$ID, long.aln.tbl$start, long.aln.tbl$matchLen,
                long.aln.tbl$seqLen, gci, sep=",");
            cat("\n");
            ## clear read buffer
            readBuffer <- NULL;
        }
    }
    row <- readLines(input, n=1);
}
