#!/usr/bin/env perl
use warnings;
use strict;

use Pod::Usage; ## uses pod documentation in usage code
use Getopt::Long qw(:config auto_help pass_through);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);

$| = 1; # automatically flush output [https://perldoc.perl.org/perlvar]

my $pos = -1;
my $seqName = "";
my $quiet = 1;
my $UMIfileName = "";
my $tagID = "RX";
my %foundUMIs = ();

our $VERSION = "0.1";

=head1 NAME

samAddUMIs.pl -- add read UMI assignments to the RX field of a SAM file

=head1 SYNOPSIS

samtools view -h reads.bam | ./samAddUMIs.pl -umi <UMI table> [options]

The UMI table should be a space-separated file containing read IDs in
the first column and UMIs in the second column. Where multiple UMIs
are present for the same read ID, the last one will be used.

=head2 Options

=over 2

=item B<-umi> I<file>

Set the UMI lookup file name to I<file>

=item B<-tag> I<tagID>

Change the tag used to specify UMIs in the SAM file to I<tagID>

=item B<-help>

Only display this help message

=back

=head1 DESCRIPTION

Extracts random sequences from those mapped at each base location,
attempting to maintain a minimum coverage threshold where possible.

=cut

GetOptions("umi=s" => \$UMIfileName, "tag=s" => \$tagID)
    or pod2usage(1);

if($UMIfileName){
  # read sequence IDs from input file
  if(!$quiet){
    printf(STDERR "Attempting to read from input file '$UMIfileName'\n");
  }
  my $UMIfile = new IO::Uncompress::Gunzip "$UMIfileName" or
    pod2usage("Unable to open $UMIfileName\n");
  while(<$UMIfile>){
    chomp;
    my @F = split(/[\s,]+/);
    $foundUMIs{$F[0]} = $F[1];
  }
  close($UMIfile);
} else {
    pod2usage("No UMI file specified; cannot continue\n");
}

while(<>){
  if(/^@/){
    print;
    next;
  }
  my $line = $_;
  chomp;
  my @F = split(/\t/);
  my $seqID = $F[0];
  if($foundUMIs{$seqID}){
    $_ .= "\t" . $tagID . ":Z:" . $foundUMIs{$seqID};
  }
  print "$_\n";
}
