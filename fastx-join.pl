#!/usr/bin/env perl
use warnings;
use strict;

## fastx-join.pl -- Join sequences containing near-matching ends
## into a single merged sequence

use Getopt::Long qw(:config auto_help pass_through);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);

sub rc {
  my ($seq) = @_;
  $seq =~ tr/ACGTUYRSWMKDVHBXN-/TGCAARYSWKMHBDVX*-/;
  # work on masked sequences as well
  $seq =~ tr/acgtuyrswmkdvhbx*/tgcaaryswkmhbdvx*/;
  return(scalar(reverse($seq)));
}

sub rev {
  my ($seq) = @_;
  return(scalar(reverse($seq)));
}

sub printStats {
  my ($seq, $seqID, $qual, $qualID, $kmerLength, $rcPropThresh, $rcBaseThresh) = @_;
  my $len = length($seq);
  my %rcCentres = ();
  my $sseq = "";
  if($seqID && (length($seq) > $kmerLength)){
    my %rptCount = ();
    my $maxKmer = "";
    my %rptPos = ();
    my %rcSeen = ();
    my %rcDups = ();
    my $rcCount = 0;
    for(my $p = 0; ($p + $kmerLength) <= $len; $p++){
      $sseq = substr($seq, $p, $kmerLength);
      if($sseq =~ /N/){
        next;
      }
      my $rcs  = rc($sseq);
      if(exists($rptPos{$rcs})){
        my $oldP = $rptPos{$rcs};
	my $mid = ($p + ($oldP + $kmerLength - 1)) >> 1; # divide by 2, remove remainder
        if(!exists($rcCentres{$oldP})){
          $rcCentres{$p} = $mid;
          $rcCentres{$oldP} = $mid;
          $rcCount += 2;
        } else {
          $rcDups{$p} = 1;
          $rcDups{$oldP} = 1;
          $rcCount += 1;
        }
      }
      $rptPos{$sseq} = $p;
    }
    my @midPoints = ();
    foreach my $pos (keys(%rcCentres)){
      if(!$rcDups{$pos}){
        push(@midPoints, $rcCentres{$pos});
      }
    }
    my $medMidPoint = -1;
    if(@midPoints){
      my @sortedMids = sort {$a <=> $b} @midPoints;
      $medMidPoint = $sortedMids[($#midPoints >> 1)];
    }
    my $rcRatio = $rcCount/($len - $kmerLength + 1);
    my $numDups = scalar(keys(%rcDups));
    printf(STDERR "%8d %0.3f %8d %8d %8d %s\n",
	   $len, $rcRatio, $rcCount,
	   $numDups, $medMidPoint,
	   $seqID);
    if(($rcRatio > $rcPropThresh) && ($rcCount > $rcBaseThresh) && ($medMidPoint > -1)){
      my $seqIDbase = "";
      my $seqIDrest = "";
      if($seqID =~ /(^[^ ]+)(.*)$/){
        $seqIDbase = $1;
        $seqIDrest = $2;
      }
      ## When outputting split reads, the forward sequence is
      ## reverse-complemented, so that both sequences start from the
      ## split point (for nanopore reads, the split point is slightly
      ## more likely to have good bases compared to the sequence ends)
      if($qual){
        printf("@%s_split1%s\n%s\n+\n%s\n", $seqIDbase, $seqIDrest, rc(substr($seq, 0, $medMidPoint)), scalar(reverse(substr($qual, 0, $medMidPoint))));
        printf("@%s_split2%s\n%s\n+\n%s\n", $seqIDbase, $seqIDrest, substr($seq, $medMidPoint),    substr($qual, $medMidPoint));
      } else {
        my $seq1 = rc(substr($seq, 0, $medMidPoint));
        my $seq2 = substr($seq, $medMidPoint);
        $seq1 =~ s/(.{100})/$1\n/g;
        $seq1 =~ s/\n$//;
        $seq2 =~ s/(.{100})/$1\n/g;
        $seq2 =~ s/\n$//;
        printf(">%s_split1%s\n%s\n", $seqIDbase, $seqIDrest, $seq);
        printf(">%s_split2%s\n%s\n", $seqIDbase, $seqIDrest, $seq);
      }
    } else {
      if($qual){
        printf("@%s\n%s\n+\n%s\n", $seqID, $seq, $qual);
      } else {
        $seq = substr($seq, $medMidPoint);
        $seq =~ s/(.{100})/$1\n/g;
        $seq =~ s/\n$//;
        printf(">%s\n%s\n", $seqID, $seq);
      }
    }
  }
}

$| = 1; ## don't buffer output

my $kmerLength = 17; ## number of bases in hash keys
my $maxDelta = -1;
my $maxEndGap = -1;
my $matchLength = 200000;
my $rcBasesThreshold = 50;

GetOptions("matchlength|m=i" => \$matchLength,
           "endgap|e=i" => \$maxEndGap,
           "maxdelta|d=i" => \$maxDelta,
           "kmerlength|k=i" => \$kmerLength) or
    die("Error in command line arguments");

# Complain about non-file command line argument
my @files = ();
while(@ARGV){
  my $arg = shift(@ARGV);
  if(-e $arg){
    push(@files, $arg);
  } else {
    die("Unknown argument: $arg");
  }
}
@ARGV = @files;

# use stdin if no files supplied
if(!@ARGV){
  @ARGV = '-' unless (-t STDIN);
}

if($kmerLength != 17){
  print(STDERR "Setting kmer length to $kmerLength\n");
}

if($maxDelta == -1){
  $maxDelta = $kmerLength;
}

if($maxEndGap == -1){
  $maxEndGap = $maxDelta;
}

## Load sequences into memory
my %seqs = ();
my %quals = ();
my $inQual = 0; # false
my $seqID = "";
my $qualID = "";
my $seq = "";
my $qual = "";
my $buffer = "";
foreach my $file (@ARGV) {
  # This little gunzip dance makes sure the script can handle both
  # gzip-compressed and uncompressed input, regardless of whether
  # or not it is piped
  my $z = new IO::Uncompress::Gunzip($file, MultiStream => 1)
    or die "gunzip failed: $GunzipError\n";
  while(<$z>){
    chomp;
    chomp;
    if (!$inQual) {
      if (/^(>|@)((.+?)( .*?\s*)?)$/) {
        my $newSeqID = $2;
        my $newShortID = $3;
        if($seqID){
#          printf("Adding %s: %s..%s\n",
#                 $seqID, substr($seq, 0, 20), substr($seq, -20));
          $seqs{$seqID} = $seq;
          $quals{$qualID} = $qual if $qual;
        }
        $seq = "";
        $qual = "";
        $buffer = "";
        $seqID = $newSeqID;
      } elsif (/^\+(.*)$/) {
        $inQual = 1;            # true
        $qualID = $1;
      } else {
        $seq .= $_;
      }
    } else {
      $qual .= $_;
      if (length($qual) >= length($seq)) {
        $inQual = 0;            # false
      }
    }
  }
}
if($seqID){
#  printf("Adding %s: %s..%s\n",
#         $seqID, substr($seq, 0, 20), substr($seq, -20));
  $seqs{$seqID} = $seq;
  $quals{$qualID} = $qual if $qual;
}

## store kmer and RC locations for the first and last chunk of each sequence
my %kmerLocs = ();
my %kmerCounts = ();
foreach my $lSeqID (keys(%seqs)){
  printf(STDERR "Indexing %s\n", $lSeqID);
  my $sseq = "";
  my $src = "";
  my $seq = $seqs{$lSeqID};
  my $len = length($seq);
  my $maxLen = ($len < $matchLength) ? $len : $matchLength;
  for(my $p = 0; ($p + $kmerLength) <= $maxLen; $p++){
    $sseq = substr($seq, $p, $kmerLength);
    # if($p < 10){
    #   printf("Position %d of %s: adding %s to kmer database\n",
    #       $p, $lSeqID, $sseq);
    # }
    $src = rc($sseq);
    if($sseq =~ /N/){
      next;
    }
    $kmerLocs{$sseq}{$lSeqID}{"FWD"}{$p} = 1;
    $kmerLocs{$src}{$lSeqID}{"RC"}{$p} = 1;
    $kmerCounts{$sseq}++;
    $kmerCounts{$src}++;
  }
  for(my $p = 0; ($p + $kmerLength) <= $maxLen; $p++){
    my $adjP = length($seq) - $p - $kmerLength;
    $sseq = substr($seq, $adjP, $kmerLength);
    # if($p < 10){
    #   printf("Position %d of %s: adding %s to kmer database\n",
    #          $adjP, $lSeqID, $sseq);
    # }
    $src = rc($sseq);
    if($sseq =~ /N/){
      next;
    }
    $kmerLocs{$sseq}{$lSeqID}{"FWD"}{$adjP} = 1;
    $kmerLocs{$src}{$lSeqID}{"RC"}{$adjP} = 1;
    $kmerCounts{$sseq}++;
    $kmerCounts{$src}++;
  }
}

my %hitStats = ();
foreach my $seqID1 (keys(%seqs)){
  foreach my $seqID2 (keys(%seqs)){
    if($seqID1 ge $seqID2){
      next;
    }
    $hitStats{$seqID1}{$seqID2}{"dminF"} = $matchLength * 2;
    $hitStats{$seqID1}{$seqID2}{"dmaxF"} = -1;
    $hitStats{$seqID1}{$seqID2}{"dtotF"} = ();
    $hitStats{$seqID1}{$seqID2}{"dminR"} = $matchLength * 2;
    $hitStats{$seqID1}{$seqID2}{"dmaxR"} = -1;
    $hitStats{$seqID1}{$seqID2}{"dtotR"} = ();
    $hitStats{$seqID1}{$seqID2}{"FWD"}{"hits"} = 0;
    $hitStats{$seqID1}{$seqID2}{"RC"}{"hits"} = 0;
  }
}

## Instead of checking kmers in sequence order, just check them as
## they come, and determine the closest distance
## Things to record (per pair):
##  * number of hits
##  * smallest distance
##  * earliest match
##  * latest match
##  * mean delta
while (my($sseq, $count) = each %kmerCounts){
  if($count == 1){ # only seen once; no need to check pairs
    next;
  }
  foreach my $seqID1 (keys(%seqs)){
    foreach my $seqID2 (keys(%seqs)){
      if($seqID1 ge $seqID2){
        next;
      }
      ## Check forward distances
      if (exists($kmerLocs{$sseq}{$seqID1}{"FWD"}) &&
          exists($kmerLocs{$sseq}{$seqID2}{"FWD"})) {
        my $dmin = $matchLength * 2;
        foreach my $fp1 (keys(%{$kmerLocs{$sseq}{$seqID1}{"FWD"}})){
          foreach my $fp2 (keys(%{$kmerLocs{$sseq}{$seqID2}{"FWD"}})){
            # printf("Looking at %s; position %d in %s; position %d in %s\n",
            #        $sseq, $fp1, $seqID1, $fp2, $seqID2);
            if($dmin > abs($fp1 - $fp2)){
              $dmin = abs($fp1 - $fp2);
            }
          }
        }
        if($dmin < $matchLength){
          $hitStats{$seqID1}{$seqID2}{"FWD"}{"hits"}++;
          if($dmin < $hitStats{$seqID1}{$seqID2}{"dminF"}){
            $hitStats{$seqID1}{$seqID2}{"dminF"} = $dmin;
          }
          if($dmin > $hitStats{$seqID1}{$seqID2}{"dmaxF"}){
            $hitStats{$seqID1}{$seqID2}{"dmaxF"} = $dmin;
          }
          push(@{$hitStats{$seqID1}{$seqID2}{"dtotF"}}, $dmin);
        }
      }
      ## Check reverse distances
      if (exists($kmerLocs{$sseq}{$seqID1}{"FWD"}) &&
          exists($kmerLocs{$sseq}{$seqID2}{"RC"})) {
        my $dmin = $matchLength * 2;
        foreach my $fp1 (keys(%{$kmerLocs{$sseq}{$seqID1}{"FWD"}})){
          foreach my $fp2 (keys(%{$kmerLocs{$sseq}{$seqID2}{"RC"}})){
            if($dmin > abs($fp1 - $fp2)){
              $dmin = abs($fp1 - $fp2);
            }
          }
        }
        if($dmin < $matchLength){
          $hitStats{$seqID1}{$seqID2}{"RC"}{"hits"}++;
          if($dmin < $hitStats{$seqID1}{$seqID2}{"dminR"}){
            $hitStats{$seqID1}{$seqID2}{"dminR"} = $dmin;
          }
          if($dmin > $hitStats{$seqID1}{$seqID2}{"dmaxR"}){
            $hitStats{$seqID1}{$seqID2}{"dmaxR"} = $dmin;
          }
          push(@{$hitStats{$seqID1}{$seqID2}{"dtotR"}}, $dmin);
        }
      }
    }
  }
}

foreach my $seqID1 (keys(%seqs)){
  foreach my $seqID2 (keys(%seqs)){
    if($seqID1 ge $seqID2){
      next;
    }
    my @fMins = @{$hitStats{$seqID1}{$seqID2}{"dtotF"}}; @fMins = sort {$a <=> $b} (@fMins);
    my @rMins = @{$hitStats{$seqID1}{$seqID2}{"dtotR"}}; @rMins = sort {$a <=> $b} (@rMins);
    my $fMed = @fMins[$#fMins/2];
    my $rMed = @rMins[$#rMins/2];
    printf("%s vs %s:\n", $seqID1, $seqID2);
    printf("FWD: %6d; Rev: %6d; dminF: %6d; dmaxF: %6d; dmedF: %0.1f, dminR: %6d; dmaxR: %6d; dmedR: %0.1f".
           "\n",
           $hitStats{$seqID1}{$seqID2}{"FWD"}{"hits"},
           $hitStats{$seqID1}{$seqID2}{"RC"}{"hits"},
           $hitStats{$seqID1}{$seqID2}{"dminF"},
           $hitStats{$seqID1}{$seqID2}{"dmaxF"},
           $fMed,
           $hitStats{$seqID1}{$seqID2}{"dminR"},
           $hitStats{$seqID1}{$seqID2}{"dmaxR"},
           $rMed
        );
  }
}
