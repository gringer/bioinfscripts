#!/usr/bin/env perl
use warnings;
use strict;

use Pod::Usage; ## uses pod documentation in usage code
use Getopt::Long qw(:config auto_help pass_through);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);

our $VERSION = "0.1";

=head1 NAME

fastx-gcfilter.pl -- Filter sequences based on GC content

=head1 SYNOPSIS

./fastx-gcfilter.pl <reads.fq> -min <fraction> -max <fraction> [options]

=head2 Options

=over 2

=item B<-help>

Only display this help message

=item B<-reverse> or B<-v>

Invert selection logic: exclude specified sequences

=item B<-minimum> I<fraction>

Set the minimum GC fraction (range 0..1)

=item B<-maximum> I<fraction>

Set the maximum GC fraction (range 0..1)

=item B<-report>

Only report the GC fraction; don't filter

=item B<-quiet>

Don't report additional information to standard error

=back

=head1 DESCRIPTION

Filter sequences from a fastq/fasta file based on GC content.

=cut

my $quiet = 0;
my $minGC = -1;
my $maxGC = -1;
my $invert = 0;
my $reportOnly = 0;

GetOptions("reverse|v!" => \$invert,
           "report|r!" => \$reportOnly,
           "minimum=f" => \$minGC,
           "maximum=f" => \$maxGC,
           "quiet!" => \$quiet)
  or pod2usage(1);

if($invert){
  print(STDERR "inverting range filter (i.e. excluding GC range)\n");
}

if((!$reportOnly) && (($minGC == -1) || ($maxGC == -1))){
  pod2usage("Error: both 'minimum' and 'maximum' GC ".
            "fractions need to be set\n");
}

# Complain about non-file command line argument
my @files = ();
while(@ARGV){
  my $arg = shift(@ARGV);
  if(-e $arg){
    push(@files, $arg);
  } else {
    die("Unknown argument: $arg");
  }
}
@ARGV = @files;

# use stdin if no files supplied
if(!@ARGV){
  @ARGV = '-' unless (-t STDIN);
}

sub processSeq {
  my ($seqID, $seq, $qual, $containsFQ, $min, $max, $invert) = @_;
  my $gc = ($seq =~ tr/GCgc//);
  my $at = ($seq =~ tr/ATat//);
  my $gcProp = $gc / ($gc + $at);
  if($reportOnly){
    printf("%0.4f %s\n", $gcProp, $seqID);
  } else {
    if((($gcProp > $min) && ($gcProp < $max)) xor $invert){
      if($qual){
        printf("@%s\n%s\n+\n%s\n", $seqID, $seq, $qual);
      } elsif(!$containsFQ) {
        $seq =~ s/(.{100})/$1\n/g;
        $seq =~ s/\n$//;
        printf(">%s\n%s\n", $seqID, $seq);
      }
    }
  }
}

my %fastXStrs = ();

my $inQual = 0; # false
my $seqID = "";
my $qualID = "";
my $seq = "";
my $qual = "";
my $containsFQ = 0; # false
my $seqsRead = 0;
foreach my $file (@ARGV) {
  # This little gunzip dance makes sure the script can handle both
  # gzip-compressed and uncompressed input, regardless of whether
  # or not it is piped
  my $z = new IO::Uncompress::Gunzip($file, MultiStream => 1)
    or die "gunzip failed: $GunzipError\n";
  while(<$z>){
    chomp;
    chomp;
    if (!$inQual) {
      if (/^(>|@)(.*)$/) {
        my $newSeqID = $2;
        if ($seqID) {
          processSeq($seqID, $seq, $qual, $containsFQ, $minGC, $maxGC, $invert);
          $seqsRead++;
        }
        $seq = "";
        $qual = "";
        $seqID = $newSeqID;
      } elsif (/^\+(.*)$/) {
        $inQual = 1;     # true
        $containsFQ = 1; # true
        $qualID = $1;
      } else {
        $seq .= $_;
      }
    } else {
      $qual .= $_;
      if (length($qual) >= length($seq)) {
        $inQual = 0;            # false
      }
    }
  }
}

if ($seqID) {
  processSeq($seqID, $seq, $qual, $containsFQ, $minGC, $maxGC, $invert);
  $seqsRead++;
}

print(STDERR "Done, processed $seqsRead reads\n");
