#!/usr/bin/env perl
use warnings;
use strict;

use Getopt::Long qw(:config auto_help pass_through);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);

sub rc {
  my ($seq) = @_;
  $seq =~ tr/ACGTUYRSWMKDVHBXN-/TGCAARYSWKMHBDVXN-/;
  # work on masked sequences as well
  $seq =~ tr/acgtuyrswmkdvhbxn/tgcaaryswkmhbdvxn/;
  return(scalar(reverse($seq)));
}

sub comp {
  my ($seq) = @_;
  $seq =~ tr/ACGTUYRSWMKDVHBXN-/TGCAARYSWKMHBDVXN-/;
  # work on masked sequences as well
  $seq =~ tr/acgtuyrswmkdvhbxn/tgcaaryswkmhbdvxn/;
  return(scalar($seq));
}

sub rev {
  my ($seq) = @_;
  return(scalar(reverse($seq)));
}

sub med {
  ## median
  my @data = @_;
  if(scalar(@data) <= 1){
    return 0;
  }
  my @dl = sort {$a <=> $b} (@data);
  my $dll = scalar(@dl);
  my $med = ($dll % 2 == 0) ?
      (($dl[$dll / 2 - 1] + $dl[$dll / 2]) / 2) :
      ($dl[($dll-1) / 2]);
  return $med;
}

sub mad {
  ## mean absolute deviation [from the median]
  my @data = @_;
  if(scalar(@data) <= 1){
    return 0;
  }
  my @dl = sort {$a <=> $b} (@data);
  my $dll = scalar(@dl);
  my $med = ($dll % 2 == 0) ?
      (($dl[$dll / 2 - 1] + $dl[$dll / 2]) / 2) :
      ($dl[($dll-1) / 2]);
  my $totDev = 0;
  foreach my $val (@dl){
    $totDev += abs($val - $med);
  }
  return ($totDev / $dll);
}

sub printStats {
  my ($seq, $seqID, $trim, $kmerLength) = @_;
  my $len = length($seq);
  my $sseq = "";
  if($seqID && (length($seq) > $trim) && (length($seq) > $kmerLength)){
    my $countTotal = 0;
    my $countMax = 0;
    my $maxKmer = "";
    my %rptPos = ();
    my %dupCount = ();
    my %dupGapLengths = ();
    my %dupGapLengthTotal = ();
    for(my $p = 0; ($p + $kmerLength) <= $len; $p++){
      $sseq = substr($seq, $p, $kmerLength);
      if(exists($rptPos{$sseq})){
	my $gap = $p - $rptPos{$sseq};
	if(!exists($dupCount{$sseq})){
	  $dupCount{$sseq} = 1;
	  $dupGapLengths{$sseq} = ();
	}
	$dupCount{$sseq}++;
	push(@{$dupGapLengths{$sseq}}, $gap);
	$dupGapLengthTotal{$sseq} += $gap;
      }
      $rptPos{$sseq} = $p;
    }
    my $numKmers = scalar(keys(%dupCount));
    printf("## %s: (%d repeated kmers of length %d)\n", $seqID, $numKmers,
	   $kmerLength);
    foreach my $kmer (keys(%dupCount)){
      printf("%s,%d,%.2f,%.2f,%.2f\n",
	     $kmer, $dupCount{$kmer},
	     $dupGapLengthTotal{$kmer} / ($dupCount{$kmer}-1),
	     med(@{$dupGapLengths{$kmer}}),
	     mad(@{$dupGapLengths{$kmer}}));
    }
  }
}

$| = 1; ## don't buffer output

my $trim = 0;
my $kmerLength = 17; ## number of bases in hash keys

GetOptions("trim=s" => \$trim, "kmerlength|k=i" => \$kmerLength) or
    die("Error in command line arguments");

# Complain about non-file command line argument
my @files = ();
while(@ARGV){
  my $arg = shift(@ARGV);
  if(-e $arg){
    push(@files, $arg);
  } else {
    die("Unknown argument: $arg");
  }
}
@ARGV = @files;

# use stdin if no files supplied
if(!@ARGV){
  @ARGV = '-' unless (-t STDIN);
}

if($kmerLength != 17){
  print(STDERR "Setting kmer length to $kmerLength\n");
}

my $inQual = 0; # false
my $seqID = "";
my $qualID = "";
my $seq = "";
my $qual = "";
my $buffer = "";
foreach my $file (@ARGV) {
  # This little gunzip dance makes sure the script can handle both
  # gzip-compressed and uncompressed input, regardless of whether
  # or not it is piped
  my $z = new IO::Uncompress::Gunzip($file, MultiStream => 1)
    or die "gunzip failed: $GunzipError\n";
  while(<$z>){
    chomp;
    chomp;
    if (!$inQual) {
      if (/^(>|@)((.+?)( .*?\s*)?)$/) {
        my $newSeqID = $2;
        my $newShortID = $3;
        printStats($seq, $seqID, $trim, $kmerLength);
        $seq = "";
        $qual = "";
        $buffer = "";
        $seqID = $newSeqID;
      } elsif (/^\+(.*)$/) {
        $inQual = 1;            # true
        $qualID = $1;
      } else {
        $seq .= $_;
      }
    } else {
      $qual .= $_;
      if (length($qual) >= length($seq)) {
        $inQual = 0;            # false
      }
    }
  }
}

printStats($seq, $seqID, $trim, $kmerLength);
