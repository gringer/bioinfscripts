#!/usr/bin/env perl
use warnings;
use strict;

## fastq2fasta.pl -- convert from fastq to fasta

## This script reads in files line by line, looks for the '@' that
## determines the start of the sequence and stores the header, then
## captures the sequence and associated quality string. Once a full
## sequence and quality string is read, the sequence part of it is
## written out, replacing the '@' in the header line with a '@', and
## making sure that sequence lines are at most 60 characters in length.

use Getopt::Long qw(:config auto_help pass_through);

my $minLength = 0;
my $singleLine = 0;

GetOptions("minLength=i" => \$minLength, "singleLine!" => \$singleLine) or
  die("Error in command line arguments");

my $inQual = 0; # false
my $seqID = "";
my $qualID = "";
my $seq = "";
my $qual = "";
while(<>){
  chomp;
  chomp;
  if(/^\s+$/){
    next;
  }
  if(!$inQual){
    if(/^@(.+)$/){
      $seqID = $1;
      $seq = "";
    } elsif(/^\+(.*)$/) {
      $inQual = 1; # true
      $qualID = $1;
      $qual = "";
      if(length($seq) > $minLength){
        my $printedSeq = $seq;
        if(!$singleLine){
          $printedSeq =~ s/(.{60})/$1\n/g;
          $printedSeq =~ s/\n$//g;
        }
        printf(">%s\n%s\n", $seqID, $printedSeq);
      }
    } else {
      $seq .= $_;
    }
  } else {
    $qual .= $_;
    if(length($qual) >= length($seq)){
      $inQual = 0; # false
    }
  }
}
