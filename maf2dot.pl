#!/usr/bin/perl

use warnings;
use strict;

my $qSeq = "";
my $qStart = 0;
my $qLen = 0;
my $qName = "";
my $tSeq = "";
my $tStart = 0;
my $tLen = 0;
my $tName = "";

while(<>){
  if(!/^[as]/){
    next;
  }
  my @F = split(/\s+/);
  if($F[0] eq "a"){
    $qSeq = "";
    $tSeq = "";
  } elsif($F[0] eq "s"){
    if($tSeq){
      $qName = $F[1];
      $qStart = $F[2];
      $qLen = $F[5];
      $qSeq = $F[6];
      ## blank out any insertions into the target
      for(my $i=0; $i < length($tSeq); $i++){
        if(substr($tSeq, $i, 1) eq "-"){
          substr($tSeq, $i, 1) = " ";
          substr($qSeq, $i, 1) = " ";
       }
      }
      ## remove blanked out regions from both sequences
      $qSeq =~ s/ //g;
      $tSeq =~ s/ //g;
      $qSeq = (" " x $tStart) . $qSeq;
      $tSeq = (" " x $tStart) . $tSeq;
      ## generate dot consensus
      my $cons = "";
      for(my $i=0; $i < length($tSeq); $i++){
        my $st = substr($tSeq, $i, 1);
        my $sq = substr($qSeq, $i, 1);
        if(($st ne $sq) && ($sq ne "-")){
          $cons .= $sq;
        } elsif ($st eq " "){
          $cons .= " ";
        } else {
          $cons .= ".";
        }
      }
      printf("%-55s %-4d %s\n", $qName, $qStart-$tStart, $qSeq);
      printf("%-55s %-4d %s\n", $qName, $qStart-$tStart, $cons);
   } else {
      $tName = $F[1];
      $tStart = $F[2];
      $tLen = $F[5];
      $tSeq = $F[6];
    }
  }
}
