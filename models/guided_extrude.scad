// Path extrusion script, various functions for properly orienting faces

// Path extrusion, where a shape is extruded along a 3D path, can be
// challenging to properly calculate, because it's difficult to work out
// the proper rotations of the shape, especially along the axis of
// extrusion between two successive points. This path extrusion
// library provides many different functions to help lock that
// rotation to the intended angle, depending on the intended
// application. Examples are provided at the end of the library to
// demonstrate different classes of rotation locking.

// OpenSCAD_User_Manual/List_Comprehensions#Flattening_a_nested_vector
function flatten(l) = [ for (a = l) for (b = a) b ];

// OpenSCAD_User_Manual/List_Comprehensions#Concatenating_two_vectors
function cat(L1, L2) = [for(L=[L1, L2], a=L) a];

// https://math.stackexchange.com/a/1076999/756273
function planarNormals(p1, p2, p3) =
  let(lx = (p2 - p1) / norm(p2 - p1),
      n2 = (p3 - p1),
      lz = cross(lx, n2) / norm(cross(lx, n2)),
      ly = cross(lx, lz) / norm(cross(lx, lz)))
  [lx, ly, lz];

// https://math.stackexchange.com/a/2755842/756273
// https://github.com/sergarrido/random/tree/master/circle3d
function circleCentre(p1, p2, p3) =
  let(v1 = (p2 - p1),
      v2 = (p3 - p1),
      b = 0.5 / ((v1*v1)*(v2*v2)-(v1*v2)^2),
      k1 = b * (v2*v2)*((v1*v1) - (v1*v2)),
      k2 = b * (v1*v1)*((v2*v2) - (v1*v2)))
      p1+k1*v1+k2*v2;

// https://stackoverflow.com/a/38407105/3389895
function getPerpendVectors(p1, p2) =
  let(v1 = (p2 - p1) / norm(p2-p1))
  (max(v1) == 1) ? (
    v1[0] == 1 ? [[1,0,0], [0,1,0], [0,0,1]] :
    v1[1] == 1 ? [[0,1,0], [1,0,0], [0,0,1]] :
    [[0,0,1], [0,1,0], [1,0,0]]) :
    let(v2 = (abs(v1[2]) < abs(v1[0])) ? [v1[1], -v1[0], 0] / norm([v1[1], -v1[0], 0]) :
      [0, -v1[2], v1[1]] / norm([0, -v1[2], v1[1]]))
    let(v3 = cross(v1, v2) / norm(cross(v1, v2)))
    [v1, v2, v3];

// https://math.stackexchange.com/a/100766/756273
function projectPoint(point, planeNormal, planePoint) =
  let(x = point[0], y = point[1], z = point[2],
      a = planeNormal[0], b = planeNormal[1], c = planeNormal[2],
      d = planePoint[0], e= planePoint[1], f = planePoint[2],
      t = (a*d + b*e + c*f - a*x - b*y - c*z) / (a*a + b*b + c*c))
      [x + t*a, y + t*b, z + t*c];

// magic function to that creates a vector plane that approximately bisects the
// two outer points, and is oriented to match as closely as possible the vector
// plane of the first point.
function getMidDirectionVectors(a, b, c, vecAx, vecAy, calcAxy = true) =
  let(pVecs1 = calcAxy ? getPerpendVectors(a, b) : [vecAx, vecAy],
      pVecs2 = getPerpendVectors(a, c),
      projectedYPoint = projectPoint(a + pVecs1[2]*10, a - b, b),
      vecTest2cNonDir = cross(pVecs2[0], b - projectedXPoint) / 
        norm(cross(pVecs2[0], b - projectedXPoint)),
      vecTest2bNonDir = cross(pVecs2[0], vecTest2cNonDir) /
        norm(cross(pVecs2[0], vecTest2cNonDir)),
      vecTest2b = (norm((b + vecTest2bNonDir) - projectedXPoint) < 
                       norm((b - vecTest2bNonDir) - projectedXPoint)) ?
                       vecTest2bNonDir : -vecTest2bNonDir,
      vecTest2c = (norm((b + vecTest2cNonDir) - projectedYPoint) < 
                      norm((b - vecTest2cNonDir) - projectedYPoint)) ?
                      vecTest2cNonDir : -vecTest2cNonDir,
      pVecs2a = planarNormals(b, c, projectedXPoint))
      [vecTest2b, vecTest2c];

function mapfuncConstantX(shape, pathPoint) =
   [ for(i = [0 : (len(shape) - 1)])
       [0, shape[i][0], shape[i][1]] + pathPoint ];

function mapfuncConstantY(shape, pathPoint) =
   [ for(i = [0 : (len(shape) - 1)])
       [shape[i][1], 0, shape[i][0]] + pathPoint ];

function mapfuncConstantZ(shape, pathPoint) =
   [ for(i = [0 : (len(shape) - 1)])
       [shape[i][0], shape[i][1], 0] + pathPoint ];

function mapfuncVertCylinder(shape, pathPoint) =
   let(pAng = atan2(pathPoint[1], pathPoint[0]),
       pRad = sqrt(pathPoint[0]*pathPoint[0] + pathPoint[1]*pathPoint[1]))
   [ for(i = [0 : (len(shape) - 1)])
       [(shape[i][0]) * cos(pAng),
        (shape[i][0]) * sin(pAng),
        shape[i][1]] + pathPoint ];

function mapfuncLocalCylinder(shape, lastPathPoint, pathPoint, nextPathPoint) =
   let(pAng = atan2(pathPoint[1], pathPoint[0]),
       pRad = sqrt(pathPoint[0]*pathPoint[0] + pathPoint[1]*pathPoint[1]),
       pCen = circleCentre(lastPathPoint, pathPoint, nextPathPoint),
       pNorms = planarNormals(pCen, pathPoint, lastPathPoint))
   // map shape to local X/Z axis (i.e. a circle facing the last point and touching the centre)
   [ for(i = [0 : (len(shape) - 1)])
     (shape[i][0] * pNorms[0] + shape[i][1] * pNorms[2] + pathPoint) ];

function mapfuncRadialCylinder(shape, pathPoint) =
   let(pAng = atan2(pathPoint[1], pathPoint[0]),
       pRad = sqrt(pathPoint[0]*pathPoint[0] + pathPoint[1]*pathPoint[1]),
       pCirc = 2 * PI * pRad)
   [ for(i = [0 : (len(shape) - 1)])
       [pRad * cos(pAng + (shape[i][0] / pCirc) * 360),
        pRad * sin(pAng + (shape[i][0] / pCirc) * 360),
        shape[i][1] + pathPoint[2]]  ];

function mapfuncProjectedPattern(shape, lastPathPoint, pathPoint, nextPathPoint, state) =
  let(midVec = (nextPathPoint - lastPathPoint) / norm(nextPathPoint - lastPathPoint))
   // map shape to local X/Z axis (i.e. a circle facing the last point and touching the centre)
   [ for(i = [0 : (len(shape) - 1)])
     (shape[i][0] * pNorms[0] + shape[i][1] * pNorms[2] + pathPoint) ];

function mapFunc(extrudeType, shape, lastPathPoint, pathPoint, nextPathPoint) =
   (extrudeType == "constantX") ?
     mapfuncConstantX(shape, pathPoint) :
   (extrudeType == "constantY") ?
     mapfuncConstantY(shape, pathPoint) :
   (extrudeType == "constantZ") ?
     mapfuncConstantZ(shape, pathPoint) :
   (extrudeType == "vertCylinder") ?
     mapfuncVertCylinder(shape, pathPoint) :
   (extrudeType == "localCylinder") ?
     mapfuncLocalCylinder(shape, lastPathPoint, pathPoint, nextPathPoint) :
   (extrudeType == "radialCylinder") ?
     mapfuncRadialCylinder(shape, pathPoint) :
     [0,0,0];

function needsState(extrudeType) =
  (extrudeType == "projectedPattern") ? true :
  false;

function mapFuncState(extrudeType, shape, lastPathPoint, pathPoint, nextPathPoint, state) =
  // Note: for compatibility and debugging purposes, all the existing extrusions
  //        are given a null state
  (!needsState(extrudeType)) ?
    [mapFunc(extrudeType, shape, lastPathPoint, pathPoint, nextPathPoint), []] :
    (extrudeType == "projectedPattern") ?
    mapfuncProjectedPattern(shape, lastPathPoint, pathPoint, nextPathPoint, state) :
    [[0,0,0],[]];

function extrudePoints(extrudeType, shape, path, i = 0, result = [], state = []) =
   let(shapeCount = len(shape), pathCount = len(path))
   (i >= pathCount) ? result :
		 let(lastPath = (i == 0) ? path[pathCount-2] : path[i-1],
				 curPath = path[i],
         nextPath = (i == pathCount - 1) ? path[1] : path[(i+1)],
		     posPointState = mapFuncState(extrudeType, shape, lastPath, curPath, nextPath, state))
     extrudePoints(extrudeType, shape, path,
                        i=i+1, result = cat(result, posPointState[0]), state=cat(state, posPointState[1]));

module mapExtrude(extrudeType, shape, path) {
  shapeCount = len(shape); pathCount = len(path);
  extrudedPoints = (needsState(extrudeType)) ? // avoid slower extrusion if not needed
    extrudePoints(extrudeType, shape, path) :
    flatten([ for(i = [0 : (pathCount - 1)])
      let(lastPath = (i == 0) ? path[pathCount-2] : path[i-1],
          curPath = path[i],
          nextPath = (i == pathCount - 1) ? path[1] : path[(i+1)])
        mapFunc(extrudeType, shape, lastPath, curPath, nextPath) ]);
   pointsLen = len(extrudedPoints);
   faceBottom = [ for(i = [0 : (shapeCount - 1)]) i ];
   faceTop = [ for(i = [0 : (shapeCount - 1)])
       (pointsLen - i - 1) ];
   faceLinksA = [ for(l = [0:(pathCount - 2)], i = [0:(shapeCount - 1)])
           [i + (l * shapeCount), i + ((l+1) * shapeCount),
            (i+1) % shapeCount + (l * shapeCount)] ];
   faceLinksB = [ for(l = [0:(pathCount - 2)], i = [0:(shapeCount - 1)])
           [(i+1) % shapeCount + (l * shapeCount),
            i + ((l + 1) * shapeCount),
            (i+1) % shapeCount + ((l + 1) * shapeCount)] ];
   polyhedron( points = extrudedPoints, convexity=3,
       faces  = concat([faceBottom], faceLinksA, faceLinksB, [faceTop]));
}

// weave pattern -- extrude example with constant X
module exampleWeave(){
    function movePathXZ(x, w, h, y) = [x, y, h * cos(x/w * 360)];
    function movePathXY(x, w, h, y) = [x, h * cos(x/w * 360), y];
    function movePathZX(z, w, h, x) = [x, -h * cos(z/w * 360), z];
    function movePathYZ(y, w, h, x) = [x, y, -h * cos(y/w * 360)];

    for(rt = [0, 90]) rotate(rt) for(py = [-25 : 5 : 25]) {
        s = (rt == 0 ? -1 : 1) * (((abs(py) / 5) % 2) * 2 - 1);
        mapExtrude("constantX",
           [for(theta = [22.5:45:359]) [cos(theta), sin(theta)]],
           [for(px = [-27.5 : 27.5]) movePathXZ(px, 10, s, py) ]);
    }
    linear_extrude(height=2, center=true) offset(0.9, $fn=2) difference(){
        square(27.6*2, center=true);
        square(27.4*2, center=true);
    }
}

module exampleThreadedRod(height=20, D=10, P=1.5, theta=30){
    // https://en.wikipedia.org/wiki/ISO_metric_screw_thread
    // a threaded rod of diameter D has a trapezoid groove cut into it
    // along a helical path
    // the thread is usually chamfered at the ends to make it easier
    // to attach nuts
    xw = 0.4; // extrude width
    H = 1 / (2 * tan(theta));
    $fn = 24;
    numLoops = ceil(height / P);
    trapezoidShape = [[H/8, -P/2 - xw/2], [H/8-xw/2, -P/2 - xw/2],
                      [-7*H/8-xw/2, 0],
                      [H/8-xw/2, P/2 +xw/2], [H/8, P/2 + xw/2]];
    translate([0,0,-height/2]) difference(){
      cylinder(d=D-xw, h=height);
      // in order to avoid object self-intersections, the cut paths are
      // 3/4 turns in length, and overlap by 1/4 turn
      for(loopPos = [0 : 0.5 : numLoops]) {
        helicalPath = [ for (th = [(loopPos*360) :
                                   (360 / $fn) :
                                   ((loopPos + 0.75)*360)])
          [((D+xw)/2) * cos(th), ((D+xw)/2) * sin(th), (th/360) * P]];
          mapExtrude("vertCylinder", trapezoidShape, helicalPath);
      }
      rotate_extrude(){
          polygon([[D/2+2, -1], [D/2+2, 4], [D/2-2, -1]]);
      }
      rotate_extrude(){
          polygon([[D/2+2, height+1],
                   [D/2+2, height-4],
                   [D/2-2, height+1]]);
      }
    }
    *cylinder(d=D+2, h=P/8);
    *rotate(-90) translate([D/2,0,0]) rotate([90,0,0]) linear_extrude(height=1)
      polygon(trapezoidShape);
}

// example demonstrating wrapping around a cylinder
module examplePunchedCircle(){
  cylinder(d=10, h=20, center=true, $fn=24);
  mapExtrude("radialCylinder",
    shape=[for(theta = [0:20:359]) [4 * cos(theta), 4 * sin(theta)]],
    path=[[4,0,0], [6,0,0]]);
}

// example demonstrating a generic extrusion function that wraps around local cylinders
// at each path point
// note: this requires all adjacent points to be non-linear
// (i.e. it will fail if there are any straight lines between three consecutive points)
module exampleTrefoilKnot(){
	// equation from https://commons.wikimedia.org/wiki/File:Tricoloring.png
	extrudeShape1 = [ for(r=[0:360/30:360]) ((r > 300 ? 0.6 : 1) * [cos(r), sin(r)]) ];
	extrudeShape2 = [ for(r=[0:360/30:360]) 0.8 * [cos(r), sin(r)] ];
	trefoilPath = [ for(theta=[0:(360/300):(360)])
		3 * [2*sin(2*theta)-sin(theta),
	         2*cos(2*theta)+cos(theta),
	         sin(3*theta)] ];
	color("orange") mapExtrude("localCylinder", extrudeShape1, trefoilPath);
	color("cyan") mapExtrude("localCylinder", extrudeShape2, trefoilPath);
}

module exampleCinquefoilKnot(){
	// equation from https://commons.wikimedia.org/wiki/File:Tricoloring.png
	extrudeShape1 = [ for(r=[0:360/10:360]) ((r > 300 ? 0.6 : 1) *
      [cos(r + $t*360), sin(r + $t*360)]) ];
	extrudeShape2 = [ for(r=[0:360/10:360]) 0.8 * [cos(r + $t*360), sin(r + $t*360)] ];
	trefoilPath = [ for(theta=[0:(360/60):(360)])
		5 * [( 2*sin(2*theta)-(2/3)*sin(3*theta) ),
          ( 2*cos(2*theta)+(2/3)*cos(3*theta) ),
          sin(5*theta)] ];
	color("orange") mapExtrude("localCylinder", extrudeShape1, trefoilPath);
	color("cyan") mapExtrude("localCylinder", extrudeShape2, trefoilPath);
}


*exampleWeave();
*exampleThreadedRod();
*examplePunchedCircle();
*rotate([$t*360,0,0])
	exampleTrefoilKnot();
exampleCinquefoilKnot();
