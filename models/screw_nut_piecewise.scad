// number of outer points
arm_count = 5;

// radius of fillets on top and bottom edges
fillet_radius = sqrt(2);

// radius of outer points of the knob
arm_width = 5;

// diameter of the knob
knob_diameter = 40;

// distance inwards to create scallops
scallop_depth = 5;

// height of knob grips
knob_height = 10;

// extended height of the upper surface
upper_height = 5;

// resolution (in mm)
outer_res = 0.5;

// Style for nut
nut_style = "rounded"; // ["rounded", "bevel", "fillet", "flat"]

// radius of a circle that creates a chord with a given width and height
function chord_radius(cw, ch) =
   (ch^2 + (cw/2)^2) / (2 * ch);

function flatten(input, pos = 0, value = []) =
  (pos >= len(input)) ? value :
     flatten(input = input, pos = pos+1, value = concat(value, input[pos]));

// work out knob radius
knob_radius = knob_diameter / 2;
// work out outer circumference
outer_circumference = knob_diameter * PI;
// scallop width in terms of circumference distance
scallop_width = (outer_circumference - arm_count * arm_width) / (arm_count);
// work out the radius of the scallop cutout
crs = chord_radius(scallop_width, scallop_depth);
// calculate tangent angle
tangle = asin(scallop_width / (2 * crs));
// calculate arm circle radius
cra = arm_width * crs / scallop_width;
// calculate arm height
cha = cra - cra * cos(tangle);
// work out angular resolution for the different components
res_scallop = (outer_res / (crs * 2 * PI)) * 360;
res_arm = (outer_res / (cra * 2 * PI)) * 360;
// create radial point array (representing deltas from the radius)
radial_points = flatten(
  [ for(cycle = [0:(arm_count-1)])
    concat(
      [ for(theta = [-tangle:res_arm:tangle])
          cra * [sin(theta), cos(theta)] +
            [arm_width/2,-cra+cha] +
            [cycle * (scallop_width+arm_width), knob_radius]
      ],
      [ for(theta = [-tangle:res_scallop:tangle])
          crs * [sin(theta), -cos(theta)] +
            [-scallop_width/2,crs-scallop_depth] +
            [(cycle+1) * (scallop_width+arm_width), knob_radius]
      ])
  ]);

// simple linear extrusion, mapping the radial points to a circle
module knob_extrude_linear(){
  linear_extrude(height=knob_height, center=true)
      polygon([for(i = [0 : (len(radial_points) - 1)])
        let(theta = 360 * radial_points[i][0] / outer_circumference,
            radius = radial_points[i][1])
        radius * [cos(theta), sin(theta)]]);
}

// add upper and lower bevels at 45° angle
// note: bevel is angled towards the centre of the object,
//       not towards or away from the centre of the component circles
module knob_bevel(){
  ph_points = concat(
    [for(i = [0 : (len(radial_points) - 1)])
        let(theta = 360 * radial_points[i][0] / outer_circumference,
            radius = radial_points[i][1] - fillet_radius)
        radius * [cos(theta), sin(theta),0] +
          [0,0,-knob_height/2]],
    [for(i = [0 : (len(radial_points) - 1)])
        let(theta = 360 * radial_points[i][0] / outer_circumference,
            radius = radial_points[i][1])
        radius * [cos(theta), sin(theta),0] +
          [0,0,-knob_height/2 + fillet_radius]],
    [for(i = [0 : (len(radial_points) - 1)])
        let(theta = 360 * radial_points[i][0] / outer_circumference,
            radius = radial_points[i][1])
        radius * [cos(theta), sin(theta),0] +
          [0,0, knob_height/2 - fillet_radius]],
    [for(i = [0 : (len(radial_points) - 1)])
        let(theta = 360 * radial_points[i][0] / outer_circumference,
            radius = radial_points[i][1] - fillet_radius)
        radius * [cos(theta), sin(theta),0] +
          [0,0,knob_height/2]]
    );
  rl = len(radial_points);
  //echo(ph_points);
  polyhedron(points = ph_points,
    faces=concat(
      // lower surface
      [[for(i = [0:(rl-1)]) i]],
      // left triangle
      [for (j = [0,1,2]) for(i = [0:(rl-1)])
        [i,i+rl,(i+1) % rl] + [j*rl, j*rl, j*rl]],
      // right triangle
      [for (j = [0,1,2]) for(i = [0:(rl-1)])
        [(i+1) % rl + rl,(i+1) % rl,i+rl] +  + [j*rl, j*rl, j*rl]],
      // upper surface
      [[for(i = [(rl-1):-1:0]) (i + rl*3)]]));
}

// add upper and lower fillets (same fillet centering issue applies)
module knob_fillet(){
  ph_points = concat(
    // lower fillet
    [for (thf = [0:10:80]) for(i = [0 : (len(radial_points) - 1)])
        let(theta = 360 * radial_points[i][0] / outer_circumference,
            radius = radial_points[i][1] + fillet_radius * (cos(thf+270)-1))
        radius * [cos(theta), sin(theta),0] +
          [0,0,-knob_height/2 + fillet_radius * sin(thf+270)]],
    // bottom of straight path
    [for(i = [0 : (len(radial_points) - 1)])
        let(theta = 360 * radial_points[i][0] / outer_circumference,
            radius = radial_points[i][1])
        radius * [cos(theta), sin(theta),0] +
          [0,0,-knob_height/2 + fillet_radius]],
    // top of straight path
    [for(i = [0 : (len(radial_points) - 1)])
        let(theta = 360 * radial_points[i][0] / outer_circumference,
            radius = radial_points[i][1])
        radius * [cos(theta), sin(theta),0] +
          [0,0, knob_height/2 - fillet_radius]],
    // upper fillet
    [for (thf = [10:10:90]) for(i = [0 : (len(radial_points) - 1)])
        let(theta = 360 * radial_points[i][0] / outer_circumference,
            radius = radial_points[i][1] - fillet_radius * (cos(thf+180)+1))
        radius * [cos(theta), sin(theta),0] +
          [0,0,knob_height/2 - fillet_radius * sin(thf+180)]]
    );
  rl = len(radial_points);
  //echo(ph_points);
  polyhedron(points = ph_points,
    faces=concat(
      // lower surface
      [[for(i = [0:(rl-1)]) i]],
      // left triangle
      [for (j = [0:18]) for(i = [0:(rl-1)])
        [i,i+rl,(i+1) % rl] + [j*rl, j*rl, j*rl]],
      // right triangle
      [for (j = [0:18]) for(i = [0:(rl-1)])
        [(i+1) % rl + rl,(i+1) % rl,i+rl] +  + [j*rl, j*rl, j*rl]],
      // upper surface
      [[for(i = [(rl-1):-1:0]) (i + rl*19)]]));
}

// add upper and lower fillets, and upper curve
module knob_rounded(){
  // upper chord radius
  ucr = chord_radius(knob_diameter, upper_height);
  ph_points = concat(
    // lower fillet
    [for (thf = [0:10:80]) for(i = [0 : (len(radial_points) - 1)])
        let(theta = 360 * radial_points[i][0] / outer_circumference,
            radius = radial_points[i][1] + fillet_radius * (cos(thf+270)-1))
        radius * [cos(theta), sin(theta),0] +
          [0,0,-knob_height/2 + fillet_radius * sin(thf+270)]],
    // bottom of straight path
    [for(i = [0 : (len(radial_points) - 1)])
        let(theta = 360 * radial_points[i][0] / outer_circumference,
            radius = radial_points[i][1])
        radius * [cos(theta), sin(theta),0] +
          [0,0,-knob_height/2 + fillet_radius]],
    // top of straight path
    [for(i = [0 : (len(radial_points) - 1)])
        let(theta = 360 * radial_points[i][0] / outer_circumference,
            radius = radial_points[i][1])
        radius * [cos(theta), sin(theta),0] +
          [0,0, knob_height/2 + (sqrt(ucr^2 - radius^2) - ucr) - fillet_radius]],
    // upper fillet
    [for (thf = [10:10:90]) for(i = [0 : (len(radial_points) - 1)])
        let(theta = 360 * radial_points[i][0] / outer_circumference,
            radius = radial_points[i][1] - fillet_radius * (cos(thf+180)+1))
        radius * [cos(theta), sin(theta),0] +
          [0,0,knob_height/2 + (sqrt(ucr^2 - radius^2) - ucr) -
            fillet_radius * sin(thf+180)]],
    // upper curves
    [for (radFrac = [1:-0.1:0.1]) for(i = [0 : (len(radial_points) - 1)])
        let(theta = 360 * radial_points[i][0] / outer_circumference,
            adjRadFrac = (radFrac - 0.1) / 0.9,
            radius = (radial_points[i][1] *radFrac * adjRadFrac +
              knob_diameter*0.5*radFrac*(1-adjRadFrac)) - fillet_radius)
        radius * [cos(theta), sin(theta),0] +
          [0,0,knob_height/2 + (sqrt(ucr^2 - radius^2) - ucr) + fillet_radius]]
    );
  rl = len(radial_points);
  // join everything together
  polyhedron(points = ph_points,
    faces=concat(
      [[for(i = [0:(rl-1)]) i]], // lower surface
      [for (j = [0:28]) for(i = [0:(rl-1)])
        [i,i+rl,(i+1) % rl] + [j*rl, j*rl, j*rl]], // left triangle
      [for (j = [0:28]) for(i = [0:(rl-1)])
        [(i+1) % rl + rl,(i+1) % rl,i+rl] +  + [j*rl, j*rl, j*rl]], // right triangle
      [[for(i = [(rl-1):-1:0]) (i + rl*29)]])); // upper surface
}

if(nut_style == "flat"){
  knob_extrude_linear();
} else if(nut_style == "bevel") {
  knob_bevel();
} else if(nut_style == "fillet") {
  knob_fillet();
} else if(nut_style == "rounded") {
  knob_rounded();
}