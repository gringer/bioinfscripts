use <path_extrude.scad>;
vl = 300; // vase length
vt = 9;  // turns per vase
bpr = 0.4; // base-pair radius
bbr = 2; // backbone radius
// https://en.wikipedia.org/wiki/Nucleic_acid_double_helix#Helix_geometries
hr = vl/3.32 * 1/vt; // helix radius
prr = hr * (550 / 3320); // purine radius (guessed based on https://www.rcsb.org/3d-view/1ZEW)
bpt = 10.5; // bases per turn
dpb = 360 / bpt; // degrees per base
dpmm = vl/(vt*360); // turns per millimetre
lip = 360 - dpb / 2; // last iteration position
os = 120; // opposite strand separation (in degrees)
bs = dpb * 3; // degrees to slip back when connecting strands
rt = 6; // turns per ring
type = "linear"; // ["linear", "ring"]

// *GRINGENE* -- TAA GGN MGN ATH AAY GGN GAR AAY GAR TGA
//            -- TAA GGC AGG ATC AAC GGC GAG AAC GAG TGA

bases = [3,0,0, 2,2,3, 0,2,2, 0,3,2, 0,0,1, 2,2,1, 2,0,2, 0,0,1, 2,0,2, 3,2,0];

// OpenSCAD_User_Manual/List_Comprehensions#Concatenating_two_vectors
function cat(L1, L2) = [for(L=[L1, L2], a=L) a];

// https://math.stackexchange.com/a/1076999/756273
function planarNormals(p1, p2, p3) = 
  let(lx = (p2 - p1) / norm(p2 - p1),
      n2 = (p3 - p1),
      lz = cross(lx, n2) / norm(cross(lx, n2)),
      ly = cross(lx, lz) / norm(cross(lx, lz)))
  [lx, ly, lz];

// https://math.stackexchange.com/a/2755842/756273
// https://github.com/sergarrido/random/tree/master/circle3d
function circleCentre(p1, p2, p3) = 
  let(v1 = (p2 - p1),
      v2 = (p3 - p1),
      b = 0.5 / ((v1*v1)*(v2*v2)-(v1*v2)^2),
      k1 = b * (v2*v2)*((v1*v1) - (v1*v2)),
      k2 = b * (v1*v1)*((v2*v2) - (v1*v2)))
      p1+k1*v1+k2*v2;

// https://stackoverflow.com/a/38407105/3389895
function getPerpendVectors(p1, p2) =
  let(v1 = (p2 - p1) / norm(p2-p1))
  (max(v1) == 1) ? (
    v1[0] == 1 ? [[1,0,0], [0,1,0], [0,0,1]] :
    v1[1] == 1 ? [[0,1,0], [1,0,0], [0,0,1]] :
    [[0,0,1], [0,1,0], [1,0,0]]) :
    let(v2 = (abs(v1[2]) < abs(v1[0])) ? [v1[1], -v1[0], 0] / norm([v1[1], -v1[0], 0]) :
      [0, -v1[2], v1[1]] / norm([0, -v1[2], v1[1]]))
    let(v3 = cross(v1, v2) / norm(cross(v1, v2)))
    [v1, v2, v3];

// project a point [point] onto a plane (with normal [planeNormal], and plane centre [planePoint])
// https://math.stackexchange.com/a/100766/756273
function projectPoint(point, planeNormal, planePoint) =
  let(x = point[0], y = point[1], z = point[2],
      a = planeNormal[0], b = planeNormal[1], c = planeNormal[2],
      d = planePoint[0], e= planePoint[1], f = planePoint[2],
      t = (a*d + b*e + c*f - a*x - b*y - c*z) / (a*a + b*b + c*c))
      [x + t*a, y + t*b, z + t*c];

function base2col(b) = (b == 0) ? [0,0.392,0] :  // Adenine  Green
                       (b == 1) ? [0,0,1] :  // Cytosine Blue
                       (b == 2) ? [1,0.843,0] :  // Guanine  Yellow
                       [0.980,0.502,0.447];              // Thymine  Red

module drawLine(p1, p2){
  translate(p1) rotate([-acos((p2[2]-p1[2]) / norm(p1-p2)),0,
          -atan2(p2[0]-p1[0],p2[1]-p1[1])]) {
        cylinder(r1=bpr, r2=bpr/2, h=norm(p1-p2)-bpr*2, $fn=12);
        translate([0,0,norm(p1-p2)-bpr*4]) cylinder(r1=bpr*2, r2=0, h=bpr*4, $fn=12);
     }
}

module drawStick(p1, p2){
  translate(p1) rotate([-acos((p2[2]-p1[2]) / norm(p1-p2)),0,
          -atan2(p2[0]-p1[0],p2[1]-p1[1])]) {
        cylinder(r=bpr, h=norm(p1-p2), $fn=12);
     }
}


module drawBases(p1, p2, pc, pos, both=true){
  baseType = bases[(pos % len(bases) + len(bases)) % len(bases)];
  bd = ((baseType % 2) == 0) ? 1.75 : 2.25; // A/G are double-ring purines
  c1 = base2col(baseType);
  c2 = base2col(3-baseType);
  br = (((baseType+1) % 4) < 2) ? 45 : 30;
  if(baseType != 20){
      translate(p1) color(c1)
        rotate([-acos((p2[2]-p1[2]) / norm(p1-p2)),0,
                -atan2(p2[0]-p1[0],p2[1]-p1[1])]) 
           rotate([0,0,br]) translate([-bpr,-bpr*(baseType%3 != 0?1.5:0.5)])
      cube([bpr*2,bpr*(baseType%3!=0?3:1),norm(p1-p2)/bd]);
  }
  if(both && (baseType != 20)){
  translate(p2) color(c2)
    rotate([-acos((p2[2]-p1[2]) / norm(p1-p2)),0,
            -atan2(p2[0]-p1[0],p2[1]-p1[1])]) 
       rotate([0,180,br]) translate([-bpr,-bpr*(baseType%3 != 0?1.5:0.5)])
      cube([bpr*2,bpr*(baseType%3!=0?3:1),norm(p1-p2)/(4.5-bd)]);
  }
}

vxr = -6;
vyr = -60;
vzr = 160;

module purine(vx = [1,0,0], vy = [0,1,0], vz = [0,0,1], pr = 5, ph = 2, sc = "steelblue", bc = "salmon"){
  pentRad = pr  *  (sqrt(1/10 * (5 + sqrt(5))));
  pentCent = pentRad * 5 * -vy;
  hexCent = pentCent + pentRad * sin(36) * vx + pentRad * cos(36) * -vy +
  pr * cos(12) * vx + pr * sin(12) * vy +
  + 0*vz;
  phPointsHex = [ for (zp = [-0.5, 0.5]) for(hr = [0:(360/6):(360-180/6)])
    vx * pr * cos(hr+12) + vy * pr * sin(hr+12) + vz * zp * ph + hexCent ];
  lpph3 = [for (i = [0:2]) len(phPointsHex)];
  lpph5 = [for (i = [0:4]) len(phPointsHex)];
  phPointsPent = [ for (zp = [-0.5, 0.5]) for(hr = [0:(360/5):(360-180/5)])
    vx * pentRad * cos(hr+90) + vy * pentRad * sin(hr+90) + vz * zp * ph + pentCent];
  rotate(a=vyr, v=vy) rotate(a=vxr, v=vx) rotate(a=vzr, v=vz) {
      color(bc) polyhedron(points=concat(phPointsHex, phPointsPent), faces=
      concat([[0,1,2,3,4,5]],
        [[11,10,9,8,7,6]],
        [for(i = [3:7]) [(i % 6) + 6, (i+1) % 6+6, (i+1) % 6] ],
        [for(i = [3:7]) [(i+1) % 6, (i % 6), (i % 6)+6] ],
        [[0,1,2,3,4] + lpph5],
        [[9,8,7,6,5] + lpph5],
        [for(i = [4:7]) [(i % 5) + 5, (i+1) % 5+5, (i+1) % 5] + lpph3 ],
        [for(i = [4:7]) [(i+1) % 5, (i % 5), (i % 5)+5] + lpph3 ]
        ));
      color(sc) drawStick([0,0,0], pentCent);
    }
}

module pyrimidine(vx = [1,0,0], vy = [0,1,0], vz = [0,0,1], pr = 5, ph = 2, sc = "steelblue", bc = "salmon"){
  hexCent = pr * 5 * -vy;
  phPoints = [ for (zp = [-0.5, 0.5]) for(hr = [0:(360/6):(360-180/6)])
    vx * pr * cos(hr+30) + vy * pr * sin(hr+30) + vz * zp * ph + hexCent];
  rotate(a=vyr, v=vy) rotate(a=vxr, v=vx) rotate(a=vzr, v=vz) {
      color(bc) polyhedron(points=phPoints, faces=
      concat([[0,1,2,3,4,5]],
        [[11,10,9,8,7,6]],
        [for(i = [0:5]) [i + 6, (i+1) % 6+6, (i+1) % 6] ],
        [for(i = [0:5]) [(i+1) % 6, i, i+6] ]));
      color(sc) drawStick([0,0,0], hexCent);
   }
}


rotate([54.13,7,25.13]) 
translate([-0,0,0]) rotate([$t*360*1,0,0]) {
    base_centre = [for(i = [-(180*vt):(dpb):(180 * vt )]) [
              i * dpmm, 0, 0
              ] ];
    base_poss = [for (shift = [0, os])
      [for(i = [-(180*vt):(dpb):(180 * vt)]) [
              i * dpmm,
              hr * cos(i + shift),
              hr * sin(i + shift)
              ] ] ];
    base_poss_backBone = [for (shift = [0, os])
      [for(i = [-(180*vt+dpb):(dpb/2):(180 * vt + dpb)]) [
              i * dpmm,
              hr * cos(i + shift),
              hr * sin(i + shift)
              ] ] ];
    backBone_centre = [for(i = [-(180*vt+dpb):(dpb/2):(180 * vt + dpb)]) [
              i * dpmm, 0, 0
              ] ];
    // DNA backbone
    *if(type == "linear"){
      for(strand = [0, 1]){
          for(pp = base_poss_backBone[strand]){
            translate(pp) sphere(r=1, $fn=12);
          }
          //path_extrude(exShape=myPoints, exPath=myPath, merge=false, exRots = [for(i = [0:len(myPath)]) 0]);
      }
    }
    bl = len(base_poss_backBone[0]) - 1;
    // backbone points (running anti-parallel for each strand
    bbPoints = [for (strand = [0, 1]) [for (i = [0:bl]) base_poss_backBone[strand][(strand == 0 ? i : (bl - i))] ]];
    // helix centres for each backbone point
    hxCens = [for (strand = [0, 1]) [for (i = [0:bl]) backBone_centre[(strand == 0 ? i : bl - i)] ]];
    // non-normalised perpendicular vectors
    pvNN = [for (strand = [0, 1]) [for (i = [1:(bl-1)]) getPerpendVectors(bbPoints[strand][i], bbPoints[strand][i+1])]];
    // normalised vector pointing along the backbone
    pvbN = [for (strand = [0, 1]) [for (i = [1:(bl-1)])
      ( pvNN[strand][i-1][0] ) ]];
    // normalised planar helix centres for each backbone point
    hxCensNorm = [for (strand = [0, 1]) [for (i = [1:(bl-1)]) projectPoint(hxCens[strand][i], pvbN[strand][i-1], bbPoints[strand][i]) ]];
    // normalised vector pointing towards the centre
    pvcN = [for (strand = [0, 1]) [for (i = [1:(bl-1)])
      ( (hxCensNorm[strand][i-1] - bbPoints[strand][i]) / norm(hxCensNorm[strand][i-1] - bbPoints[strand][i]) ) ]];
    // normalised vector perpendicular to the centre and backbone vectors
    pvpN = [for (strand = [0, 1]) [for (i = [1:(bl-1)])
      ( cross(pvbN[strand][i-1], pvcN[strand][i-1]) / norm(cross(pvbN[strand][i-1], pvcN[strand][i-1])) ) ]];
    // line from attachment point to helix centres
    *for(strand = [0,1]){
      for(i = [2:2:(bl-1)]){
        color("cyan") drawLine(bbPoints[strand][i], bbPoints[strand][i] + pvbN[strand][i-1] * 5);
        color("magenta") drawLine(bbPoints[strand][i], bbPoints[strand][i] + pvcN[strand][i-1] * 5);
        color("yellow") drawLine(bbPoints[strand][i], bbPoints[strand][i] + pvpN[strand][i-1] * 5);
      }
    }
    // number of extrusion points per backbone position
    bbFn = 5;
    // backbone extrusion points
    bbExtPoints = [ for(strand = [0,1]) [ for(i = [1:(bl-1)]) for (ir = [0:(360/bbFn):(360 - 0.5/bbFn)])
      (bbPoints[strand][i] + bbr * pvcN[strand][i-1] * cos(ir) + bbr * pvpN[strand][i-1] * sin(ir)) ]];
    phl = len(bbExtPoints[0]) - 1;
    phlV = [for (i = [0:(bbFn-1)]) phl];
    *for (strand = [0,1]){
      for(i = [0:phl]){
        translate(bbExtPoints[strand][i]) cube(1, center=true);
      }
    }
    strandColours = ["purple", "darkcyan"];
    for(strand = [0, 1]){
      color(strandColours[strand]) polyhedron(points = bbExtPoints[strand], 
         faces=concat([[0,1,2,3,4], [0,-1,-2,-3,-4] + phlV],
         [for (bi = [0:(bl-3)]) for (i=[0:(bbFn-1)]) [(i+1) % bbFn + bi * bbFn, i + bi * bbFn, i+bbFn + bi * bbFn]],
         [for (bi = [0:(bl-3)]) for (i=[0:(bbFn-1)]) [((i+1) % bbFn) + bbFn + bi * bbFn, (i + 1) % bbFn + bi * bbFn, i+bbFn + bi * bbFn]]  ));
    }
    blp = len(base_poss[0]) - 1;
    for(i = [0:blp]){
      //drawBases(base_poss[0][i], base_poss[1][i], base_centre[i], i, both=true);
      baseType = bases[(i % len(bases) + len(bases)) % len(bases)];
      c1 = base2col(baseType);
      c2 = base2col(3-baseType);
      bbPos = i * 2 + 1;
      bbPosRC = (blp - i + 1) * 2;
      translate(bbPoints[0][bbPos]) if(baseType % 2 == 0) {
        purine(vx=pvpN[0][bbPos-1], vy=pvcN[0][bbPos-1], vz=-pvbN[0][bbPos-1], pr=prr, ph=0.8, sc=strandColours[0], bc=c1);
      } else {
        pyrimidine(vx=pvpN[0][bbPos-1], vy=pvcN[0][bbPos-1], vz=-pvbN[0][bbPos-1], pr=prr, ph=0.8, sc=strandColours[0], bc=c1);
      }
      translate(bbPoints[1][bbPosRC]) if(baseType % 2 == 1) {
        purine(vx=pvpN[1][bbPosRC-1], vy=pvcN[1][bbPosRC-1], vz=-pvbN[1][bbPosRC-1], pr=prr, ph=0.8, sc=strandColours[1], bc=c2);
      } else {
        pyrimidine(vx=pvpN[1][bbPosRC-1], vy=pvcN[1][bbPosRC-1], vz=-pvbN[1][bbPosRC-1], pr=prr, ph=0.8, sc=strandColours[1], bc=c2);
      }
    }
}
