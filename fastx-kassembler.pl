#!/usr/bin/env perl
use warnings;
use strict;

use Pod::Usage; ## uses pod documentation in usage code
use Getopt::Long qw(:config auto_help pass_through);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);
use Time::HiRes qw(time); ## for measuring sub-second time

$| = 1; # automatically flush output [https://perldoc.perl.org/perlvar]

our $VERSION = "0.1";

=head1 NAME

fastx-kassembler.pl -- simple kmer-based (de-bruijn) assembler for filtered short sequences, amplicons and plasmids

=head1 SYNOPSIS

./fastx-kassembler.pl <reads.fq> [options]

=head2 Options

=over 2

=item B<-help>

Only display this help message

=item B<-forward>

Only create forward-direction kmers (for strand-specific assembly)

=item B<-iterations> I<count>

Run the path traversal I<count> times

=item B<-kmerLength> I<kLen>

Set length of kmers for counting and assembly to I<kLen> bases (default 32)

=item B<-verbose>

Show additional output to help with understanding the counting / assembly process

=item B<-notrim>

Don't display low coverage-trimmed sequences

=item B<-count> I<extendCount>

Attempt at most I<extendCount> extensions (in both directions) from the most frequently-occuring kmer (default 10,000)

=item B<-min> I<minKmerCount>

After counting kmers, remove all kmers with a count of less than I<minKmerCount> (default 2)

=back

=cut

my $verboseLevel = 0;
my $count = 10000; ## default number of extension attempted
my $sequencesRead = 0;
my %kmerCounts = ();
my %kmerRCounts = ();
my %kmerLCounts = ();
my $kLen = 32;
my $minKmerCount = 2;
my $forwardOnly = 0;
my $iterations = 4;
my $trimSeqs = 1; ## true

sub rc {
  my ($seq) = @_;
  $seq =~ tr/ACGT/TGCA/;
  return(scalar(reverse($seq)));
}

sub countKmers {
  my ($seq) = @_;
  for(my $i = 0; $i < (length($seq) - $kLen); $i++){
    # despite appearances, this seems to be pretty fast
    $kmerCounts{substr($seq, $i, $kLen)}++;
  }
}

GetOptions("forward!" => \$forwardOnly, "kmerlength=i" => \$kLen, "iterations|i=i" => \$iterations,
           "verbose!" => \$verboseLevel, "trim!" => \$trimSeqs, "count=i" => \$count, "min=i" => \$minKmerCount) or
    pod2usage(1);

# Complain about non-file command line argument
my @files = ();
while(@ARGV){
  my $arg = shift(@ARGV);
  if(-e $arg){
    push(@files, $arg);
  } else {
    die("Unknown argument: $arg");
  }
}
@ARGV = @files;

# use stdin if no files supplied
if(!@ARGV){
  @ARGV = '-' unless (-t STDIN);
}

my $startTime = time;
my $inQual = 0; # false
my $seqID = "";
my $qualID = "";
my $seq = "";
my $qual = "";
foreach my $file (@ARGV) {
  # This little gunzip dance makes sure the script can handle both
  # gzip-compressed and uncompressed input, regardless of whether
  # or not it is piped
  my $z = new IO::Uncompress::Gunzip($file, MultiStream => 1)
    or die "gunzip failed: $GunzipError\n";
  while(<$z>){
    chomp;
    chomp;
    if (!$inQual) {
      if (/^(>|@)(.*)$/) {
        my $newSeqID = $2;
        if ($seqID) {
          countKmers($seq);
        }
        $seq = "";
        $qual = "";
        $seqID = $newSeqID;
      } elsif (/^\+(.*)$/) {
        $inQual = 1;            # true
        $qualID = $1;
      } else {
        $seq .= $_;
      }
    } else {
      $qual .= $_;
      if (length($qual) >= length($seq)) {
        $inQual = 0;            # false
        if ($seqID) {
          countKmers($seq);
          $seqID = "";
          $seq = "";
          $qual = "";
        }
      }
    }
  }
}

if ($seqID) {
  countKmers($seq);
}

my $timeDiff = time - $startTime;
printf(STDERR "Seen %d kmers in %0.1f seconds\n", scalar(keys(%kmerCounts)), $timeDiff);

if(scalar(keys(%kmerCounts)) == 0){
  print(STDERR "Error: no sequences seen\n");
  pod2usage(1);
}

# remove singular kmers
while (my ($kmer, $count) = each %kmerCounts) {
  if($count < $minKmerCount){
    delete $kmerCounts{$kmer};   # This is safe [https://perldoc.perl.org/functions/each]
  }
}

printf(STDERR "Deleted kmers with count of less than %d; %d kmers remain\n",
       $minKmerCount, scalar(keys(%kmerCounts)));

my $chainID = 1;

# carry out de-bruijn chain assembly
sub doDebruijn {

  my %assemblyLink = ();
  my %kmersSeen = ();
  my @sortedKmers = sort { $kmerCounts{$b} <=> $kmerCounts{$a} } keys(%kmerCounts);

  if($verboseLevel > 0){
    print(STDERR "Most frequent kmers:\n");
    for (my $i = 0; $i < 10; $i++){
      my $kmer = $sortedKmers[$i];
      printf(STDERR " %5d %d %s\n", $i, $kmerCounts{$kmer}, $kmer);
    }
  }

  if($count > scalar(@sortedKmers)){
    $count = scalar(@sortedKmers);
  }

  my $assemblyChain = $sortedKmers[0];
  my $maxChainCount = $kmerCounts{$assemblyChain} + 1;
  $kmersSeen{$assemblyChain} = 1;
  my %baseCounts = ();
  my $changed = 1;
  my $startPos = 0;
  my @relCov = ($maxChainCount - 1);

  for(my $i = 0; $changed && ($i < $count); $i++){
    $changed = 0;
    my $leftKmer = substr($assemblyChain, 0, $kLen - 1);
    my $rightKmer = substr($assemblyChain, -($kLen - 1));
    my $maxCount = 0;
    my $maxBase = "";
    # find a left extension (if any)
    foreach my $base ("A", "C", "G", "T"){
      if(exists($kmerCounts{$base . $leftKmer})){
        my $tmpCount = $kmerCounts{$base . $leftKmer};
        if($tmpCount > $maxCount){
          $maxBase = $base;
          $maxCount = $tmpCount;
        }
      }
    }
    if($maxCount && !$kmersSeen{$maxBase . $leftKmer}){
      $changed = 1;
      $assemblyChain = $maxBase . $assemblyChain;
      $kmersSeen{$maxBase . $leftKmer} = 1;
      unshift(@relCov, $maxCount);
      $startPos++;
    }
    $maxCount = 0;
    $maxBase = "";
    foreach my $base ("A", "C", "G", "T"){
      if(exists($kmerCounts{$rightKmer . $base})){
        my $tmpCount = $kmerCounts{$rightKmer . $base};
        if($tmpCount > $maxCount){
          $maxBase = $base;
          $maxCount = $tmpCount;
        }
      }
    }
    if($maxCount && !$kmersSeen{$rightKmer . $maxBase}){
      $changed = 1;
      $assemblyChain = $assemblyChain . $maxBase;
      $kmersSeen{$rightKmer . $maxBase} = 1;
      push(@relCov, $maxCount);
    }
  }

  my $lastCov = $relCov[$#relCov];
  for(my $i = 0; $i < ($kLen-1); $i++){
    push(@relCov, $lastCov);
  }

  if($verboseLevel > 0){
    my @countDrops = map {sprintf("%s;", ($maxChainCount - $_))} (@relCov);
    $countDrops[$startPos] = "*" . $countDrops[$startPos] . "*";
    printf("Kmer differences in primary chain from most abundant kmer: %s\n", join(";", @countDrops));
  }

  my @qScores = map {sprintf("%s", chr(-10 * log(($maxChainCount - $_) / $maxChainCount) / log(10) + 33))} (@relCov);

  ## Adjust coverage-trimmed edge positions to account for kmer length
  my $leftEdgePos = $startPos;
  my $rightEdgePos = $startPos;
  for (my $i = $startPos; ($i > 0) && ($qScores[$i] ne "!"); $i--){
    $leftEdgePos = $i;
  }
  for (my $i = $startPos; ($i < $#qScores) && ($qScores[$i] ne "!"); $i++){
    $rightEdgePos = $i;
  }
  $leftEdgePos = (($leftEdgePos - $kLen) < 0) ? 0 : $leftEdgePos - $kLen;
  $rightEdgePos = (($rightEdgePos + $kLen) > $#qScores) ? ($#qScores + 1) : ($rightEdgePos + $kLen + 1);

  ## Print out full chain
  printf("\@chain_%04d [%d bp; max coverage=%d]\n%s\n+\n%s\n", $chainID, length($assemblyChain), $maxChainCount, $assemblyChain,
         join("", @qScores));

  my $trimmedChain = $assemblyChain;

  if(($trimSeqs) && (($verboseLevel > 0) || ($rightEdgePos - $leftEdgePos < (length($assemblyChain) - 5)))){
    ## Print out trimmed chain
    $trimmedChain = substr($assemblyChain, $leftEdgePos, $rightEdgePos - $leftEdgePos);
    printf("\@chain_%04d_trimmed [%d bp; max coverage=%d]\n%s\n+\n%s\n", $chainID, $rightEdgePos - $leftEdgePos, $maxChainCount,
           $trimmedChain, substr(join("", @qScores), $leftEdgePos, $rightEdgePos - $leftEdgePos));
  }

  ## TODO: iterate through trimmed chain and remove seen kmers [TODO: if they have a single path]
  for(my $i = 0; $i <= (length($trimmedChain) - $kLen); $i++){
    my $kmer = substr($trimmedChain, $i, $kLen);
    my $leftKmer = substr($trimmedChain, $i, $kLen-1);
    my $rightKmer = substr($trimmedChain, $i+1, $kLen);
    delete $kmerCounts{$kmer};
    delete $kmerCounts{$leftKmer};
    delete $kmerCounts{$rightKmer};
  }
  $chainID++;
}

## rinse / repeat
for(my $i = 0; $i < $iterations; $i++){
  doDebruijn();
}
