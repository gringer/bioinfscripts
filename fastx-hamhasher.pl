#!/usr/bin/env perl
use warnings;
use strict;

## fastx-hamhasher.pl -- Identifies sequences closely matching sequences in a provided file
## Unlike traditional aligners, this looks for full length exact matches and one edit-distance matches

use Getopt::Long qw(:config auto_help pass_through);

my $quiet = 0;
my $seqFileName = "";
my %targetSeqs = ();
my %targetHash = ();
my $prefix = "";
my %prefixHash = ();

sub processSeq {
  my ($seq, $qual, $seqID) = @_;
  my $targName = "";
  if(exists($targetHash{$seq})){
    $targName = " [" . $targetHash {$seq} . "]";
  } elsif ($prefix && (exists($prefixHash{substr($seq, 0, length($prefix))}))) {
    if(!$quiet){
      warn(sprintf("Warning: sequence '%s' starts with '%s', but doesn't match a known sequence:\n  %s",
                   $seqID, $prefix, $seq));
    }
    $targName = " [$prefix-unknown]";
  }
  if($qual){
    printf("@%s%s\n%s\n+\n%s\n", $seqID, $targName, $seq, $qual);
  } else {
    printf(">%s%s\n%s\n", $seqID, $targName, $seq);
  }
}

GetOptions("quiet!" => \$quiet, "seqfile=s" => \$seqFileName, "prefix=s" => \$prefix) or
  die("Error in command line arguments");

if(!(-e $seqFileName)){
  die("Error: no sequence file provided in input (-seqfile <fileName>)");
} else {
  my $targetName = "";
  open(my $seqFile, "<", $seqFileName);
  while(<$seqFile>){
    chomp; chomp;
    if(/^>(.*)$/){
      $targetName = $1;
    } else {
      if(!$targetName){
        die("Error: sequence before fasta header in file '$seqFileName'");
      }
      $targetSeqs{$targetName} .= $_;
    }
  }
  close($seqFile);
}

## create target hash structure
## [memory usage is approx 4 * length of string * length of string]

foreach my $targetName (keys(%targetSeqs)){
  my $targetSeq = $targetSeqs{$targetName};
  # One substitution
  for(my $i1 = 0; $i1 < length($targetSeq); $i1++){
    foreach my $b1 ("A", "C", "G", "T"){
      my $seq = $targetSeq;
      substr($seq, $i1, 1, $b1);
      $targetHash{$seq} = $targetName;
    }
  }
  # One deletion
  for(my $i1 = 0; $i1 < length($targetSeq); $i1++){
    foreach my $b1 ("A","C","G","T"){
      my $seq = $targetSeq . $b1;
      substr($seq, $i1, 1, "");
      $targetHash{$seq} = $targetName;
    }
  }
  # One insertion
  for(my $i1 = 0; $i1 < length($targetSeq); $i1++){
    foreach my $b1 ("A","C","G","T"){
      my $seq = $targetSeq;
      substr($seq, $i1, 0, $b1);
      substr($seq, length($targetSeq), 1, "");
      $targetHash{$seq} = $targetName;
    }
  }
}

## create prefix hash structure
## [memory usage is approx 4 * length of string * length of string]

if($prefix){
  # Two substitution
  for(my $i1 = 0; $i1 < length($prefix); $i1++){
    foreach my $b1 ("A", "C", "G", "T"){
      for(my $i2 = 0; $i2 < length($prefix); $i2++){
        foreach my $b2 ("A", "C", "G", "T"){
          my $seq = $prefix;
          substr($seq, $i1, 1, $b1);
          substr($seq, $i2, 1, $b2);
          $prefixHash{$seq} = $prefix;
        }
      }
    }
  }
  # One deletion
  for(my $i1 = 0; $i1 < length($prefix); $i1++){
    foreach my $b1 ("A","C","G","T"){
      my $seq = $prefix . $b1;
      substr($seq, $i1, 1, "");
      $prefixHash{$seq} = $prefix;
    }
  }
  # One insertion
  for(my $i1 = 0; $i1 < length($prefix); $i1++){
    foreach my $b1 ("A","C","G","T"){
      my $seq = $prefix;
      substr($seq, $i1, 0, $b1);
      substr($seq, length($prefix), 1, "");
      $prefixHash{$seq} = $prefix;
    }
  }
}

# unknown commands are treated as identifiers
my @files = ();
while(@ARGV){
  my $arg = shift(@ARGV);
  if(-f $arg){
    push(@files, $arg);
  }
}
@ARGV = @files;

my $inQual = 0; # false
my $seqID = "";
my $qualID = "";
my $seq = "";
my $qual = "";
while(<>){
  chomp;
  chomp;
  if(!$inQual){
    if(/^(>|@)((.+?)( .*?\s*)?)$/){
      my $newSeqID = $2;
      my $newShortID = $3;
      if($seqID || ($seqID eq "0")){
        processSeq($seq, $qual, $seqID);
      }
      $seq = "";
      $qual = "";
      $seqID = $newSeqID;
    } elsif(/^\+(.*)$/) {
      $inQual = 1; # true
      $qualID = $1;
    } else {
      $seq .= $_;
    }
  } else {
    $qual .= $_;
    if(length($qual) >= length($seq)){
      $inQual = 0; # false
    }
  }
}

if($seqID || ($seqID eq "0")){
  processSeq($seq, $qual, $seqID);
}
